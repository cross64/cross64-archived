# cross64

This is the official repository for the Cross64 suite development.
The projects included are:
  - Update/Starter (App)
  - Core of the Cross64 App (Lib)
  - Runtimes/Cross64 API/Interface (Lib)
  - Official Game Core/API's (Lib)