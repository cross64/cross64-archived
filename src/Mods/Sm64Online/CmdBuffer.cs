using Cross64.Runtimes;
using System;

namespace Sm64Online
{
    public partial class Sm64OnlineMod
    {
        private enum Command
        {
            Empty = 0,
            Spawn = -1,
            Despawn = -2,
        }

        private class CommandBuffer
        {
            private readonly uint addrList = 0x803000;

            private readonly Slot[] slots = new Slot[16];

            public CommandBuffer()
            {
                for (var i = 0; i < 16; i++)
                    slots[i] = new Slot((uint)(addrList + i * 0x08));
            }

            public void RunCommand(int index, Command cmd = Command.Empty, Action<uint> cb = default)
            {
                if (cmd == Command.Empty || cmd == slots[index].Command) return;

                slots[index].Command = cmd;
                slots[index].cb = cb;
                slots[index].ticking = true;
            }

            public void OnTick()
            {
                for (var i = 0; i < slots.Length; i++)
                {
                    if (!slots[i].ticking || slots[i].Command != Command.Empty) continue;

                    // Command is finished
                    slots[i].cb(slots[i].Pointer);
                    slots[i].ticking = false;
                }
            }

            private class Slot
            {
                internal Action<uint> cb;
                internal bool ticking;

                private readonly uint addr;

                public Slot(uint address)
                {
                    addr = address;
                }

                public Command Command
                {
                    get => (Command)Emulator.RdRam.Read32(addr);
                    set
                    {
                        var exists = Pointer != 0x0;
                        if ((exists && value == Command.Spawn) ||
                            (!exists && value == Command.Despawn))
                            Emulator.RdRam.Write32(addr, (int)Command.Empty);
                        else Emulator.RdRam.Write32(addr, (uint)value);
                    }
                }

                public Emulator.Pointer Pointer => Emulator.RdRam.Read32(addr + 0x04);
            }
        }
    }
}