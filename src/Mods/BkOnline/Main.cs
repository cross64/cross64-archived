using System;
using Cross64;
using Cross64.Api;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace BkOnline
{
    public class Sm64OnlinePlugin : PluginBase
    {
        public override NetworkType NetworkMode => NetworkType.Decentralized;
        public override string MasterServerIp => "158.69.60.101";
        public override GameID[] Game => new[] {GameID.BanjoKazooie};
        public override string Name => "BkOnline";
        public override string Description => "Made by SpiceyWolf and Mittenz.";
        public override string DiscordImage => "bko";
        public override ModBase InitMod => new BkOnlineMod();
    }

    public partial class BkOnlineMod : NetworkedModBase
    {
        protected override object Config => config;
        private ConfigDef config = new ConfigDef();

        public class ConfigDef
        {
            public bool PrintEventsLevel { get; set; } = false;
            public bool PrintEventsScene { get; set; } = false;
            public bool PrintNetClient { get; set; } = false;
            public bool PrintNetServer { get; set; } = false;
            public bool ShowTracker { get; set; } = false;
            public bool SkipIntro { get; set; } = false;
        }

        internal BanjoKazooie Api;

        public override void Initialize(ApiBase api)
        {
            Api = (BanjoKazooie) api;
        }

        protected override void Dispose(bool disposing)
        {
        }

        public override void OnTick(uint frame)
        {
        }
        
        public override void OnClientReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void OnServerReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void Client_JoinLobby()
        {
        }

        public override void Client_PlayerJoined(int index)
        {
        }

        public override void Client_PlayerLeft(int index)
        {
        }

        public override void Server_LobbyCreated()
        {
        }

        public override void Server_PlayerJoined(int index)
        {
        }

        public override void Server_PlayerLeft(int index)
        {
        }
    }
}