using System;
using Cross64;
using Cross64.Api;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Dk64Online
{
    public class Sm64OnlinePlugin : PluginBase
    {
        public override NetworkType NetworkMode => NetworkType.Decentralized;
        public override string MasterServerIp => "158.69.60.101";
        public override GameID[] Game => new[] {GameID.DonkeyKong64};
        public override string Name => "Dk64Online";
        public override string Description => "Made by PMONickPop123 and SpiceyWolf.";
        public override string DiscordImage => "dk64o";
        public override ModBase InitMod => new Dk64OnlineMod();
    }

    public partial class Dk64OnlineMod : NetworkedModBase
    {
        protected override object Config => config;
        private ConfigDef config = new ConfigDef();

        public class ConfigDef
        {
            public bool PrintNetClient { get; set; } = false;
            public bool PrintNetServer { get; set; } = false;
        }

        internal DonkeyKong64 Api;

        public override void Initialize(ApiBase api)
        {
            Api = (DonkeyKong64) api;
        }

        protected override void Dispose(bool disposing)
        {
        }

        public override void OnTick(uint frame)
        {
        }
        
        public override void OnClientReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void OnServerReceivePacket(CrossPacket packet, string packetName, int senderIndex)
        {
        }

        public override void Client_JoinLobby()
        {
        }

        public override void Client_PlayerJoined(int index)
        {
        }

        public override void Client_PlayerLeft(int index)
        {
        }

        public override void Server_LobbyCreated()
        {
        }

        public override void Server_PlayerJoined(int index)
        {
        }

        public override void Server_PlayerLeft(int index)
        {
        }
    }
}