using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChouSnobowKids : ApiBase
    {
        public override string GameName => "Chou Snobow Kids";
        public override GameID GameID => GameID.ChouSnobowKids;

        public ChouSnobowKids()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
