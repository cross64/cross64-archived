using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class OgaBatoruRokujuyonPasonObuRodoriKyariba : ApiBase
    {
        public override string GameName => "Ōga Batoru Rokujūyon Pāson Obu Rōdorī Kyaribā";
        public override GameID GameID => GameID.OgaBatoruRokujuyonPasonObuRodoriKyariba;

        public OgaBatoruRokujuyonPasonObuRodoriKyariba()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
