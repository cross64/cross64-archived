using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class YukeYukeTroubleMakers : ApiBase
    {
        public override string GameName => "Yuke Yuke!! Trouble Makers";
        public override GameID GameID => GameID.YukeYukeTroubleMakers;

        public YukeYukeTroubleMakers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
