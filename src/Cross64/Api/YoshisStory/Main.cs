using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class YoshisStory : ApiBase
    {
        public override string GameName => "Yoshi's Story";
        public override GameID GameID => GameID.YoshisStory;

        public YoshisStory()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
