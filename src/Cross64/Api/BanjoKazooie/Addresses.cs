namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public enum AddressType
        {
            BetaMenu,

            Camera,
            Inventory,
            Player,
            Puppet,

            ColActor,
            ColVoxel,

            RtActorArrayPtr,
            RtCollisionPtr,
            RtCurExit,
            RtCurHealth,
            RtCurLevel,
            RtCurLevelEvents,
            RtCurLevelLookup,
            RtCurProfile,
            RtCurScene,
            RtCurSceneEvents,
            RtIsCutscene,
            RtIsLoading,
            RtTransitionState,
            RtVoxelArrayPtr,
            RtVoxelCountPtr,

            SaveCheatFlags,
            SaveSram,
            SaveGameFlags,
            SaveHoneycombFlags,
            SaveJiggyFlags,
            SaveMoveFlags,
            SaveMumboTokenFlags,
            SaveNoteTotals,
        }

        public enum GameVersion
        {
            JP_1_0,
            PAL_1_0,
            USA_1_0,
            USA_1_1
        }

        private void SetVersion(GameVersion version)
        {
            Addresses.Clear();

            switch (version)
            {
                case GameVersion.JP_1_0:
                    Addresses.Add((int) AddressType.BetaMenu, 0x383bc0);
                    Addresses.Add((int) AddressType.Camera, 0x37e458);
                    Addresses.Add((int) AddressType.Inventory, 0x386a70);
                    Addresses.Add((int) AddressType.Player, 0x37cbe0);

                    // Runtime Data
                    Addresses.Add((int) AddressType.RtActorArrayPtr, 0x36f260);
                    Addresses.Add((int) AddressType.RtCollisionPtr, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurExit, 0x37f406);
                    Addresses.Add((int) AddressType.RtCurLevel, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurLevelEvents, 0x383301);
                    Addresses.Add((int) AddressType.RtCurLevelLookup, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurProfile, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurScene, 0x37f405);
                    Addresses.Add((int) AddressType.RtCurSceneEvents, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtIsCutscene, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtIsLoading, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtTransitionState, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtVoxelArrayPtr, 0x382ab0);
                    Addresses.Add((int) AddressType.RtVoxelCountPtr, 0x00000); // Need found

                    // Save Data
                    Addresses.Add((int) AddressType.SaveCheatFlags, 0x383d48);
                    Addresses.Add((int) AddressType.SaveSram, 0x00000); // Need found
                    Addresses.Add((int) AddressType.SaveGameFlags, 0x383d18);
                    Addresses.Add((int) AddressType.SaveHoneycombFlags, 0x383e20);
                    Addresses.Add((int) AddressType.SaveJiggyFlags, 0x383e00);
                    Addresses.Add((int) AddressType.SaveMoveFlags, 0x37cea0);
                    Addresses.Add((int) AddressType.SaveMumboTokenFlags, 0x383e30);
                    break;

                case GameVersion.PAL_1_0:
                    Addresses.Add((int) AddressType.BetaMenu, 0x383a60);
                    Addresses.Add((int) AddressType.Camera, 0x37e328);
                    Addresses.Add((int) AddressType.Inventory, 0x386910);
                    Addresses.Add((int) AddressType.Player, 0x37cab0);

                    // Runtime Data
                    Addresses.Add((int) AddressType.RtActorArrayPtr, 0x36eae0);
                    Addresses.Add((int) AddressType.RtCollisionPtr, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurExit, 0x37f2c6);
                    Addresses.Add((int) AddressType.RtCurLevel, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurLevelEvents, 0x383301);
                    Addresses.Add((int) AddressType.RtCurLevelLookup, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurProfile, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurScene, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurSceneEvents, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtIsCutscene, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtIsLoading, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtTransitionState, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtVoxelArrayPtr, 0x382970);
                    Addresses.Add((int) AddressType.RtVoxelCountPtr, 0x00000); // Need found

                    // Save Data
                    Addresses.Add((int) AddressType.SaveCheatFlags, 0x383bb8);
                    Addresses.Add((int) AddressType.SaveSram, 0x00000); // Need found
                    Addresses.Add((int) AddressType.SaveGameFlags, 0x383b88);
                    Addresses.Add((int) AddressType.SaveHoneycombFlags, 0x383cc0);
                    Addresses.Add((int) AddressType.SaveJiggyFlags, 0x383ca0);
                    Addresses.Add((int) AddressType.SaveMoveFlags, 0x37cd70);
                    Addresses.Add((int) AddressType.SaveMumboTokenFlags, 0x383cd0);
                    break;

                case GameVersion.USA_1_0:
                    Addresses.Add((int) AddressType.BetaMenu, 0x383080);
                    Addresses.Add((int) AddressType.Camera, 0x37d958);
                    Addresses.Add((int) AddressType.Inventory, 0x385f30);
                    Addresses.Add((int) AddressType.Player, 0x37c0e0);

                    // Runtime Data
                    Addresses.Add((int) AddressType.RtActorArrayPtr, 0x36e560);
                    Addresses.Add((int) AddressType.RtCollisionPtr, 0x3820b8);
                    Addresses.Add((int) AddressType.RtCurExit, 0x37e8f6);
                    Addresses.Add((int) AddressType.RtCurLevel, 0x383301);
                    Addresses.Add((int) AddressType.RtCurLevelEvents, 0x383328);
                    Addresses.Add((int) AddressType.RtCurLevelLookup, 0x36b818);
                    Addresses.Add((int) AddressType.RtCurProfile, 0x365e00);
                    Addresses.Add((int) AddressType.RtCurScene, 0x37e8f5);
                    Addresses.Add((int) AddressType.RtCurSceneEvents, 0x367000);
                    Addresses.Add((int) AddressType.RtIsCutscene, 0x367684);
                    Addresses.Add((int) AddressType.RtIsLoading, 0x37e8f4);
                    Addresses.Add((int) AddressType.RtTransitionState, 0x382438);
                    Addresses.Add((int) AddressType.RtVoxelArrayPtr, 0x381fa0);
                    Addresses.Add((int) AddressType.RtVoxelCountPtr, 0x381fc8);

                    // Save Data
                    Addresses.Add((int) AddressType.SaveCheatFlags, 0x3831d8);
                    Addresses.Add((int) AddressType.SaveSram, 0x383d20);
                    Addresses.Add((int) AddressType.SaveGameFlags, 0x3831a8);
                    Addresses.Add((int) AddressType.SaveHoneycombFlags, 0x3832e0);
                    Addresses.Add((int) AddressType.SaveJiggyFlags, 0x3832c0);
                    Addresses.Add((int) AddressType.SaveMoveFlags, 0x37c3a0);
                    Addresses.Add((int) AddressType.SaveMumboTokenFlags, 0x3832f0);
                    break;

                case GameVersion.USA_1_1:
                    Addresses.Add((int) AddressType.BetaMenu, 0x3822a0);
                    Addresses.Add((int) AddressType.Camera, 0x37cb58);
                    Addresses.Add((int) AddressType.Inventory, 0x385150);
                    Addresses.Add((int) AddressType.Player, 0x37b2e0);

                    // Runtime Data
                    Addresses.Add((int) AddressType.RtActorArrayPtr, 0x36d760);
                    Addresses.Add((int) AddressType.RtCollisionPtr, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurExit, 0x37daf6);
                    Addresses.Add((int) AddressType.RtCurLevel, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurLevelEvents, 0x383301);
                    Addresses.Add((int) AddressType.RtCurLevelLookup, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurProfile, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtCurScene, 0x37daf5);
                    Addresses.Add((int) AddressType.RtCurSceneEvents, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtIsCutscene, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtIsLoading, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtTransitionState, 0x00000); // Need found
                    Addresses.Add((int) AddressType.RtVoxelArrayPtr, 0x3811a0);
                    Addresses.Add((int) AddressType.RtVoxelCountPtr, 0x00000); // Need found

                    // Save Data
                    Addresses.Add((int) AddressType.SaveCheatFlags, 0x382428);
                    Addresses.Add((int) AddressType.SaveSram, 0x00000); // Need found
                    Addresses.Add((int) AddressType.SaveGameFlags, 0x3823f8);
                    Addresses.Add((int) AddressType.SaveHoneycombFlags, 0x382500);
                    Addresses.Add((int) AddressType.SaveJiggyFlags, 0x3824e0);
                    Addresses.Add((int) AddressType.SaveMoveFlags, 0x37b5a0);
                    Addresses.Add((int) AddressType.SaveMumboTokenFlags, 0x382510);
                    break;
            }
        }
    }
}