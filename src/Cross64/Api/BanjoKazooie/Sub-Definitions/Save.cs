﻿using System.Collections.Generic;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public class SaveDef
        {
            private const int SRAM_SIZE = 0x78;

            public readonly FlagsDef Flags = new();
            public readonly InventoryDef Inventory = new();
            public readonly BufferObject Moves = new(Addresses[(int) AddressType.SaveMoveFlags], 0x4);

            public readonly BufferObject NoteTotal =
                new(Addresses[(int) AddressType.Inventory] + (int) InventoryType.NoteTotals, 0xf);

            public readonly Dictionary<ProfileType, BufferObject> Sram = new();

            public SaveDef()
            {
                var addr = Addresses[(int) AddressType.SaveSram];
                Sram.Add(ProfileType.A, new BufferObject(addr + (SRAM_SIZE * 0), SRAM_SIZE));
                Sram.Add(ProfileType.B, new BufferObject(addr + (SRAM_SIZE * 1), SRAM_SIZE));
                Sram.Add(ProfileType.C, new BufferObject(addr + (SRAM_SIZE * 2), SRAM_SIZE));
                Sram.Add(ProfileType.Title, new BufferObject(addr + (SRAM_SIZE * 3), SRAM_SIZE));
            }
        }
    }
}