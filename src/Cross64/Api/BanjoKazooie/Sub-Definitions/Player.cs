﻿using System;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public class PlayerDef
        {
            private Emulator.Pointer _instance = Addresses[(int) AddressType.Player];
            private Emulator.Pointer addrAnim = Addresses[(int) AddressType.Player] - 0x1c0;

            private Utility.Vector3 oldPos = new();
            private Utility.Vector3 newPos = new();
            private Utility.Vector3 oldVel = new();
            private Utility.Vector3 newVel = new();

            public AnimalType Animal
            {
                get => (AnimalType) _instance.Read8(0x10a8);
                set => _instance.Write8((byte) value, 0x10a8);
            }

            public int AnimationFrame
            {
                get => (int) addrAnim.ReadP32(0x04);
                set => addrAnim.WriteP32(0x04, (uint) value);
            }

            public int AnimationId
            {
                get => (int) addrAnim.ReadP32(0x1c);
                set => addrAnim.WriteP32(0x1c, (uint) value);
            }

            public bool Exists => _instance.U32 != 0;

            public bool FlipFacing
            {
                get => _instance.Read8(0x7) == 1;
                set => _instance.Write8(value ? 1 : 0, 0x7);
            }

            public short ModelIndex
            {
                get => (short) _instance.Read16(0x4);
                set => _instance.Write16((ushort) value, 0x4);
            }

            public Emulator.Pointer ModelPointer
            {
                get => _instance.U32;
                set => _instance.Write32(value);
            }

            public int MovementState
            {
                get => (int) _instance.Read32(0x3c0);
                set => _instance.Write32((uint) value, 0x3c0);
            }

            public byte Opacity
            {
                get => _instance.Read8(0x6);
                set => _instance.Write8(value, 0x6);
            }

            public float Scale
            {
                get => _instance.ReadF(0xc);
                set => _instance.WriteF(value, 0xc);
            }

            public bool Visible
            {
                get => _instance.Read8(0x8) == 1;
                set => _instance.Write8(value ? 1 : 0, 0x8);
            }

            public BufferObject VisibleParts
            {
                get => new(0x1154, 0xf);
                set => _instance.WriteB(value.Bytes, 0x1154);
            }

            #region Position

            public Utility.Vector3 PrevPosition => new(oldPos);
            public float PrevPosX => oldPos.X;
            public float PrevPosY => oldPos.Y;
            public float PrevPosZ => oldPos.Z;

            public Utility.Vector3 Position
            {
                get => new(newPos);
                set
                {
                    newPos = value;
                    _instance.WriteB(value.ToArray(), 0x4c0);
                }
            }

            public float PosX
            {
                get => newPos.X;
                set
                {
                    newPos.X = value;
                    _instance.WriteF(value, 0x4c0);
                }
            }

            public float PosY
            {
                get => newPos.Y;
                set
                {
                    newPos.Y = value;
                    _instance.WriteF(value, 0x4c4);
                }
            }

            public float PosZ
            {
                get => newPos.Z;
                set
                {
                    newPos.Z = value;
                    _instance.WriteF(value, 0x4c8);
                }
            }

            #endregion

            #region Velocity

            public Utility.Vector3 PrevVelocity => new(oldVel);
            public float PrevVelX => oldVel.X;
            public float PrevVelY => oldVel.Y;
            public float PrevVelZ => oldVel.Z;

            public Utility.Vector3 Velocity
            {
                get => new(newVel);
                set
                {
                    newVel = value;
                    _instance.WriteB(value.ToArray(), 0x3d8);
                }
            }

            public float VelX
            {
                get => newVel.X;
                set
                {
                    newVel.X = value;
                    _instance.WriteF(value, 0x3d8);
                }
            }

            public float VelY
            {
                get => newVel.Y;
                set
                {
                    newVel.Y = value;
                    _instance.WriteF(value, 0x3dc);
                }
            }

            public float VelZ
            {
                get => newVel.Z;
                set
                {
                    newVel.Z = value;
                    _instance.WriteF(value, 0x3e0);
                }
            }

            #endregion

            #region Rotation

            public Utility.Vector3 Rotation
            {
                get => new(RotX, RotY, RotZ);
                set
                {
                    RotX = value.X;
                    RotY = value.Y;
                    RotZ = value.Z;
                }
            }

            public float RotX
            {
                get => _instance.ReadF(0x460);
                set => _instance.WriteF(value, 0x460);
            }

            public float RotY
            {
                get => _instance.ReadF(0x5b0);
                set => _instance.WriteF(value, 0x5b0);
            }

            public float RotZ
            {
                get => _instance.ReadF(0x5a0);
                set => _instance.WriteF(value, 0x5a0);
            }

            #endregion

            internal void OnTick()
            {
                oldPos = newPos;
                oldVel = newVel;

                newPos = new Utility.Vector3(_instance.ReadB(12, 0x4c0));
                newVel = new Utility.Vector3(_instance.ReadB(12, 0x3d8));
            }
        }
    }
}