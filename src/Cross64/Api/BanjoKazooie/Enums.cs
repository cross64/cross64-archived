﻿namespace Cross64.Api
{
    public partial class BanjoKazooie
    {
        public enum CMD : int
        {
            Empty = 0,
            Spawn = -1,
            Despawn = -2,
        }
        
        public enum VisiblePartsType
        {
            KazooieUpperBody1,
            KazooieTail,
            KazooieLegs,
            KazooieShoes,
            KazooieUpperBody2,
            KazooieBoots,
            EyeStateLeft = 0x6,
            EyeStateRight = 0xa
        }

        public enum AnimationType
        {
            Unknown = 0x0000,
            BanjoDucking = 0x0001,
            BanjoWalkingSlow = 0x0002,
            BanjoWalking = 0x0003,

            // 0x0004 X
            BanjoPunching = 0x0005,

            // 0x0006 X
            BanjoTalonTrotEnd = 0x0007,
            BanjoJumping = 0x0008,
            BanjoDying = 0x0009,
            BanjoClimbing = 0x000A,

            // 0x000B
            BanjoRunning = 0x000C,

            // 0x000D
            BanjoSkidding = 0x000E,
            BanjoHurt = 0x000F,
            BigbuttCharging = 0x0010,
            BanjoWonderwingRunning = 0x0011,

            // 0x0012 X
            // 0x0013 X
            // 0x0014 X
            BanjoTalonTrotWalking = 0x0015,
            BanjoTalonTrotStart = 0x0016,
            BanjoFlutter = 0x0017,
            BanjoFeatheryFlap = 0x0018,
            BanjoRatatatRap = 0x0019,
            BanjoRatatatRapStart = 0x001A,
            BanjoWonderwingJumping = 0x001B,
            BanjoBeakBarge = 0x001C,
            BanjoBeakBuster = 0x001D,

            // 0x001E X
            // 0x001F X
            // 0x0020 X
            BigbuttSkidding = 0x0021,
            BanjoWonderwingStart = 0x0022,
            BanjoWonderwing = 0x0023,
            YumyumHopping = 0x0024,

            // 0x0025 X
            BanjoTalonTrot = 0x0026,
            BanjoTalonTrotJumping = 0x0027,
            TermiteHurt = 0x0028,
            TermiteDying = 0x0029,
            BanjoShootingEgg = 0x002A,
            BanjoPoopingEgg = 0x002B,
            SnippetWalking = 0x002C,
            JinjoIdle = 0x002D,
            BanjoJiggyJig = 0x002E,
            JinjoHelp = 0x002F,
            HeldJiggyJiggyJig = 0x0030,
            JinjoHopping = 0x0031,
            BigbuttAttacking = 0x0032,
            BigbuttEating = 0x0033,
            BigbuttDying = 0x0034,
            BigbuttAlerted = 0x0035,
            BigbuttWalking = 0x0036,

            // 0037
            BanjoFlying = 0x0038,
            BanjoSwimmingSurface = 0x0039,

            // 003A X
            // 003B X
            BanjoDiving = 0x003C,
            BanjoShockSpringJump1 = 0x003D,
            BanjoFlyingCrash = 0x003E,
            BanjoSwimmingUnderwaterB = 0x003F,
            BanjoWadingBootsStart = 0x0040,
            BanjoWadingBoots = 0x0041,
            BanjoWadingBootsWalking = 0x0042,
            BanjoBeakbombStart = 0x0043,
            BanjoTurboTalonTrainers = 0x0044,
            BanjoFlyingStart = 0x0045,

            // 0046 X
            BanjoBeakbomb = 0x0047,
            BanjoShockSpringJumpStart = 0x0048,
            BanjoShockSpringJump2 = 0x0049,

            // 004A X
            BanjoFlapFlip = 0x004B,
            BanjoFlapFlipTransistion = 0x004C,
            BanjoHurt2 = 0x004D,
            MmMudhutSmashing = 0x004E,
            BanjoWaterSplash = 0x004F,

            // 0050 X
            CongaIdle = 0x0051,
            CongaHurt = 0x0052,
            CongaDefeated = 0x0053,
            CongaThrowing = 0x0054,
            CongaBeatingChest = 0x0055,
            CongaRaisingArms = 0x0056,
            BanjoSwimmingUnderwater = 0x0057,
            BanjoSwimmingUnderwaterA = 0x0058,
            BanjoSlidingBack = 0x0059,
            BanjoSlidingFront = 0x005A,
            ChimpyHopping = 0x005B,
            ChimpyIdle = 0x005C,
            ChimpyWalking = 0x005D,
            TickerIdle = 0x005E,
            TickerWalking = 0x005F,
            TermiteJumping = 0x0060,
            BanjoFlapFlipEnd = 0x0061,
            GrublinIdle = 0x0062,
            GrublinWalking = 0x0063,
            GrublinJumping = 0x0064,
            BeehiveDying = 0x0065,
            BanjoTalonTrotHurt = 0x0066,
            WadingBootsIdle = 0x0067,
            BanjoFalling = 0x0068,
            BanjoOnTumblar = 0x0069,
            MumboSleeping = 0x006A,
            MumboWalking = 0x006B,
            MumboIdle = 0x006C,
            MumboTransforming = 0x006D,
            MumboUnknown6e = 0x006E,
            BanjoIdle = 0x006F,
            BanjoSwimmingUnderwater2 = 0x0070,
            BanjoSwimmingSlow = 0x0071,
            BanjoCarryingItem = 0x0072,
            BanjoCarryingItemWalking = 0x0073,

            // 0074 X
            // 0075 X
            // 0076
            BanjoLosingMinigame = 0x0077,
            SnackerSwimming = 0x0078,
            CsConcertMumboPlaying = 0x0079,
            CsConcertBanjoAngry = 0x007A,
            CsConcertBanjoPlaying = 0x007B,
            CsConcertBanjoEnd = 0x007C,
            CsConcertTootyStart = 0x007D,
            CsConcertBanjoStart = 0x007E,
            CsConcertCutscene = 0x007F,
            CsConcertTimer = 0x0080,
            CsConcertUnknown0x81 = 0x0081,
            CsConcertMumboDancing = 0x0082,
            CsConcertTootyDancing = 0x0083,
            TootyHopping = 0x0084,

            // 0085
            // 0086 X
            // 0087 X
            // 0088 X
            // 0089 X
            // 008A X
            // 008B X
            RarewareLogoFalling = 0x008C,

            // 008D X
            // 008E X
            NintendoCubeWalking = 0x008F,
            NintendoCubeShrugging = 0x0090,
            CsConcertFrogHopping = 0x0091,
            ShrapnelChasing = 0x0092,
            TootyRunning = 0x0093,
            GrublinDying = 0x0094,
            BanjoIdleKazooieTaunt = 0x0095,
            SnippetRecovering = 0x0096,
            SnippetDying = 0x0097,

            // 0098
            // 0099
            RipperIdle = 0x009A,
            RipperChasing = 0x009B,

            // 009C X
            NibblyChasing = 0x009D,
            TeeheeIdle = 0x009E,
            TeeheeAlerted = 0x009F,
            PumpkinWalking = 0x00A0,
            PumpkinJumping = 0x00A1,
            CongaThrowing2 = 0x00A2,
            NapperSleeping = 0x00A3,
            NapperLookingAround = 0x00A4,
            NapperWalking = 0x00A5,
            NapperAlerted = 0x00A6,
            MotzhandIdle = 0x00A7,
            MotzhandPlaying = 0x00A8,
            Pot = 0x00A9,
            YumyumIdle = 0x00AA,
            YumyumEating = 0x00AB,
            TeeheeChasing = 0x00AC,
            NibblyFlyingStart = 0x00AD,
            NibblyIdle = 0x00AE,

            // 00AF X
            BanjoFalling2 = 0x00B0,
            BanjoClimbing2 = 0x00B1,
            BanjoClimbingFreeze = 0x00B2,
            ChompaIdle = 0x00B3,
            ChompaAttacking = 0x00B4,
            BlubberWalking = 0x00B5,
            BlubberCrying = 0x00B6,
            BlubberDancing = 0x00B7,
            BlubberRunning = 0x00B8,
            BanjoDrowning = 0x00B9,

            // 00BA X
            // 00BB X
            LockupIdle = 0x00BC,
            NipperVulnerable = 0x00BD,
            NipperHurt = 0x00BE,
            NipperAttacking = 0x00BF,
            NipperIdle = 0x00C0,

            // 00C1  Littlebounce
            // 00C2  Wobblybounce
            ClankerIdle = 0x00C3,
            ClankerMouthOpen = 0x00C4,
            GrabbaAppearing = 0x00C5,
            GrabbaHiding = 0x00C6,
            GrabbaIdle = 0x00C7,
            GrabbaDefeated = 0x00C8,
            MagicCarpet = 0x00C9,
            GloopSwimming = 0x00CA,
            GloopBlowingBubble = 0x00CB,
            BanjoBeakbombEnd = 0x00CC,

            // 00CD Green Grate near RBB… (4B1)
            RubeeIdle = 0x00CE,
            HistupRaised = 0x00CF,
            HistupRaising = 0x00D0,
            HistupInPot = 0x00D1,
            BanjoGettingUp = 0x00D2, // ;-)
            BanjoBeakbombHurt = 0x00D3,
            SwitchDown = 0x00D4,
            SwitchUp = 0x00D5,
            TurboTalonTrainersIdle = 0x00D6,

            // 00D7
            // 00D8
            GobiIdle = 0x00D9,
            GobiPullingOnChain = 0x00DA,
            FlibbitHopping = 0x00DB,
            GobisRopePulling = 0x00DC,
            GobisRopeIdle = 0x00DD,

            // 00DE X
            RubeePettingToots = 0x00DF,
            CrocWalking = 0x00E0,
            CrocIdle = 0x00E1,
            HistupPeeking = 0x00E2,
            RubeeIdle2 = 0x00E3,
            RubeePlaying = 0x00E4,
            GrabbaShadowSpawning = 0x00E5,
            GrabbaShadowIdle = 0x00E6,
            GrabbaShadowHiding = 0x00E7,
            GrabbaShadowDefeated = 0x00E8,
            SlappaAppearing = 0x00E9,
            SlappaMoving = 0x00EA,
            SlappaSlapping = 0x00EB,
            SlappaGettingUp = 0x00EC,
            AncientOneEnterExit = 0x00ED,
            SlappaDying = 0x00EE,
            SlappaHurt = 0x00EF,
            MinijinxyEating = 0x00F0,
            MagicCarpet2 = 0x00F1,

            // 00F2 X
            // 00F3 X
            GobiRelaxing = 0x00F4,

            // 00F5
            BanjoIdlePullingKazooie = 0x00F6,
            GobiHappy = 0x00F7,
            GobiRunning = 0x00F8,
            BuzzbombFlying = 0x00F9,
            FlibbitIdle = 0x00FA,
            FlibbitTurning = 0x00FB,
            GobiGivingWater = 0x00FC,
            GobiGettingUp = 0x00FD,
            TrunkerShort = 0x00FE,
            TrunkerGrowing = 0x00FF,

            // 0100
            TanktupHeadIdle = 0x0101,
            TanktupHeadPounded = 0x0102,
            TanktupLegBackLeftHit = 0x0103,
            TanktupLegFrontLeftHit = 0x0104,
            TanktupLegFrontRightHit = 0x0105,
            TanktupLegBackRightHit = 0x0106,
            TanktupSpawningJiggy = 0x0107,
            SirSlushIdle = 0x0108,
            SirSlushAttacking = 0x0109,

            // 010A X
            // 010B
            BanjoDuckingTurning = 0x010C,
            BanjoFlyingHit = 0x010D,
            BuzzbombPreparingCharge = 0x010E,
            BuzzbombCharging = 0x010F,
            BuzzbombFalling = 0x0110,
            BuzzbombDying = 0x0111,
            FlibbitDyingStart = 0x0112,
            FlibbitDyingEnd = 0x0113,

            // 0114 X
            // 0115 X
            BanjoDuckingLooking = 0x0116,

            // 0117 Jellyfish (Unknown) 0x117
            // 0118 X
            // 0119 X
            // 011A X
            BanjoCarryingItemThrowing = 0x011B,
            CrocJumping = 0x011C,
            CrocHurt = 0x011D,
            CrocDying = 0x011E,
            WalrusIdle = 0x011F,
            WalrusWalking = 0x0120,
            WalrusJumping = 0x0121,
            CrocBiting = 0x0122,
            CrocEatWrongThing = 0x0123,
            MrVileEating = 0x0124,
            YumblieAppearing = 0x125,
            YumblieLeaving = 0x0126,
            YumblieIdle = 0x0127,
            GrumblieAppearing = 0x0128,
            GrumblieLeaving = 0x0129,
            GrumblieIdle = 0x012A,
            TiptupLookingAroundShrugging = 0x012B,
            TiptupTapping = 0x012C,
            TiptupChoirMemberIdle = 0x012D,
            TiptupChoirMemberSinging = 0x012E,
            TiptupChoirMemberHurt = 0x012F,
            JinjoCirclingStart = 0x0130,
            JinjoCirclingEnd = 0x0131,
            FloatsamBouncing = 0x0132,
            NipperDying = 0x0133,

            // 0134
            // 0135
            // 0136
            GrimletAttacking = 0x0137,
            TextBackdropAppearing = 0x0138,
            BottlesDisappearing = 0x0139,
            BottlesAppearing = 0x013A,
            BottlesScratching = 0x13B,
            BottlesMolehillIdle1 = 0x013C,
            BottlesMolehillIdle2 = 0x013D,
            SnorkelSwimming = 0x013E,
            SnorkelStuck = 0x013F,

            // 0140
            // 0141
            RbbAnchorIdle = 0x0141,
            RbbAnchorRising = 0x0142,
            Button = 0x0143,
            JinxySniffing = 0x0144,
            JinxySneezing = 0x0145,
            BossBoomboxAppearing = 0x0146,
            BoomboxHopping = 0x0147,
            BoomboxExploding = 0x0148,
            BanjoFallDamage = 0x0149,
            BanjoListening = 0x014A,
            CroctusIdle = 0x014B,
            BoggyIdle = 0x014C,
            BoggyHit = 0x014D,
            BoggyLayingDown = 0x14E,
            BoggyRunning = 0x014F,
            BoggyOnSledIdle = 0x0150,
            RaceFlagHit = 0x0151,
            RaceFlagIdle = 0x0152,
            GoldChestSpawning = 0x0153,
            SnackerEating = 0x0154,
            SnippetGetUp = 0x0155,
            MutieSnippetWalking = 0x0156,
            MutieSnippetUpsidedownStart = 0x0157,
            MutieSnippetUpsidedown = 0x0158,
            MutieSnippetUpsidedownEnd = 0x0159,
            GrilleChompaAttacking = 0x015A,
            GrilleChompaDying = 0x015B,
            WhiplashIdle = 0x015C,
            WhiplashAttacking = 0x015D,

            // 015E
            CsConcertBanjoOffScreen = 0x015F,
            CsConcertBugCrawling = 0x0160,

            // 0161
            TootsIdle = 0x0162,
            CsConcertBuzzbombHittingLogo = 0x0163,

            // 0164
            BeehiveIdle = 0x0165,
            GoldChestBouncing = 0x0166,

            // 0167 Banjo/MoveVeryLittle (used in small cutscenes)
            // 0168
            // 0169
            // 016A
            SnarebearSnapping = 0x016B,
            SnarebearIdle = 0x016C,
            TwinklyBoxOpening = 0x016D,
            MumboReclining = 0x016E,
            ZubbaFlying = 0x016F,
            ZubbaIdle = 0x0170,
            ZubbaFalling = 0x0171,
            ZubbaLanding = 0x0172,
            FlowerSpringGrowing = 0x0173,
            FlowerSummerGrowing = 0x0174,
            FlowerAutumnGrowing = 0x0175,
            GobiYawning = 0x0176,
            GobiSleeping = 0x0177,
            TwinklyAppearing = 0x0178,
            BoggyOnSledTaunting = 0x0179,
            BoggyOnSledLookingBack = 0x017A,

            // 017B
            TwinklyTwinkling = 0x017C,
            BoogysChildrenHappy = 0x017D,
            BoogysChildrenSad = 0x017E,
            MumboSweeping = 0x017F,
            MumboRotation = 0x0180,
            FlowerSpringIdle = 0x0181,
            FlowerSummerIdle = 0x0182,
            FlowerAutumnIdle = 0x0183,
            BigCluckerAttackingShort = 0x0184,
            BigCluckerAttackingLong = 0x0185,
            BigCluckerDying = 0x0186,

            // 0187
            PumpkinDying = 0x0188,
            FloatsamDying = 0x0189,
            FpPresentIdle = 0x018A,

            // 018B X
            // 018C X
            // 018D
            // 018E
            EyrieSpringFallingAsleep = 0x018F,
            EyrieSpringSleeping = 0x0190,
            EyrieSummerIdle = 0x0191,
            EyrieSummerGrowing = 0x0192,
            EyrieSummerFallingAsleep = 0x0193,
            EyrieSummerSleeping = 0x0194,
            EyrieAutumnIdle = 0x0195,
            EyrieAutumnGrowing = 0x0196,
            EyrieAutumnFallingAsleep = 0x0197,
            EyrieAutumnSleeping = 0x0198,
            EyrieWinterIdle = 0x0199,
            EyrieWinterFlying = 0x019A,
            BanjoTransforming = 0x019B,
            WalrusHurt = 0x019C,
            WalrusDying = 0x019D,
            WalrusOnSled = 0x019E,
            WalrusLostRaceStart = 0x019F,

            // 01A0 Unknown Dying (0x1A0)
            SledIdle = 0x01A1,
            NabnutSleeping = 0x01A2,
            NabnutIdle = 0x01A3,
            NabnutEating = 0x01A4,

            // 01A5 X
            GnawtyIdle = 0x01A6,
            GnawtyHappy = 0x01A7,
            GnawtyWalking = 0x01A8,
            WalrusLostRace = 0x01A9,
            BoggyWonRace = 0x01AA,
            BoggyLostRace = 0x01AB,
            WozzaWithJiggyIdle = 0x01AC,
            WozzaThrowingJiggy = 0x01AD,
            WozzaWalking = 0x01AE,
            TwinklyMuncherDying = 0x01AF,
            TwinklyMuncherAppearing = 0x01B0,
            TwinklyMuncherIdle = 0x01B1,
            TwinklyMuncherMunching = 0x01B2,
            WozzaBeforeStop = 0x01B3,
            WozzaScared = 0x01B4,
            WozzaGivingJiggy = 0x01B5,
            WozzaHalfThrowFreeze = 0x01B6,
            CsIntroGreenMistIdle = 0x01B7,
            CsIntroDoorOpening = 0x01B8,
            CsIntroGruntyIdle = 0x01B9,

            // 01BA
            CsIntroGruntyPickingNose = 0x01BB,

            // 01BC
            CsIntroGruntyAngryAtDingpot = 0x01BD,
            CsIntroGruntyThrowingBooger = 0x01BE,
            CsIntroGruntyShockedConfused = 0x01BF,
            CsIntroGruntyWalking = 0x01C0,

            // 01C1
            CsIntroDoorClosing = 0x01C2,

            // 01C3
            CsIntroGruntysBroomFlying = 0x01C4,
            GruntyFlying = 0x01C5,

            // 01C6
            CsIntroBanjoSleeping = 0x01C7,
            CsIntroBanjoWakingUp = 0x01C8,
            CsIntroBedsheetsBanjoSleeping = 0x01C9,
            CsIntroBedsheetsBanjoAwake = 0x01CA,
            CsIntroKazooieOnCoatRackAppearing = 0x01CB,

            // 01CC
            CsIntroKazooieOnCoatRackIdle = 0x01CD,
            CsIntroCurtain = 0x01CE,
            CsIntroKazooieOnCoatRackUneasy = 0x01CF,
            TootyIdle = 0x01D0,

            // 01D1
            // 01D2
            CsIntroKazooieOnCoatRackWakingBanjo = 0x01D3,
            CsIntroKazooieOnCoatRackFalling = 0x01D4,
            TootyScare = 0x01D5,
            GrublinWalking2 = 0x01D6,
            GrublinAlerted = 0x01D7,
            GrublinChasing = 0x01D8,
            GrublinDying2 = 0x01D9,
            SnippetIdle = 0x01DA,
            MutieSnippetIdle = 0x01DB,
            BeeFlying = 0x01DC,
            BeeWalking = 0x01DD,
            BeeIdle = 0x01DE,
            BeeUnknown0x1df = 0x01DF,
            BeeHurt = 0x01E0,
            BeeDying = 0x01E1,
            BeeJumping = 0x01E2,
            GvBrickWallSmashing = 0x01E3,
            LimboIdle = 0x01E4,
            LimboAlerted = 0x01E5,
            LimboChasing = 0x01E6,
            LimboBreaking = 0x01E7,
            LimboRecovering = 0x01E8,
            MummumIdle = 0x01E9,
            MummumCurling = 0x01EA,
            MummmumUncurling = 0x01EB,

            // 01EC
            RipperHurt = 0x01ED,
            RipperDying = 0x01EE,

            // 01EF
            // 01F0  Web (Floor)
            // 01F1  Web Dying (Floor)
            // 01F2  Web (Wall)
            // 01F3  Web Dying (Wall)
            ShrapnelIdle = 0x01F4,

            // 01F5
            // Jiggy Transition
            // 01F6
            // 01F7  Kazooie Feathers Poof (End intro)
            // 01F8  Bottles PointAtGrunty
            // 01F9  Tooty Confused
            SexyGruntyWalking = 0x01FA,
            SexyGruntyCheckingSelfOut = 0x01FB,
            UglyTootyWalking = 0x01FC,
            UglyTootyPunching = 0x01FD,

            // 01FE  Machine Door Opening
            // 01FF  Machine Door Closing
            // 0200  Static Machine Door Up
            KlungoWalking = 0x0201,
            KlungoPushingButton = 0x0202,

            // 0203 X
            GruntyFalling = 0x0204,

            // 0205  Dingpot wap
            // 0206  Dingpot
            // 0207  Grunty Crammed in Machine
            RoystenIdle = 0x0208,
            CuckooClockIdle = 0x0209,
            CuckooClockChiming = 0x020A,

            // 020B  Grunty Falling
            // 020C
            KlungoPullingLever = 0x020D,

            // 020E Machine Lever down
            KlungoLaughing = 0x020F,

            // 0210  Machine
            // 0211
            WarpCauldronActivating = 0x0212,
            WarpCauldronSleeping = 0x0213,
            WarpCauldronIdle = 0x0214,
            WarpCauldronTeleporting = 0x0215,
            WarpCauldronRejecting = 0x0216,

            // 0217  Transform Pad
            // 0218
            // 0219
            EyrieSummerEating = 0x021A,
            EyrieAutumnEating = 0x021B,

            // 021C
            EyrieFlying = 0x021D,
            EyrieWinterPoopingJiggy = 0x021E,

            // 021F X
            // 0220  Sir. Slush
            // 0221  Wozza (in cave)
            // 0222  Boggy Sleeping
            TopperIdle = 0x0223,
            TopperDying = 0x0224,

            // 0225  Colliwobble
            // 0226  Bawl
            // 0227  Bawl Dying
            // 0228
            // 0229  Whipcrack Attacking
            // 022A  Whipcrack
            NabnutFat = 0x022B,
            NabnutCrying = 0x022C,
            NabnutHappy = 0x022D,
            NabnutIdle2 = 0x022E,
            NabnutRunning = 0x022F,
            MrsNabnutSleeping = 0x0230,
            NabnutsBedsheets = 0x0231,

            // 0232  X
            // 0233  Chinker
            // 0234  Snare-Bear (Winter)
            // 0235  Sarcophagus (GV Lobby)
            PumpkinHurt = 0x0236,

            // 0237  Twinkly Present
            LoggoIdle = 0x0238,

            // 0239  Leaky Hop
            // 023A  Gobi Fly
            // 023B  Gobi Fly Prepare Attack
            // 023C  Gobi Fly Charge
            // 023D  Gobi Fly Dying
            // 023E Portrait Chompa (Picture Monster)
            // 023F  Portrait
            LoggoFlushing = 0x0240,

            // 0241
            // 0242  Gobi Relaxing
            GrublinHoodIdle = 0x0243,
            GrublinHoodAlerted = 0x0244,
            GrublinHoodChasing = 0x0245,
            GrublinHoodDying = 0x0246,

            // 0247
            // 0248
            // 0249
            FsBanjoCookingIdle = 0x024A,
            FsBanjoCookingSelected = 0x024B,
            FsBanjoCookingSpin = 0x024C,
            FsBanjoSleepingIdle = 0x024D,
            FsBanjoSleepingSelected = 0x024E,
            FsBanjoSleepingSpring = 0x024F,
            FsBanjoPlayingGameboyIdle = 0x0250,
            FsBanjoPlayingGameboyThumbsup = 0x0251,
            FsBanjoPlayingGameboySpring = 0x0252,
            BigbuttHurt = 0x0253,
            BigbuttDefeatedStart = 0x0254,
            BigbuttRecovering = 0x0255,

            // 0256
            // 0257  Grunty Green Spell (flying)
            // 0258  Grunty Hurt
            // 0259  Grunty Hurt
            // 025A  Grunty Fireball Spell
            AcornIdle = 0x025B,

            // 025C  Grunty Phase 1 Swooping
            // 025D  Grunty Entring Final Phase
            // 025E  Grunty Phase 1 Vulnerable
            // 025F  Grunty
            // 0260  Grunty Fireball Spell
            // 0261  Grunty Green Spell
            JinjoStatueRising = 0x0262,

            // 0263  Grunty Fall off Broom
            JinjoStatueActivating = 0x0264,
            JinjoStatueIdle = 0x0265,

            // 0266  Grunty/Falling down tower
            // 0267  Big Blue Egg
            // 0269  Big Red Feather
            // 026A  Big Gold Feather
            BrentildaIdle = 0x026B,
            BrentildaHandsOnHips = 0x026C,
            GruntlingIdle = 0x026D,
            GruntlingAlerted = 0x026E,
            GruntlingChasing = 0x026F,
            GruntlingDying = 0x0270,

            // 0271  Door of Grunty
            CheatoIdle = 0x0272,
            SnackerHurt = 0x0273,
            SnackerDying = 0x0274,

            // 0275  Jinjonator Activating
            // 0276  Jinjonator Charging
            // 0277
            // 0278  Jinjonator Recoil
            // 0279  Grunty JawDrop > Shiver
            // 027A  Grunty Hurt by Jinjonator
            // 027B  Jinjonator? (spin spin spin, stop far way, shake)
            // 027C  Jinjonator Charging
            // 027D  Jinjonator Final Hit
            // 027E  Jinjonator Taking Flight
            // 027F  Jinjonator Circling
            // 0280  Jinjonator Attacking
            // 0281  Wishy-Washy-Banjo 'Doooohh….'
            // 0282  Banjo Unlocking Note Door
            // 0283  Grunty Chattering Teeth
            // 0284  PRESS START Appearing
            // 0285  PRESS START
            // 0286  NO CONTROLLER Appearing
            // 0287  NO CONTROLLER
            FlibbitHurt = 0x0288,
            GnawtySwimming = 0x0289,
            FfWashingMachine = 0x028A,

            // 028B Grunty
            GruntyDoll = 0x028C,
            // 028D  Grunty Walking
            // 028E  Tooty Looking Around
            // 028F  Dingpot
            // 0290  Dingpot Shooting
            // 0291  Mumbo Flipping Food
            // 0292  Food Flipping
            // 0293  Banjo Drinking
            // 0294  Mumbo Screaming
            // 0295  Banjo's Chair Breaking
            // 0296  Bottles Eating corn
            // 0297  Mumbo Skidding/Giving Flower
            // 0298
            // 0299  Bottles Falling off chair
            // 029A  Banjo Drunk
            // 029B  Kazooie Hits Banjo
            // 029C  Yellow Jinjo Waving & Whistling
            // 029D  Melon Babe Walking
            // 029E  Blubber On Jetski
            // 029F  Blubber Cheering on JetSki
            // 02A0  Curtains (Bottles Bonus)
            // 02A1  Banjo's Hand Dropping Jiggy
            // 02A2  Banjo's Hand
            // 02A3  Banjo's Hand Turning Jiggy (Right)
            // 02A4  Banjo's Hand Turning Jiggy (Left)
            // 02A5  Banjo's Hand Grabbing Jiggy
            // 02A6  Banjo's Hand Thumbs Up
            // 02A7  Banjo's Hand Placing Jiggy
            // 02A8  Banjo's Hand Thumbs Down
            // 02A9  Nibbly Falling
            // 02AA  Nibbly Dying
            // 02AB  Tee-Hee Dying
            // 02AC  Grunty Upset
            // 02AD  Grunty Looking
            // 02AE  Tree Shaking (Mumbo)
            // 02AF  Mumbo Sliding down tree
            // 02B0  Mumbo on tree (waving pictures)
            // 02B1  Mumbo falling from tree
            // 02B2  Bottles Eating watermelon
            // 02B3  Mumbo Hit by Coconuts
            // 02B4  Mumbo shake head sitting down
            // 02B5  Mumbo Jumping > Running
            // 02B6  Klungo Pushing rock
            // 02B7  Klungo Tired
            // 02B8  Tooty Drinking
            // 02B9  Grunty's Rock
            // 02BA  Kazooie Talking
            // 02BB  Mumbo Running After Melon Babe
            // 02BC  Mumbo Talking
            // 02BD
            // 02BE
            // 02BF
            // 02C0  Piranha Dying
            // 02C1
            // 02C2
            // 02C3
            // 02C4
            // 02C5  Grunty Preparing charge
            // 02C6  Mumbo's Hand
            // 02C7  Mumbo's Hand Appearing
            // 02C8  Mumbo's Hand Leaving
        }

        public enum ActorType
        {
            Bigbutt = 0x0004,
            Ticker = 0x0005,
            Grublin = 0x0006,
            Mumbo0x0007 = 0x0007,
            MmConga = 0x0008,
            MmHut = 0x0009,
            Fish = 0x000A,
            ShockSpringPad = 0x000B,
            MudHut = 0x000C,
            WoodDemolished = 0x000D,
            Bull2 = 0x000E,
            MmChimpy = 0x000F,
            MmJuJuHitbox = 0x0011,
            Beehive = 0x0012,
            MmCongaOrange = 0x0014,
            Shadow = 0x0017,
            Leaky = 0x001E,
            Oyster = 0x0024,
            MmmCemetaryPot = 0x0025,
            ClimbablePole = 0x0026,
            MmCollectableOrange = 0x0029,
            TtcGoldBullion = 0x002A,
            TurboTrainers = 0x002C,
            MumboToken = 0x002D,
            Phantom = 0x0039,
            MmmMotzand = 0x003A,
            BigKey = 0x003C,
            RbbPropeller1 = 0x003D,
            RbbPropeller2 = 0x003E,
            RbbPropeller3 = 0x003F,
            RbbPropeller4 = 0x0040,
            RbbPropeller5 = 0x0041,
            RbbPropeller6 = 0x0042,
            Screw = 0x0043,
            Rock = 0x0044,
            Unknown0x0045 = 0x0045,
            Jiggy = 0x0046,
            EmptyHoneycombPiece = 0x0047,
            ExtraLife = 0x0049,
            WoodExplosion = 0x004A,
            Explosion2 = 0x004B,
            Steam = 0x004C,
            Steam2 = 0x004D,
            Sparkles = 0x004E,
            Sparkles2 = 0x004F,
            Honeycomb = 0x0050,
            MusicNote = 0x0051,
            Egg = 0x0052,
            TtcRedArrow = 0x0053,
            TtcRedQuestionMark = 0x0054,
            TtcRedX = 0x0055,
            Explosion = 0x0056,
            MmOrangePad = 0x0057,
            MmJujuTotemPole = 0x0059,
            JiggyInHand = 0x005A,
            Egg1 = 0x005B,
            CollectableJinjoYellow = 0x005E,
            CollectableJinjoOrange = 0x005F,
            CollectableJinjoBlue = 0x0060,
            CollectableJinjoPink = 0x0061,
            CollectableJinjoGreen = 0x0062,
            WaterLevelSwitch = 0x0064,
            WadingBoots = 0x0065,
            Unknown0x0066 = 0x0066,
            TtcSnippet = 0x0067,
            Snacker = 0x0068,
            TtcYumyum = 0x0069,
            Unknown0x006c = 0x006C,
            BanjoDoor = 0x006D,
            CsConcertBk = 0x008E,
            CsConcertMumbo = 0x008F,
            YellowJinjo1 = 0x0090,
            YellowJinjo2 = 0x0091,
            CsConcertTooie = 0x0092,
            CsNintendoCube = 0x0093,
            CsRarewareLogo = 0x0094,
            YellowJinjo3 = 0x0095,
            YellowJinjo4 = 0x0096,
            YellowJinjo5 = 0x0097,
            YellowJinjo6 = 0x0098,
            Unknown0x0099 = 0x0099,
            Unknown0x009a = 0x009A,
            CsConcertBull = 0x009B,
            CsConcertFrog = 0x009C,

            // 	{id=0x009D, name="Bull"},
            Grunty1 = 0x009E,
            YellowJinjo7 = 0x009F,

            // 	{id=0x00A0, name="Banjo-Kazooie Sign"},
            Unknown0x00a1 = 0x00A1,

            // 	{id=0x00A2, name="Fire Sparkle"},
            YellowJinjo8 = 0x00A3,
            Unknown0x00a4 = 0x00A4,
            CsConcertBuzzbomb = 0x00A5,
            BgsYumblie = 0x00A6,
            CsBbqMumbo = 0x00A7,
            CsBbq1 = 0x00A8,
            CsBbq2 = 0x00A9,
            CsIntroDingpot1 = 0x00AA,
            CsIntroGruntyArms = 0x00AB,
            Grunty2 = 0x00AC,
            CsIntroNibbly = 0x00AD,
            CsIntroDingpot2 = 0x00AE,
            CsIntroGreenMist = 0x00AF,
            CsIntroBottles = 0x00B0,
            CsIntroBottlesMolehill = 0x00B1,
            CsIntroMachineRoomDoor = 0x00B2,
            CsIntroTooty = 0x00B4,
            CsIntroGruntysBroomstick = 0x00B5,
            CsIntroGruntyOnBroomstick = 0x00B6,
            YellowJinjo9 = 0x00B7,
            CsIntroBanjoSleeping = 0x00B8,
            CsIntroBed = 0x00B9,
            YellowJinjo10 = 0x00BA,
            CsIntroKazooieOnCoatRack = 0x00BB,
            CsIntroBanjosCurtains = 0x00BC,
            CsIntroCurtainsPurpleCheckered = 0x00BC,
            CsIntroBanjosDoor = 0x00BD,

            // 	{id=0x00BD, name="Door, Yellow 2D"},
            YellowJinjo11 = 0x00BE,
            YellowJinjo12 = 0x00BF,

            // 	{id=0x00C0, name="Unknown 0x00C0"},
            // 	{id=0x00C1, name="Jiggy Cutout Board"},
            CsGameoverSexyGrunty = 0x00C2,
            CsGameoverUglyTooty = 0x00C3,

            // 	{id=0x00C5, name="Chimpy's Tree Stump"},
            MmmRipper = 0x00C7,

            // 	{id=0x00C8, name="Boggy on Sled 2"},
            MmmTeehee = 0x00CA,

            // 	{id=0x00CB, name="Barrel Top"},
            // 	{id=0x00E4, name="Flight Pad"},
            // 	{id=0x00E4, name="Fly Pad"},
            CcGloop = 0x00E6,

            // 	{id=0x00E7, name="Unknown 0x00E7"},
            BgsTanktup = 0x00E8,
            BgsTanktupHead = 0x00E9,
            BgsTanktupLeg1 = 0x00EA,
            BgsTanktupLeg2 = 0x00EB,
            BgsTanktupLeg3 = 0x00EC,
            Egg2 = 0x00EE,
            Egg3 = 0x00EF,
            Egg4 = 0x00F0,

            // 	{id=0x00F1, name="Leaf"},
            TtcBlackSnippet = 0x00F2,

            // 	{id=0x00F3, name="Unknown 0x00F3"},
            CcMutieSnippet = 0x00F5,

            // 	{id=0x00F6, name="Big Alligator"},
            // 	{id=0x0101, name="Side of Rock"},
            // 	{id=0x0102, name="Side of Rock"},
            // 	{id=0x0108, name="Shadow 0x0108"},
            // 	{id=0x0109, name="Big Door"},
            // 	{id=0x010A, name="Main Door"},
            MmmCellarDoor = 0x010B,
            //   {id=0x010C, name="Locked Gade"},

            // 	{id=0x010D, name="Gate"},
            // 	{id=0x010E, name="Hatch"},
            MmmChurchDoor = 0x0114,
            TtcBlubber = 0x0115,

            // 	{id=0x0116, name="Bullseye"},
            TtcNipperShell = 0x0117,

            // 	{id=0x0118, name="Grey Slappa"},
            GvMagicCarpet1 = 0x0119,

            // 	{id=0x011A, name="Sarcopphagus"},
            GvRubee = 0x011B,
            GvHistup = 0x011C,

            // 	{id=0x011D, name="Pot"},
            // 	{id=0x011E, name="Hand Shadows"},
            // 	{id=0x011F, name="Dirt flies up"},
            // 	{id=0x0120, name="Purple Slappe"},
            // 	{id=0x0121, name="Big Jinxy Head"},
            // 	{id=0x0122, name="Square Shadow"},
            GvMagicCarpet2 = 0x0123,

            // 	{id=0x0124, name="Sir Slush, Snowman"},
            // 	{id=0x0124, name="Snowman"},
            // 	{id=0x0125, name="Snowball"},
            // 	{id=0x0126, name="Snowman's Hat"},
            // 	{id=0x0129, name="Red Feather"},
            // 	{id=0x012C, name="Bottles Mound"},
            // 	{id=0x012C, name="Mole Hill"},
            GvGobi1 = 0x012E,

            // 	{id=0x012F, name="Bamboo"},
            // 	{id=0x0130, name="Jinxy's Legs"},
            GvGobi2 = 0x0131,
            GvTrunker = 0x0132,

            // 	{id=0x0133, name="Flibbit"},
            // 	{id=0x0134, name="Buzzbomb"},
            // 	{id=0x0134, name="Dragon Fly"},
            GvGobi3 = 0x0135,
            BgsFlibbetYellow = 0x0137,

            // 	{id=0x0139, name="Yumblie"},
            BgsMrVile = 0x013A,
            RbbFloatsam = 0x013B,

            // 	{id=0x013E, name="Nothing?"},
            GvSunSwitch = 0x013F,
            GvSunDoor = 0x0140,
            GvStarHatch = 0x0142,
            GvKazooieDoor = 0x0143,
            GvStarSwitch = 0x0144,
            GvHoneycombSwitch = 0x0145,
            GvKazooieTarget = 0x0146,
            GvAncientOne = 0x0147,
            BgsGreenJiggySwitch = 0x014E,
            BgsDestroyedJiggy = 0x014F,

            // 	{id=0x0151, name="Lockup"},
            // 	{id=0x0152, name="Lockup"},
            // 	{id=0x0153, name="Lockup"},
            FpChristmasTree = 0x015F,

            // 	{id=0x0160, name="Boggy on Sled"},
            // 	{id=0x0161, name="Unknown 0x0161"},
            // 	{id=0x0162, name="Unknown 0x0162"},
            // 	{id=0x0163, name="Bat"},
            SmColiwobble = 0x0164,

            // 	{id=0x0165, name="Bawl"},
            SmTopper = 0x0166,

            // 	{id=0x0167, name="Coliwobble (Spawns Extra Honeycomb Piece)"},
            CcwWinterSwitch = 0x0168,

            // 	{id=0x0169, name="Door"},
            CcwAutumnSwitch = 0x016A,

            // 	{id=0x016B, name="Door"},
            CcwSummerSwitch = 0x016C,

            // 	{id=0x016D, name="Door"},
            SmQuarry = 0x016F,
            // 	{id=0x0172, name="Weird Green Ball 1"},
            // 	{id=0x0173, name="Weird Green Ball 2"},
            //   {id=0x0174, name="Weird Green Ball 3"},

            // 	{id=0x0175, name="Ship Propeller"},
            // 	{id=0x0176, name="Green propeller switch"},
            // 	{id=0x0177, name="Shaft"},
            // 	{id=0x0178, name="Spinning Platform 1"},
            // 	{id=0x0179, name="Spinning Platform 2"},
            // 	{id=0x017A, name="Spinning Platform 3"},
            // 	{id=0x017B, name="Grey Cog 1"},
            // 	{id=0x017C, name="Grey Cog 2"},
            // 	{id=0x017D, name="Grey Cog 3"},
            // 	{id=0x017E, name="Shaft 1"},
            // 	{id=0x017F, name="Shaft 2"},
            // 	{id=0x0180, name="Shaft 3"},
            // 	{id=0x0181, name="Sled 1"},
            // 	{id=0x0182, name="Sled 2"},
            // 	{id=0x018F, name="Honeycomb Switch"},
            // 	{id=0x0191, name="Secret X Barrel Top"},
            FileSelectBanjoPlayingGameboy = 0x0196,
            FileSelectBanjoCooking = 0x0197,
            FileSelectBanjosBed = 0x0198,
            FileSelectBanjosChair = 0x0199,
            FileSelectBanjosKitchen = 0x019A,

            // 	{id=0x019B, name="Another Secret can get in desert"},
            // 	{id=0x019B, name="SNS Conversation Regarding Secrets on Beach"},
            // 	{id=0x019C, name="THE END"},
            // 	{id=0x01BB, name="Propeller 7"},
            // 	{id=0x01BC, name="Propeller 8"},
            // 	{id=0x01BD, name="Propeller 9"},
            // 	{id=0x01BE, name="Boat Propeller Switch"},
            // 	{id=0x01BF, name="1 Switch"},
            // 	{id=0x01C0, name="2 Switch"},
            // 	{id=0x01C1, name="3 Switch"},
            // 	{id=0x01C2, name="Gold Whistle 1"},
            // 	{id=0x01C3, name="Gold Whistle 2"},
            // 	{id=0x01C4, name="Gold Whistle 3"},
            // 	{id=0x01C6, name="Pipe"},
            // 	{id=0x01C7, name="Anchor Switch"},
            RbbSnorkel = 0x01C8,
            RbbAnchorAndChain = 0x01C9,
            RbbRarewareFlagPole = 0x01CA,

            // 	{id=0x01CC, name="Grill Chompa"},
            LairCheato1 = 0x01D5,
            LairCheato2 = 0x01D6,
            LairCheato3 = 0x01D7,

            // 	{id=0x01D8, name="Big Blue Egg"},
            // 	{id=0x01D9, name="Big Red Feather"},
            // 	{id=0x01DA, name="Big Gold Feather"},
            // 	{id=0x01DB, name="GAME OVER Logo"},
            // 	{id=0x01DC, name="BANJO-KAZOOIE Logo"},
            // 	{id=0x01DD, name="Copyright Info"},
            // 	{id=0x01DE, name="PRESS START"},
            // 	{id=0x01DF, name="NO CONTROLLER"},
            // 	{id=0x01E0, name="Jiggy Picture"},
            // 	{id=0x01E1, name="Iron Gate"},
            // 	{id=0x01E2, name="Spring Switch"},
            // 	{id=0x01E3, name="Door"},
            // 	{id=0x01E4, name="Toots"},
            // 	{id=0x01E5, name="Dust flies"},
            // 	{id=0x01E5, name="Firecrackers?!"},
            // 	--{id=0x01E7, name="Kitchen (Banjo's House)},
            // 	{id=0x01E9, name="Venus Flytrap"},
            FpMoggy = 0x01EA,
            FpSoggy = 0x01EB,
            FpGroggy = 0x01EC,
            FpCollectablePresentBlue = 0x01ED,
            FpCollectablePresentGreen = 0x01EF,
            FpCollectablePresentRed = 0x01F1,
            FpWozzaOutside = 0x01F3,

            // 	{id=0x01F5, name="Kazooie Door 2"},
            // 	{id=0x01F7, name="Pyramid"},
            // 	{id=0x01F8, name="Unknown 0x01F8"},
            // 	{id=0x01F9, name="Cactus"},
            BgsCroctus = 0x01FA,

            // 	{id=0x01FB, name="BGS Maze Jiggy Switch"},
            // 	{id=0x01FD, name="Clock Switch"},
            // 	{id=0x01FE, name="Gate"},
            // 	{id=0x01FF, name="Red Feather"},
            // 	{id=0x0200, name="Gold Feather"},
            // 	{id=0x0203, name="Note Door"},
            MmWitchSwitch = 0x0204,
            MmmWitchSwitch = 0x0206,
            TtcWitchSwitch = 0x0208,
            RbbWitchSwitch = 0x020B,
            // 	{id=0x020D, name="Brick wall"},
            // 	{id=0x020E, name="Entrance door to MM"},
            // 	{id=0x020F, name="RBB Entrance Door"},
            //   {id=0x0210, name="BGS Entrance Door"},

            // 	{id=0x0211, name="Chest lid"},
            // 	{id=0x0212, name="Big Iron Bars"},
            // 	{id=0x0213, name="Circular Grate"},
            // 	{id=0x0214, name="Grate Switch"},
            // 	{id=0x0215, name="Tall Pipe 1"},
            // 	{id=0x0216, name="Tall Pipe 2"},
            // 	{id=0x0217, name="Pipe Switch"},
            // 	{id=0x0218, name="Pipe"},
            // 	{id=0x0219, name="Pipe Switch 2"},
            // 	{id=0x021A, name="CC Entrance grates"},
            // 	{id=0x021B, name="Grunty's Hat"},
            // 	{id=0x021C, name="Wooden Door"},
            // 	{id=0x021D, name="Steel Door"},
            // 	{id=0x021E, name="Circular Green Grate"},
            // 	{id=0x021F, name="Circular Brown Grate"},
            // 	{id=0x0220, name="RBB Jigsaw grate"},
            // 	{id=0x0221, name="Water Switch 1"},
            // 	{id=0x0222, name="Water Switch 2"},
            // 	{id=0x0223, name="Water Switch 3"},
            // 	{id=0x0224, name="Ice Ball"},
            // 	{id=0x0224, name="Sarcopphagus"},
            // 	{id=0x0225, name="Rareware box"},
            // 	{id=0x0226, name="GV Entrance"},
            // 	{id=0x0227, name="Phantom Glow"},
            // 	{id=0x0228, name="Invisible Wall"},
            // 	{id=0x0229, name="House"},
            // 	{id=0x022B, name="Frozen Mumbo Hut"},
            // 	{id=0x022C, name="Christmas Present Stack"},
            // 	{id=0x022D, name="Snowy Bridge"},
            // 	{id=0x022E, name="Snowy Bridge 2"},
            // 	{id=0x022F, name="Snowy Bridge 3"},
            // 	{id=0x0230, name="Cobweb"},
            // 	{id=0x0231, name="Big yellow cobweb"},
            // 	{id=0x0233, name="Mumbo's Hut"},
            // 	{id=0x0234, name="CCW Entrance Door"},
            // 	{id=0x0235, name="FP Entrance Door"},
            // 	{id=0x0236, name="FP Entrance Door 2"},
            CcwWitchSwitch = 0x0237,
            FpWitchSwitch = 0x0239,
            // 	{id=0x023A, name="Wooden snow drift texture"},
            // 	{id=0x023B, name="Cauldron"},
            // 	{id=0x023C, name="CCW Switch"},
            // 	{id=0x023D, name="Mumbo Transformation Pad"},
            // 	{id=0x023E, name="Empty wooden coffin"},
            // 	{id=0x023F, name="Skylights"},
            // 	{id=0x0242, name="Unknown 0x0242"},
            // 	{id=0x0243, name="Wooden Panel"},
            // 	{id=0x0244, name="Sarcopphagus Lid"},
            // 	{id=0x0245, name="Sarcopphagus switch"},
            // 	{id=0x0246, name="Flying pad switch"},
            // 	{id=0x0247, name="Kazooie fly disk"},
            //   {id=0x0248, name="Shock jump switch"},

            // 	{id=0x0249, name="Kazooie Shock Spring disc"},
            // 	{id=0x0253, name="Piece of glass panel"},
            // 	{id=0x0254, name="Piece of glass panel"},
            // 	{id=0x0255, name="Tomb"},
            // 	{id=0x0256, name="Witch Switch (GV)"},
            // 	{id=0x0257, name="Witch Switch (BGS)"},
            // 	{id=0x0258, name="Tomb Top"},
            // 	{id=0x0259, name="Grunty eye switch 1"},
            // 	{id=0x025A, name="Grunty eye switch 2"},
            CcWitchSwitch = 0x025B,

            // 	{id=0x025C, name="Sharkfood Island"},
            SnsIceKey = 0x025D,
            SnsEgg = 0x025E,

            // 	{id=0x025F, name="Nabnut's Window Spring"},
            // 	{id=0x0260, name="Nabnut's Window Summer"},
            // 	{id=0x0261, name="Nabnut's Window Fall"},
            // 	{id=0x0262, name="Nabnut's Window Winter"},
            // 	{id=0x0263, name="Safety Boat"},
            // 	{id=0x0264, name="Safety Boat"},
            // 	{id=0x0265, name="Door"},
            // 	{id=0x0266, name="Window 1"},
            // 	{id=0x0267, name="Window 2"},
            BgsTiptup = 0x027A,
            // 	{id=0x027B, name="Choir Member (Yellow)"},
            // 	{id=0x027C, name="Choir Member (Cyan)"},
            // 	{id=0x027D, name="Choir Member (Blue)"},
            // 	{id=0x027E, name="Choir Member (Red)"},
            // 	{id=0x027F, name="Choir Member (Pink)"},
            // 	{id=0x0280, name="Choir Member (Purple)"},
            // 	{id=0x0281, name="Kaboom Part 1"},
            // 	{id=0x0282, name="Kaboom Part 2"},
            // 	{id=0x0283, name="Kaboom Part 3"},
            // 	{id=0x0284, name="Kaboom Part 4"},
            // 	{id=0x0285, name="Big Jinxy Head"},
            // 	{id=0x0286, name="Big Jinxy Head 2"},
            // 	{id=0x0287, name="Big Jinxy Head 3"},
            // 	{id=0x0288, name="Weird Green Ball"},
            //   {id=0x0289, name="Vent"},

            // 	{id=0x028A, name="Underwater Whipcrack"},
            // 	{id=0x028B, name="Waterfall/Cauldron Sound"},
            // 	{id=0x028C, name="Green Drain 1"},
            // 	{id=0x028D, name="Green Drain 2"},
            // 	{id=0x028E, name="Green Drain 3"},
            // 	{id=0x028F, name="Vent"},
            // 	{id=0x0290, name="Propeller 10"},
            // 	{id=0x0291, name="Propeller 11"},
            // 	{id=0x0292, name="Spinning SawBlade (Fast)"},
            // 	{id=0x0293, name="Propeller 13"},
            // 	{id=0x0294, name="Propeller 14"},
            // 	{id=0x0295, name="Propeller 15"},
            // 	{id=0x0296, name="Bell Buoy"},
            // 	{id=0x0297, name="Row Boat"},
            // 	{id=0x0298, name="Flat Board"},
            // 	{id=0x0299, name="Lump of Honey"},
            // 	{id=0x029A, name="Flat Board"},
            // 	{id=0x029B, name="Zubba"},
            // 	{id=0x029C, name="Zubba (Attacker)"},
            // 	{id=0x029D, name="Beakstalk"},
            // 	{id=0x029E, name="Gobi"},
            // 	{id=0x029F, name="Big Clucker"},
            // 	{id=0x02A0, name="Unknown 0x02A0"},
            // 	{id=0x02A1, name="Eyrie"},
            // 	{id=0x02A2, name="Catapiller"},
            // 	{id=0x02A3, name="Big Eyrie"},
            // 	{id=0x02A4, name="TNT Box Part 1"},
            // 	{id=0x02A5, name="Extra Life Spawn Location (RBB)"},
            // 	{id=0x02A6, name="Nabnut 1"},
            // 	{id=0x02A7, name="Nabnut 2"},
            // 	{id=0x02A8, name="Nabnut 3"},
            // 	{id=0x02A9, name="Acorn"},
            // 	{id=0x02AA, name="Gnawty 1"},
            // 	{id=0x02AB, name="Gnawty (Summer)"},
            // 	{id=0x02AC, name="Gnawty's Boulder"},
            // 	{id=0x02AE, name="Leaf Particles (Screen Overlay)"},
            // 	{id=0x02B0, name="Rain"},
            // 	{id=0x02B2, name="Snow"},
            // 	{id=0x02B5, name="Nonsense picture"},
            // 	{id=0x02D8, name="Two Poles"},
            // 	{id=0x02D9, name="X 1 (Crashes?)"},
            // 	{id=0x02DA, name="X 2 (Crashes?)"},
            // 	{id=0x02DB, name="Dingpot 3"},
            // 	{id=0x02DC, name="Gnawty's Shelves"},
            // 	{id=0x02DD, name="Gnawty's Bed"},
            // 	{id=0x02DE, name="Gnawty's House"},
            // 	{id=0x02DF, name="Lighthouse"},
            // 	{id=0x02E0, name="Stairs"},
            // 	{id=0x02E1, name="Underwater Ship Warp?"},
            // 	{id=0x02E2, name="Lighthouse Body"},
            //   {id=0x02E3, name="World Entrance Sign"}, -- MM, RBB

            // 	{id=0x02E4, name="Level Entry/Exit"},
            // 	{id=0x02E5, name="Wooden Door"},
            // 	{id=0x02E6, name="Nabnut's Door"},
            // 	{id=0x02E7, name="Wooden Door"},
            // 	{id=0x02E8, name="Window 3"},
            // 	{id=0x02E9, name="Short Window"},
            // 	{id=0x02EA, name="Tall Window"},
            // 	{id=0x02ED, name="Machine Room Booth"},
            // 	{id=0x02EE, name="Klungo 1"},
            // 	{id=0x02EF, name="Tooty 0x02EF"},
            // 	{id=0x02F0, name="Machine Room Console"},
            // 	{id=0x02F1, name="Tooty's force field"},
            // 	{id=0x02F2, name="Machine Room booth"},
            // 	{id=0x02F3, name="Lightning"},
            // 	{id=0x02F4, name="Roysten's Bowl"},
            // 	{id=0x02F5, name="Cuckoo Clock"},
            // 	{id=0x02F6, name="Yellow Jinjo 0x02F6"},
            // 	{id=0x02F7, name="Yellow Jinjo 0x02F7"},
            // 	{id=0x02F8, name="Yellow Jinjo 0x02F8"},
            // 	{id=0x02F9, name="Yellow Jinjo 0x02F9"},
            // 	{id=0x02FA, name="Klungo 2"},
            // 	{id=0x02FC, name="Mumbo 0x02FC"},
            // 	{id=0x02FD, name="Snacker"},
            // 	{id=0x02FE, name="Yellow Jinjo 0x02FE"},
            // 	{id=0x02FF, name="Unknown 0x02FF"},
            // 	{id=0x0300, name="Unknown 0x0300"},
            // 	{id=0x0301, name="Blubber"},
            // 	{id=0x0302, name="Rock"},
            // 	{id=0x0303, name="Yellow Jinjo 0x0303"},
            // 	{id=0x0304, name="Yellow Jinjo 0x0304"},
            // 	{id=0x0305, name="Yellow Jinjo 0x0305"},
            // 	{id=0x0306, name="Yellow Jinjo 0x0306"},
            // 	{id=0x0307, name="Yellow Jinjo 0x0307"},
            // 	{id=0x0308, name="Yellow Jinjo 0x0308"},
            // 	{id=0x0309, name="Yellow Jinjo 0x0309"},
            // 	{id=0x030A, name="Yellow Jinjo 0x030A"},
            // 	{id=0x030B, name="Dead Beanstalk"},
            // 	{id=0x030C, name="'Z' Logo"},
            // 	{id=0x030D, name="TNT Box Part 2"},
            // 	{id=0x030E, name="Eyrie Big "},
            // 	{id=0x030F, name="Whip Crack"},
            // 	{id=0x0310, name="Acorn mound"},
            // 	{id=0x0311, name="Nabnut's Wife"},
            // 	{id=0x0312, name="Nabnut's Bed Covers"},
            // 	{id=0x0313, name="Nabnut's Duvet"},
            // 	{id=0x0314, name="Nabnut 4"},
            // 	{id=0x0315, name="Nabnut (Top Half)"},
            // 	{id=0x031A, name="Gnawty (Winter)"},
            // 	{id=0x031C, name="Stationary Platform (Engine Room)"},
            // 	{id=0x031D, name="Pyramid 2"},
            // 	{id=0x031E, name="Palm Tree"},
            // 	{id=0x0332, name="Blue Twinkly"},
            // 	{id=0x0333, name="Green Twinkly"},
            // 	{id=0x0334, name="Orange Twinkly"},
            // 	{id=0x0335, name="Red Twinkly"},
            // 	{id=0x0336, name="Twinkly Box"},
            // 	{id=0x0337, name="Twinkly Muncher"},
            // 	{id=0x0338, name="Christmas Tree Switch"},
            // 	{id=0x0339, name="Christmas Tree"},
            //   {id=0x033A, name="Blue Present 0x033A"},

            // 	{id=0x033B, name="Green Pesent 0x033B"},
            // 	{id=0x033C, name="Red Present 0x033C"},
            // 	{id=0x033D, name="Boggy on Sled 3"},
            // 	{id=0x033E, name="Nintendo Cube?"},
            // 	{id=0x033F, name="Wozza (In Cave)"},
            // 	{id=0x0340, name="Ice Tree"},
            // 	{id=0x0341, name="Unknown 0x0341"},
            // 	{id=0x0348, name="Brentilda"},
            // 	{id=0x034D, name="Bee Swarm"},
            // 	{id=0x034E, name="Limbo"},
            // 	{id=0x034F, name="Mummy"},
            // 	{id=0x0350, name="Sea Grublin"},
            // 	{id=0x0352, name="Grunty after first note door"},
            // 	{id=0x0353, name="Weird green ball"},
            // 	{id=0x0354, name="Water Drops"},
            // 	{id=0x0355, name="Purple ice pole"},
            // 	{id=0x0356, name="Green Ice Pole"},
            // 	{id=0x0357, name="Big Ice Pole"},
            // 	{id=0x0358, name="Ice Pole"},
            // 	{id=0x035D, name="Ice pole"},
            // 	{id=0x035E, name="Race rostrum"},
            // 	{id=0x035F, name="Finish banner"},
            // 	{id=0x0360, name="Start banner"},
            // 	--id=0x361, name="Stop n' Swap?"},
            // 	{id=0x0367, name="Gruntling"},
            // 	{id=0x0368, name="Mumbo Token Sign (5)"},
            // 	{id=0x0369, name="Mumbo Token Sign (20)"},
            // 	{id=0x036A, name="Mumbo Token Sign (15)"},
            // 	{id=0x036B, name="Mumbo Token Sign (10)"},
            // 	{id=0x036C, name="Mumbo Token Sign (25)"},
            // 	{id=0x036D, name="Coliwobble"},
            // 	{id=0x036E, name="Bawl"},
            // 	{id=0x036F, name="Topper"},
            // 	{id=0x0370, name="Gold Feather"},
            // 	{id=0x0374, name="Text Trigger (Mumbo's On Vacation)"},
            // 	{id=0x0375, name="Grublin Hood"},
            // 	{id=0x0376, name="Boss Boom Box"},
            // 	--{id=0x037A, name="Assosiated with MoleHill"}, --talk volume?
            // 	{id=0x037C, name="Dust or Steam Cloud"},
            // 	{id=0x037D, name="Ice Cube"},
            // 	{id=0x037E, name="Dead Venus Flytrap"},
            // 	{id=0x037F, name="Loggo"},
            // 	{id=0x0380, name="Beetle"},
            // 	{id=0x0381, name="Portrait Chompa"},
            // 	{id=0x0382, name="Portrait (Grunty)"},
            // 	{id=0x0383, name="Fire"},
            // 	{id=0x0384, name="Steam and sparks"},
            // 	{id=0x0385, name="Portrait (Blackeye)"},
            // 	{id=0x0386, name="Portrait (Tower)"},
            // 	{id=0x0387, name="Portrait (Tree)"},
            //   {id=0x0388, name="Portrait (Ghosts)"},

            // 	{id=0x0389, name="Fireball"},
            // 	{id=0x038A, name="Green Blast"},
            // 	{id=0x038B, name="Grunty"},
            // 	{id=0x03A0, name="Ice Cube"},
            // 	{id=0x03A1, name="Jinjonator Stand"},
            // 	{id=0x03A2, name="Jinjo Stand"},
            // 	{id=0x03A5, name="Orange Jinjo"},
            // 	{id=0x03A6, name="Green Jinjo"},
            // 	{id=0x03A7, name="Purple Jinjo"},
            // 	{id=0x03A8, name="Yellow Jinjo 0x03A8"},
            // 	{id=0x03AA, name="Big Green Blast"},
            // 	{id=0x03AC, name="Jinjonator"},
            // 	{id=0x03AD, name="Rock Smash"},
            // 	{id=0x03AE, name="Boggy's Igloo"},
            // 	{id=0x03AF, name="Shadow"},
            // 	{id=0x03B0, name="House chimney"},
            // 	{id=0x03B7, name="Puzzle Jiggy Podium"},
            // 	{id=0x03BB, name="Fireball"},
            // 	{id=0x03BC, name="Jiggy Podium"},
            // 	{id=0x03BF, name="Gruntling 2"},
            // 	{id=0x03C0, name="Gruntling 3"},
            // 	{id=0x03C1, name="Purple TeeHee"},
            // 	{id=0x03C2, name="Unknown 0x03C2"},
            // 	{id=0x03C4, name="Washing machine Cauldron"},
            // 	{id=0x03C5, name="Gruntilda (Furnace Fun)"},
            // 	{id=0x03C6, name="Furnace Fun Podium"},
            // 	{id=0x03C7, name="Grunty Doll (Furnace Fun)"},
            // 	{id=0x03C8, name="Tooty (Furnace Fun)"},
        }

        public enum AnimalType
        {
            Unknown = 0x00,
            BearBird = 0x01,
            Termite = 0x02,
            Pumpkin = 0x03,
            Walrus = 0x04,
            Crocodile = 0x05,
            Bee = 0x06,
            WashingMachine = 0x07
        }

        public enum CharacterType
        {
            // CHARACTER
            BanjoKazooie,
            Blubber,
            Bottles,
            Chimpy,
            Conga,
            GruntildaSexy,
            GruntildaUgly,
            JinjoBlue,
            JinjoGreen,
            JinjoOrange,
            JinjoPink,
            JinjoYellow,
            Klungo,
            Limbo,
            Mumbo,
            Nabnut,
            Tooty,
            TootyUgly,

            // TERMITE
            BanjoTermite,
            Termite,
            CrabGreen,

            // CROCODIILE
            BanjoCrocodile,
            MrVile,

            // WALRUS
            BanjoWalrus,
            Wozza,

            // PUMPKIN
            BanjoPumpkin,
            Snowball,

            // BEE
            BanjoBee,
            Zubba
        }

        public enum ExitType
        {
            Unknown = 0x00,

            MmMainMumbosSkull = 0x01,
            MmMainTickersTowerBottom = 0x02,
            MmMainTickersToperTop = 0x03,
            MmMainButtonCutscene = 0x04,
            MmMainLevelEntrance = 0x05,

            TtcMainSandcastle = 0x03,
            TtcMainLevelEntrance = 0x04,
            TtcBlubbersShipTop = 0x05,
            TtcBlubbersShipBottom = 0x06,
            TtcMainBlubbersShipTop = 0x06,
            TtcMainBlubbersShipSide = 0x07,
            TtcMainLighthouseTop = 0x08,
            TtcMainNipper = 0x0A,
            TtcLighthouseBottom = 0x0C,
            TtcStairAlcoveTop = 0x0E,
            TtcStairAlcoveBottom = 0x0F,

            SmMainBanjosHouse = 0x12,
            SmMainGruntildasLair = 0x13
        }

        public enum InventoryType
        {
            TimerHourglass1 = 0x00 * 4, // 0X00
            TimerSkull = 0x01 * 4, // 0X04
            TimerPropellor = 0x03 * 4, // 0X0C
            TimerXmasTree = 0x05 * 4, // 0X14
            TimerHourglass2 = 0x06 * 4, // 0X18
            CurLvlNotes = 0x0C * 4, // 0X30
            Eggs = 0x0D * 4, // 0X34
            CurLvlJiggies = 0x0E * 4, // 0X38
            FeathersRed = 0x0F * 4, // 0X3C
            FeathersGold = 0x10 * 4, // 0X40
            CurLvlJinjos = 0x12 * 4, // 0X48
            HoneycombsEmpty = 0x13 * 4, // 0X4C
            Health = 0x14 * 4, // 0X50
            HealthContainers = 0x15 * 4, // 0X54
            Lives = 0x16 * 4, // 0X58
            Air = 0x17 * 4, // 0X5C
            CurLvlGoldBullion = 0x18 * 4, // 0X60
            CurLvlOrange = 0x19 * 4, // 0X64
            VileScorePlyr = 0x1A * 4, // 0X68
            VileScoreVile = 0x1B * 4, // 0X6C
            MumboTokensHeld = 0x1C * 4, // 0X70
            Grumblies = 0x1D * 4, // 0X74
            Yumblies = 0x1E * 4, // 0X78
            CurLvlPresentGreen = 0x1F * 4, // 0X7C
            CurLvlPresentBlue = 0x20 * 4, // 0X80
            CurLvlPresentRed = 0x21 * 4, // 0X84
            CurLvlCaterpillars = 0x22 * 4, // 0X88
            CurLvlAcorns = 0x23 * 4, // 0X8C
            Twinklies = 0x24 * 4, // 0X90
            MumboTokens = 0x25 * 4, // 0X94
            Jiggies = 0x26 * 4, // 0X98
            JokerCard = 0x27 * 4, // 0X9C
            TextJiggies = 0x2B * 4, // 0XAC
            NoteTotals = 0x30 * 4 // 0XC0
        }

        public enum JinjoType
        {
            Blue = 0x00,
            Green = 0x01,
            Orange = 0x02,
            Pink = 0x03,
            Yellow = 0x04
        }

        public enum LevelType
        {
            Unknown = 0x00,
            MumbosMountain = 0x01,
            TreasureTroveCove = 0x02,
            ClankersCavern = 0x03,
            BubbleGloopSwamp = 0x04,
            FreezeezyPeak = 0x05,
            GruntildasLair = 0x06,
            GobeysValey = 0x07,
            ClickClockWoods = 0x08,
            RustyBucketBay = 0x09,
            MadMonsterMansion = 0x0A,
            SpiralMountain = 0x0B,
            GruntildasLairRoof = 0x0C,
            TitleScreen = 0x0D,
            RomhackLevel = 0x0E
        }

        public enum MoveType
        {
            BeakBarge = 0x00,
            BeakBomb = 0x01,
            BeakBuster = 0x02,
            CameraControls = 0x03,
            BearPunch = 0x04,
            ClimbPoles = 0x05,
            Eggs = 0x06,
            FeatheryFlap = 0x07,
            FlipFlap = 0x08,
            Flying = 0x09,
            VariableJumpHeight = 0x0A,
            RatATatRap = 0x0B,
            Roll = 0x0C,
            ShockSpringJump = 0x0D,
            WaddingBoots = 0x0E,
            Dive = 0x0F,
            TalonTrot = 0x10,
            TurboTalonTrainers = 0x11,
            Wonderwing = 0x12,
            NoteDoorMoleHill = 0x13
        }

        public enum ProfileType
        {
            Title = -1,
            A = 0,
            B = 2,
            C = 1
        }

        public enum SceneType
        {
            Unknown = 0x00,
            SmMain = 0x01,
            MmMain = 0x02,
            Unknown0x03 = 0x03,
            Unknown0x04 = 0x04,
            TtcBlubbersShip = 0x05,
            TtcNippersShell = 0x06,
            TtcMain = 0x07,
            Unknown0x08 = 0x08,
            Unknown0x09 = 0x09,
            TtcSandcastle = 0x0A,
            CcClankersCavern = 0x0B,
            MmTickersTower = 0x0C,
            BgsMain = 0x0D,
            MmMumbosSkull = 0x0E,
            Unknown0x0f = 0x0F,
            BgsMrVile = 0x10,
            BgsTiptup = 0x11,
            GvMain = 0x12,
            GvMatchingGame = 0x13,
            GvMaze = 0x14,
            GvWater = 0x15,
            GvRubeesChamber = 0x16,
            Unknown0x17 = 0x17,
            Unknown0x18 = 0x18,
            Unknown0x19 = 0x19,
            GvSphinx = 0x1A,
            MmmMain = 0x1B,
            MmmChurch1 = 0x1C,
            MmmCellar = 0x1D,
            StartNintendo = 0x1E,
            StartRareware = 0x1F,
            EndScene2 = 0x20,
            CcWitchSwitch = 0x21,
            CcInsideClanker = 0x22,
            CcGoldFeather = 0x23,
            MmmTimblarShed = 0x24,
            MmmWell = 0x25,
            MmmDiningRoomNapper = 0x26,
            FpMain = 0x27,
            MmmRoom1 = 0x28,
            MmmRoom2 = 0x29,
            MmmFireplace = 0x2A,
            MmmChurch2 = 0x2B,
            MmmBathroom = 0x2C,
            MmmBedroom = 0x2D,
            MmmFloorboards = 0x2E,
            MmmBarrel = 0x2F,
            MmmMumbosSkull = 0x30,
            RbbMain = 0x31,
            Unknown0x32 = 0x32,
            Unknown0x33 = 0x33,
            RbbEngineRoom = 0x34,
            RbbWarehouse1 = 0x35,
            RbbWarehouse2 = 0x36,
            RbbContainer1 = 0x37,
            RbbContainer3 = 0x38,
            RbbCrewCabin = 0x39,
            RbbBossBoomBox = 0x3A,
            RbbStoreRoom = 0x3B,
            RbbKitchen = 0x3C,
            RbbNavigationRoom = 0x3D,
            RbbContainer2 = 0x3E,
            RbbCaptainsCabin = 0x3F,
            CcwMain = 0x40,
            FpBoggysIgloo = 0x41,
            Unknown0x42 = 0x42,
            CcwSpring = 0x43,
            CcwSummer = 0x44,
            CcwAutumn = 0x45,
            CcwWinter = 0x46,
            BgsMumbosSkull = 0x47,
            FpMumbosSkull = 0x48,
            Unknown0x49 = 0x49,
            CcwSpringMumbosSkull = 0x4A,
            CcwSummerMumbosSkull = 0x4B,
            CcwAutumnMumbosSkull = 0x4C,
            CcwWinterMumbosSkull = 0x4D,
            Unknown0x4e = 0x4E,
            Unknown0x4f = 0x4F,
            Unknown0x50 = 0x50,
            Unknown0x51 = 0x51,
            Unknown0x52 = 0x52,
            FpInsideXmasTree = 0x53,
            Unknown0x54 = 0x54,
            Unknown0x55 = 0x55,
            Unknown0x56 = 0x56,
            Unknown0x57 = 0x57,
            Unknown0x58 = 0x58,
            Unknown0x59 = 0x59,
            CcwSummerZubbasHive = 0x5A,
            CcwSpringZubbasHive = 0x5B,
            CcwAutumnZubbasHive = 0x5C,
            Unknown0x5d = 0x5D,
            CcwSpringNabnutsHouse = 0x5E,
            CcwSummerNabnutsHouse = 0x5F,
            CcwAutumnNabnutsHouse = 0x60,
            CcwWinterNabnutsHouse = 0x61,
            CcwWinterNabnuts1 = 0x62,
            CcwAutumnNabnuts2 = 0x63,
            CcwWinterNabnuts2 = 0x64,
            CcwSpringTop = 0x65,
            CcwSummerTop = 0x66,
            CcwAutumnTop = 0x67,
            CcwWinterTop = 0x68,
            GlLobbyMm = 0x69,
            GlPuzzleTtc = 0x6A,
            GlPuzzleCc = 0x6A,
            GlNoteDoor180 = 0x6B,
            GlPuzzleCcw = 0x6B,
            GlRedCauldron = 0x6C,
            GlLobbyTtc = 0x6D,
            GlLobbyGv = 0x6E,
            GlLobbyFp = 0x6F,
            GlLobbyCc = 0x70,
            GlWitchStatue = 0x71,
            GlLobbyBgs = 0x72,
            Unknown0x73 = 0x73,
            GlPuzzleGv = 0x74,
            GlLobbyMmm = 0x75,
            GlNoteDoor640 = 0x76,
            GlLobbyRbb = 0x77,
            GlPuzzleRbb = 0x78,
            GlLobbyCcw = 0x79,
            GlFloor2CryptInside = 0x7A,
            IntroLair1 = 0x7B,
            IntroBanjoHouse1 = 0x7C,
            IntroSpiralA = 0x7D,
            IntroSpiralB = 0x7E,
            FpWozzasCave = 0x7F,
            GlFloor3 = 0x80,
            IntroLair2 = 0x81,
            IntroMachine1 = 0x82,
            IntroGameOver = 0x83,
            IntroLair5 = 0x84,
            IntroSpiralC = 0x85,
            IntroSpiralD = 0x86,
            IntroSpiralE = 0x87,
            IntroSpiralF = 0x88,
            IntroBanjoHouse2 = 0x89,
            IntroBanjoHouse3 = 0x8A,
            RbbAnchor = 0x8B,
            SmBanjoHouse = 0x8C,
            MmmInsideLoggo = 0x8D,
            GlFurnaceFun = 0x8E,
            TtcSharkfoodIsland = 0x8F,
            GlBattlements = 0x90,
            FileSelect = 0x91,
            GvSecretChamber = 0x92,
            GlDingpot = 0x93,
            IntroSpiralG = 0x94,
            EndScene3 = 0x95,
            EndScene1 = 0x96,
            EndScene4 = 0x97,
            IntroGruntyThreat1 = 0x98,
            IntroGruntyThreat2 = 0x99,
            RomhackScene = 0xA0
        }

        public enum TransitionType
        {
            Tmp0,
            Tmp1,
            Tmp2,
            Tmp3,
            Tmp4,
        }

        public enum ActorIdType
        {
            Orange = 0x02D2,
            JinjoYellow = 0x03BB,
            JinjoOrange = 0x03BC,
            JinjoBlue = 0x03C0,
            JinjoPink = 0x03C1,
            JinjoGreen = 0x03C2,
            GoldBullion = 0x03C7,
            PresentBlue = 0x047F,
            PresentGreen = 0x0480,
            PresentRed = 0x0481,
            Caterpillar = 0x0485,
            Acorn = 0x048E,
            Note = 0x06D6
        }

        public enum VoxelIdType
        {
            Note = 0x1640,
            Orange = 0x02D2,
            GoldBullion = 0x03C7,
            PresentBlue = 0x047F,
            PresentGreen = 0x0480,
            PresentRed = 0x0481,
            Caterpillar = 0x0485,
            Acorn = 0x048E
        }
    }
}
