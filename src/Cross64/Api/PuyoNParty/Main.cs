using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PuyoNParty : ApiBase
    {
        public override string GameName => "Puyo Puyo 'N Party";
        public override GameID GameID => GameID.PuyoNParty;

        public PuyoNParty()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
