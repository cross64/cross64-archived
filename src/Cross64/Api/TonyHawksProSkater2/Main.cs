using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TonyHawksProSkater2 : ApiBase
    {
        public override string GameName => "Tony Hawk's Pro Skater 2";
        public override GameID GameID => GameID.TonyHawksProSkater2;

        public TonyHawksProSkater2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
