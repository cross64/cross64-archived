using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TiggersHoneyHunt : ApiBase
    {
        public override string GameName => "Tigger's Honey Hunt";
        public override GameID GameID => GameID.TiggersHoneyHunt;

        public TiggersHoneyHunt()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
