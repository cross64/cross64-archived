using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WorldDriverChampionship : ApiBase
    {
        public override string GameName => "World Driver Championship";
        public override GameID GameID => GameID.WorldDriverChampionship;

        public WorldDriverChampionship()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
