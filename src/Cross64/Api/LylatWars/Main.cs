using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LylatWars : ApiBase
    {
        public override string GameName => "Lylat Wars";
        public override GameID GameID => GameID.LylatWars;

        public LylatWars()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
