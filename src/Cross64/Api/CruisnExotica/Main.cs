using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CruisnExotica : ApiBase
    {
        public override string GameName => "Cruis'n Exotica";
        public override GameID GameID => GameID.CruisnExotica;

        public CruisnExotica()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
