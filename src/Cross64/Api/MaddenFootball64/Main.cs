using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MaddenFootball64 : ApiBase
    {
        public override string GameName => "Madden Football 64";
        public override GameID GameID => GameID.MaddenFootball64;

        public MaddenFootball64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
