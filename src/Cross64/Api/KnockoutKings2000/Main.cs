using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KnockoutKings2000 : ApiBase
    {
        public override string GameName => "Knockout Kings 2000";
        public override GameID GameID => GameID.KnockoutKings2000;

        public KnockoutKings2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
