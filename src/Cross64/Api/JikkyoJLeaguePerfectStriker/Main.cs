using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoJLeaguePerfectStriker : ApiBase
    {
        public override string GameName => "Jikkyō J. League: Perfect Striker";
        public override GameID GameID => GameID.JikkyoJLeaguePerfectStriker;

        public JikkyoJLeaguePerfectStriker()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
