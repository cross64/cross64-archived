using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GanbareGoemonNeoMomoyamaBakufunoOdori : ApiBase
    {
        public override string GameName => "Ganbare Goemon: Neo Momoyama Bakufu no Odori";
        public override GameID GameID => GameID.GanbareGoemonNeoMomoyamaBakufunoOdori;

        public GanbareGoemonNeoMomoyamaBakufunoOdori()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
