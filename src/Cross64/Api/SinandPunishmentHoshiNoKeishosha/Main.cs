using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SinandPunishmentHoshiNoKeishosha : ApiBase
    {
        public override string GameName => "Sin and Punishment: Hoshi No Keishōsha";
        public override GameID GameID => GameID.SinandPunishmentHoshiNoKeishosha;

        public SinandPunishmentHoshiNoKeishosha()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
