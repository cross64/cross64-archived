using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HarvestMoon64 : ApiBase
    {
        public override string GameName => "Harvest Moon 64";
        public override GameID GameID => GameID.HarvestMoon64;

        public HarvestMoon64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
