using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Rush2ExtremeRacingUSA : ApiBase
    {
        public override string GameName => "Rush 2: Extreme Racing USA";
        public override GameID GameID => GameID.Rush2ExtremeRacingUSA;

        public Rush2ExtremeRacingUSA()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
