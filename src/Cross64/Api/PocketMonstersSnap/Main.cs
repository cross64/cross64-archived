using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PocketMonstersSnap : ApiBase
    {
        public override string GameName => "Pocket Monsters Snap";
        public override GameID GameID => GameID.PocketMonstersSnap;

        public PocketMonstersSnap()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
