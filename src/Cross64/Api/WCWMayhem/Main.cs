using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WCWMayhem : ApiBase
    {
        public override string GameName => "WCW Mayhem";
        public override GameID GameID => GameID.WCWMayhem;

        public WCWMayhem()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
