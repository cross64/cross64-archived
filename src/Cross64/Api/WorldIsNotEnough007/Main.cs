using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WorldIsNotEnough007 : ApiBase
    {
        public override string GameName => "Currently Unavailable";
        public override GameID GameID => GameID.WorldIsNotEnough007;

        public WorldIsNotEnough007()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
