using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MysticalNinja2StarringGoemon : ApiBase
    {
        public override string GameName => "Mystical Ninja 2, Starring Goemon";
        public override GameID GameID => GameID.MysticalNinja2StarringGoemon;

        public MysticalNinja2StarringGoemon()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
