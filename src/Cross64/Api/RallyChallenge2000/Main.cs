using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RallyChallenge2000 : ApiBase
    {
        public override string GameName => "Rally Challenge 2000";
        public override GameID GameID => GameID.RallyChallenge2000;

        public RallyChallenge2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
