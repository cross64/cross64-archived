using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AerofightersAssault : ApiBase
    {
        public override string GameName => "Aerofighters Assault";
        public override GameID GameID => GameID.AerofightersAssault;

        public AerofightersAssault()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
