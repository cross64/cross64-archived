using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WaiailaeCountryClubTrueGolfClassics : ApiBase
    {
        public override string GameName => "Waialae Country Club: True Golf Classics";
        public override GameID GameID => GameID.WaiailaeCountryClubTrueGolfClassics;

        public WaiailaeCountryClubTrueGolfClassics()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
