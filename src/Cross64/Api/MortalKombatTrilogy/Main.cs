using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MortalKombatTrilogy : ApiBase
    {
        public override string GameName => "Mortal Kombat Trilogy";
        public override GameID GameID => GameID.MortalKombatTrilogy;

        public MortalKombatTrilogy()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
