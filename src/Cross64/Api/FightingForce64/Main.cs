using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FightingForce64 : ApiBase
    {
        public override string GameName => "Fighting Force 64";
        public override GameID GameID => GameID.FightingForce64;

        public FightingForce64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
