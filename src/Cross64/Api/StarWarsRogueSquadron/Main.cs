using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarWarsRogueSquadron : ApiBase
    {
        public override string GameName => "Star Wars: Rogue Squadron";
        public override GameID GameID => GameID.StarWarsRogueSquadron;

        public StarWarsRogueSquadron()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
