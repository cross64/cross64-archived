using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SnowSpeeder : ApiBase
    {
        public override string GameName => "Snow Speeder";
        public override GameID GameID => GameID.SnowSpeeder;

        public SnowSpeeder()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
