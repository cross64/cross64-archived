using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBALive99 : ApiBase
    {
        public override string GameName => "NBA Live '99";
        public override GameID GameID => GameID.NBALive99;

        public NBALive99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
