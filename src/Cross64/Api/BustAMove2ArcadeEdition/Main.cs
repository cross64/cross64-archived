using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BustAMove2ArcadeEdition : ApiBase
    {
        public override string GameName => "Bust-A-Move 2: Arcade Edition";
        public override GameID GameID => GameID.BustAMove2ArcadeEdition;

        public BustAMove2ArcadeEdition()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
