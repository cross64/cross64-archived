using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PGAEuropeanTourGolf : ApiBase
    {
        public override string GameName => "PGA European Tour Golf";
        public override GameID GameID => GameID.PGAEuropeanTourGolf;

        public PGAEuropeanTourGolf()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
