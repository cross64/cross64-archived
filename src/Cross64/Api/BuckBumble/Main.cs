using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BuckBumble : ApiBase
    {
        public override string GameName => "Buck Bumble";
        public override GameID GameID => GameID.BuckBumble;

        public BuckBumble()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
