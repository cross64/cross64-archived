using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WarGods : ApiBase
    {
        public override string GameName => "War Gods";
        public override GameID GameID => GameID.WarGods;

        public WarGods()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
