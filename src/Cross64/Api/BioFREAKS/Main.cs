using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BioFREAKS : ApiBase
    {
        public override string GameName => "Bio F.R.E.A.K.S.";
        public override GameID GameID => GameID.BioFREAKS;

        public BioFREAKS()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
