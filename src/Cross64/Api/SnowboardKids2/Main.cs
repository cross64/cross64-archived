using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SnowboardKids2 : ApiBase
    {
        public override string GameName => "Snowboard Kids 2";
        public override GameID GameID => GameID.SnowboardKids2;

        public SnowboardKids2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
