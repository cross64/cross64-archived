using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StuntRacer64 : ApiBase
    {
        public override string GameName => "Stunt Racer 64";
        public override GameID GameID => GameID.StuntRacer64;

        public StuntRacer64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
