using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GanbareGoemonMononokeSugoroku : ApiBase
    {
        public override string GameName => "Ganbare Goemon: Mononoke Sugoruko";
        public override GameID GameID => GameID.GanbareGoemonMononokeSugoroku;

        public GanbareGoemonMononokeSugoroku()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
