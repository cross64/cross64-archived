using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Tetrisphere : ApiBase
    {
        public override string GameName => "Tetrisphere";
        public override GameID GameID => GameID.Tetrisphere;

        public Tetrisphere()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
