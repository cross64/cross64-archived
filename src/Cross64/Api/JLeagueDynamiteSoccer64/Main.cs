using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JLeagueDynamiteSoccer64 : ApiBase
    {
        public override string GameName => "J-League Dynamite Soccer 64";
        public override GameID GameID => GameID.JLeagueDynamiteSoccer64;

        public JLeagueDynamiteSoccer64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
