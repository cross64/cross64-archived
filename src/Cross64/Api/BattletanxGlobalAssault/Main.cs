using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BattletanxGlobalAssault : ApiBase
    {
        public override string GameName => "Battletanx: Global Assault";
        public override GameID GameID => GameID.BattletanxGlobalAssault;

        public BattletanxGlobalAssault()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
