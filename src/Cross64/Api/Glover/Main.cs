using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Glover : ApiBase
    {
        public override string GameName => "Glover";
        public override GameID GameID => GameID.Glover;

        public Glover()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
