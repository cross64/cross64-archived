using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TriplePlay2000 : ApiBase
    {
        public override string GameName => "Triple Play 2000";
        public override GameID GameID => GameID.TriplePlay2000;

        public TriplePlay2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
