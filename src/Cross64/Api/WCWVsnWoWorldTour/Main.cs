using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WCWVsnWoWorldTour : ApiBase
    {
        public override string GameName => "WCW Vs. nWo: World Tour";
        public override GameID GameID => GameID.WCWVsnWoWorldTour;

        public WCWVsnWoWorldTour()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
