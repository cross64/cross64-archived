using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioKart64 : ApiBase
    {
        public override string GameName => "Mario Kart 64";
        public override GameID GameID => GameID.MarioKart64;

        public MarioKart64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
