using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChopperAttack : ApiBase
    {
        public override string GameName => "Chopper Attack";
        public override GameID GameID => GameID.ChopperAttack;

        public ChopperAttack()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
