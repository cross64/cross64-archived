using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Winback : ApiBase
    {
        public override string GameName => "Winback";
        public override GameID GameID => GameID.Winback;

        public Winback()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
