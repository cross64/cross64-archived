using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MagicalTetrisChallengeFeaturingMickeyMouse : ApiBase
    {
        public override string GameName => "Magical Tetris Challenge Featuring Mickey Mouse";
        public override GameID GameID => GameID.MagicalTetrisChallengeFeaturingMickeyMouse;

        public MagicalTetrisChallengeFeaturingMickeyMouse()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
