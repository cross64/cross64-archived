using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DeadlyArts : ApiBase
    {
        public override string GameName => "Deadly Arts";
        public override GameID GameID => GameID.DeadlyArts;

        public DeadlyArts()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
