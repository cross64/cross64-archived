using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MickeysSpeedwayUSA : ApiBase
    {
        public override string GameName => "Mickey's Speedway USA";
        public override GameID GameID => GameID.MickeysSpeedwayUSA;

        public MickeysSpeedwayUSA()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
