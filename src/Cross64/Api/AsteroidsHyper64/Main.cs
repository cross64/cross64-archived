using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AsteroidsHyper64 : ApiBase
    {
        public override string GameName => "Asteroids Hyper 64";
        public override GameID GameID => GameID.AsteroidsHyper64;

        public AsteroidsHyper64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
