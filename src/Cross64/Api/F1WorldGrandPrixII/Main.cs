using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class F1WorldGrandPrixII : ApiBase
    {
        public override string GameName => "F1 World Grand Prix II";
        public override GameID GameID => GameID.F1WorldGrandPrixII;

        public F1WorldGrandPrixII()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
