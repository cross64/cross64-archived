using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RatAttack : ApiBase
    {
        public override string GameName => "Rat Attack";
        public override GameID GameID => GameID.RatAttack;

        public RatAttack()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
