using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BassRushECOGEARPowerwormChampionship : ApiBase
    {
        public override string GameName => "Bass Rush: ECOGEAR Powerworm Championship";
        public override GameID GameID => GameID.BassRushECOGEARPowerwormChampionship;

        public BassRushECOGEARPowerwormChampionship()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
