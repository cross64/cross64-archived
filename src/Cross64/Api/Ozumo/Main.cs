using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Ozumo : ApiBase
    {
        public override string GameName => "64 Ōzumō";
        public override GameID GameID => GameID.Ozumo;

        public Ozumo()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
