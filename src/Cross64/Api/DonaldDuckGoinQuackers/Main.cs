using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DonaldDuckGoinQuackers : ApiBase
    {
        public override string GameName => "Donald Duck: Goin' Quackers, Disney's";
        public override GameID GameID => GameID.DonaldDuckGoinQuackers;

        public DonaldDuckGoinQuackers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
