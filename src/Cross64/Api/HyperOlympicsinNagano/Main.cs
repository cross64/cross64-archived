using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HyperOlympicsinNagano : ApiBase
    {
        public override string GameName => "Hyper Olympics in Nagano";
        public override GameID GameID => GameID.HyperOlympicsinNagano;

        public HyperOlympicsinNagano()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
