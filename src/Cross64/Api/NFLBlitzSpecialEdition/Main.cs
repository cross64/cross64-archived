using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLBlitzSpecialEdition : ApiBase
    {
        public override string GameName => "NFL Blitz Special Edition";
        public override GameID GameID => GameID.NFLBlitzSpecialEdition;

        public NFLBlitzSpecialEdition()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
