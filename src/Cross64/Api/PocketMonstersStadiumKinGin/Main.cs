using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PocketMonstersStadiumKinGin : ApiBase
    {
        public override string GameName => "Pocket Monsters Stadium: Kin Gin";
        public override GameID GameID => GameID.PocketMonstersStadiumKinGin;

        public PocketMonstersStadiumKinGin()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
