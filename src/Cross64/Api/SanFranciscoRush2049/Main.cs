using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SanFranciscoRush2049 : ApiBase
    {
        public override string GameName => "San Francisco Rush 2049";
        public override GameID GameID => GameID.SanFranciscoRush2049;

        public SanFranciscoRush2049()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
