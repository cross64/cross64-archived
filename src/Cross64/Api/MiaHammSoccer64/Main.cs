using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MiaHammSoccer64 : ApiBase
    {
        public override string GameName => "Mia Hamm Soccer 64";
        public override GameID GameID => GameID.MiaHammSoccer64;

        public MiaHammSoccer64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
