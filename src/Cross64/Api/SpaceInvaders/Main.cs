using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SpaceInvaders : ApiBase
    {
        public override string GameName => "Space Invaders";
        public override GameID GameID => GameID.SpaceInvaders;

        public SpaceInvaders()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
