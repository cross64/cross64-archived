using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ProMahjongKiwame64 : ApiBase
    {
        public override string GameName => "Pro Mahjong Kiwame 64";
        public override GameID GameID => GameID.ProMahjongKiwame64;

        public ProMahjongKiwame64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
