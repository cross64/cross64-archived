using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HeyYouPikachu : ApiBase
    {
        public override string GameName => "Hey You, Pikachu!";
        public override GameID GameID => GameID.HeyYouPikachu;

        public HeyYouPikachu()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
