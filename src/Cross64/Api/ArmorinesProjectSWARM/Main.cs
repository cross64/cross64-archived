using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ArmorinesProjectSWARM : ApiBase
    {
        public override string GameName => "Armorines: Project S.W.A.R.M";
        public override GameID GameID => GameID.ArmorinesProjectSWARM;

        public ArmorinesProjectSWARM()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
