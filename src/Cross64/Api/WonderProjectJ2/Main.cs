using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WonderProjectJ2 : ApiBase
    {
        public override string GameName => "Wonder Project J2";
        public override GameID GameID => GameID.WonderProjectJ2;

        public WonderProjectJ2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
