using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JangoSimulationMahjongdo64 : ApiBase
    {
        public override string GameName => "Jangō Simulation Mahjong-dō 64";
        public override GameID GameID => GameID.JangoSimulationMahjongdo64;

        public JangoSimulationMahjongdo64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
