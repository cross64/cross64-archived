using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TurokDinosaurHunter : ApiBase
    {
        public override string GameName => "Turok: Dinosaur Hunter";
        public override GameID GameID => GameID.TurokDinosaurHunter;

        public TurokDinosaurHunter()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
