using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GanbareGoemonDeroDeroDochuObakeTenkomori : ApiBase
    {
        public override string GameName => "Ganbare Goemon: Dero Dero Dochu Obake Tenkomori";
        public override GameID GameID => GameID.GanbareGoemonDeroDeroDochuObakeTenkomori;

        public GanbareGoemonDeroDeroDochuObakeTenkomori()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
