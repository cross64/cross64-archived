using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TransformersBeastWarsMetals64 : ApiBase
    {
        public override string GameName => "Transformers: Beast Wars Metals 64";
        public override GameID GameID => GameID.TransformersBeastWarsMetals64;

        public TransformersBeastWarsMetals64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
