using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RugratsinParisTheMovie : ApiBase
    {
        public override string GameName => "Rugrats in Paris: The Movie";
        public override GameID GameID => GameID.RugratsinParisTheMovie;

        public RugratsinParisTheMovie()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
