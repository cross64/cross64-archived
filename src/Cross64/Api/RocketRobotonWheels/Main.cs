using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RocketRobotonWheels : ApiBase
    {
        public override string GameName => "Rocket: Robot on Wheels";
        public override GameID GameID => GameID.RocketRobotonWheels;

        public RocketRobotonWheels()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
