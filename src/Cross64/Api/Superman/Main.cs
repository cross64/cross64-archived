using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Superman : ApiBase
    {
        public override string GameName => "Superman";
        public override GameID GameID => GameID.Superman;

        public Superman()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
