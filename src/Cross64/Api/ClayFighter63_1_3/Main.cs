using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ClayFighter63_1_3 : ApiBase
    {
        public override string GameName => "Clayfighter 63 1/3";
        public override GameID GameID => GameID.ClayFighter63_1_3;

        public ClayFighter63_1_3()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
