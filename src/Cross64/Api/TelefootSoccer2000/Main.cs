using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TelefootSoccer2000 : ApiBase
    {
        public override string GameName => "Telefoot Soccer 2000";
        public override GameID GameID => GameID.TelefootSoccer2000;

        public TelefootSoccer2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
