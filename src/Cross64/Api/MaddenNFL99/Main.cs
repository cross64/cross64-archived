using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MaddenNFL99 : ApiBase
    {
        public override string GameName => "Madden NFL '99";
        public override GameID GameID => GameID.MaddenNFL99;

        public MaddenNFL99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
