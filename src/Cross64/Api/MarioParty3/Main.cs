using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioParty3 : ApiBase
    {
        public override string GameName => "Mario Party 3";
        public override GameID GameID => GameID.MarioParty3;

        public MarioParty3()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
