using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioGolf64 : ApiBase
    {
        public override string GameName => "Mario Golf 64";
        public override GameID GameID => GameID.MarioGolf64;

        public MarioGolf64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
