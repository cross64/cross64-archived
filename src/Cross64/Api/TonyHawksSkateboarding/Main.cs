using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TonyHawksSkateboarding : ApiBase
    {
        public override string GameName => "Tony Hawk's Skateboarding";
        public override GameID GameID => GameID.TonyHawksSkateboarding;

        public TonyHawksSkateboarding()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
