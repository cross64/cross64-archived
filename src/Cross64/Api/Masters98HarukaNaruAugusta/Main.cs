using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Masters98HarukaNaruAugusta : ApiBase
    {
        public override string GameName => "Masters '98: Haruka Naru Augusta";
        public override GameID GameID => GameID.Masters98HarukaNaruAugusta;

        public Masters98HarukaNaruAugusta()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
