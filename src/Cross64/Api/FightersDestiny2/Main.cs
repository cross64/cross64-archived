using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FightersDestiny2 : ApiBase
    {
        public override string GameName => "Fighters Destiny 2";
        public override GameID GameID => GameID.FightersDestiny2;

        public FightersDestiny2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
