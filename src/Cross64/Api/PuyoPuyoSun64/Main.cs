using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PuyoPuyoSun64 : ApiBase
    {
        public override string GameName => "Puyo Puyo Sun 64";
        public override GameID GameID => GameID.PuyoPuyoSun64;

        public PuyoPuyoSun64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
