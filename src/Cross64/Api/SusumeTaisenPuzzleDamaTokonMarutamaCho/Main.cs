using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SusumeTaisenPuzzleDamaTokonMarutamaCho : ApiBase
    {
        public override string GameName => "Susume! Taisen Puzzle Dama: Tōkon! Marutama Chō";
        public override GameID GameID => GameID.SusumeTaisenPuzzleDamaTokonMarutamaCho;

        public SusumeTaisenPuzzleDamaTokonMarutamaCho()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
