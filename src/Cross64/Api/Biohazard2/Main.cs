using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Biohazard2 : ApiBase
    {
        public override string GameName => "Biohazard 2";
        public override GameID GameID => GameID.Biohazard2;

        public Biohazard2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
