using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KakutouDenshouFCupManiax : ApiBase
    {
        public override string GameName => "Kakutou Denshou: F-Cup Maniax";
        public override GameID GameID => GameID.KakutouDenshouFCupManiax;

        public KakutouDenshouFCupManiax()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
