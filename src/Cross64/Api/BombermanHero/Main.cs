using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BombermanHero : ApiBase
    {
        public override string GameName => "Bomberman Hero";
        public override GameID GameID => GameID.BombermanHero;

        public BombermanHero()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
