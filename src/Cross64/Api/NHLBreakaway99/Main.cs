using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NHLBreakaway99 : ApiBase
    {
        public override string GameName => "NHL Breakaway '99";
        public override GameID GameID => GameID.NHLBreakaway99;

        public NHLBreakaway99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
