using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ExtremeG : ApiBase
    {
        public override string GameName => "Extreme-G";
        public override GameID GameID => GameID.ExtremeG;

        public ExtremeG()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
