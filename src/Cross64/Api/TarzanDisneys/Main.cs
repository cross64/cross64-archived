using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TarzanDisneys : ApiBase
    {
        public override string GameName => "Tarzan, Disney's";
        public override GameID GameID => GameID.TarzanDisneys;

        public TarzanDisneys()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
