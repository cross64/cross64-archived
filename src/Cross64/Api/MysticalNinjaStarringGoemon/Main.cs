using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MysticalNinjaStarringGoemon : ApiBase
    {
        public override string GameName => "Mystical Ninja, Starring Goemon";
        public override GameID GameID => GameID.MysticalNinjaStarringGoemon;

        public MysticalNinjaStarringGoemon()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
