using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class F1RacingChampionship : ApiBase
    {
        public override string GameName => "F1 Racing Championship";
        public override GameID GameID => GameID.F1RacingChampionship;

        public F1RacingChampionship()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
