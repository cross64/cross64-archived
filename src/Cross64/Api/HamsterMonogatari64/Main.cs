using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HamsterMonogatari64 : ApiBase
    {
        public override string GameName => "Hamster Monogatari 64";
        public override GameID GameID => GameID.HamsterMonogatari64;

        public HamsterMonogatari64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
