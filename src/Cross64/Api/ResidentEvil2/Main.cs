using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ResidentEvil2 : ApiBase
    {
        public override string GameName => "Resident Evil 2";
        public override GameID GameID => GameID.ResidentEvil2;

        public ResidentEvil2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
