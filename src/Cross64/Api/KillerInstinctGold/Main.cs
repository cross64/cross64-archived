using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KillerInstinctGold : ApiBase
    {
        public override string GameName => "Killer Instinct Gold";
        public override GameID GameID => GameID.KillerInstinctGold;

        public KillerInstinctGold()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
