using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarshotSpaceCircusFever : ApiBase
    {
        public override string GameName => "Starshot: Space Circus Fever";
        public override GameID GameID => GameID.StarshotSpaceCircusFever;

        public StarshotSpaceCircusFever()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
