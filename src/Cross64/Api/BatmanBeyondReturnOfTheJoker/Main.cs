using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BatmanBeyondReturnOfTheJoker : ApiBase
    {
        public override string GameName => "Batman Beyond: Return of the Joker";
        public override GameID GameID => GameID.BatmanBeyondReturnOfTheJoker;

        public BatmanBeyondReturnOfTheJoker()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
