using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class VirtualProWrestling2OdoKeisho : ApiBase
    {
        public override string GameName => "Virtual Pro Wrestling 2: Ōdō Keishō";
        public override GameID GameID => GameID.VirtualProWrestling2OdoKeisho;

        public VirtualProWrestling2OdoKeisho()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
