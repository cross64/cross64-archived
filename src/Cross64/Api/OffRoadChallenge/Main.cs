using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class OffRoadChallenge : ApiBase
    {
        public override string GameName => "Off Road Challenge";
        public override GameID GameID => GameID.OffRoadChallenge;

        public OffRoadChallenge()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
