using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ExciteBike64 : ApiBase
    {
        public override string GameName => "Excitebike 64";
        public override GameID GameID => GameID.ExciteBike64;

        public ExciteBike64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
