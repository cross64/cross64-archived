using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GanbareNipponOlympics2000 : ApiBase
    {
        public override string GameName => "Ganbare Nippon! Olympics 2000";
        public override GameID GameID => GameID.GanbareNipponOlympics2000;

        public GanbareNipponOlympics2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
