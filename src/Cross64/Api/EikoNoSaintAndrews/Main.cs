using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class EikoNoSaintAndrews : ApiBase
    {
        public override string GameName => "Elkō No Saint Andrews";
        public override GameID GameID => GameID.EikoNoSaintAndrews;

        public EikoNoSaintAndrews()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
