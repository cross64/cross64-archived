using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class IggysReckinBalls : ApiBase
    {
        public override string GameName => "Iggy's Reckin' Balls";
        public override GameID GameID => GameID.IggysReckinBalls;

        public IggysReckinBalls()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
