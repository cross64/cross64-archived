using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class OperationWinback : ApiBase
    {
        public override string GameName => "Operation Winback";
        public override GameID GameID => GameID.OperationWinback;

        public OperationWinback()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
