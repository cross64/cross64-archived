namespace Cross64.Api
{
    public partial class SuperMario64
    {
        public enum AddressType
        {
            /// <summary> Player object </summary>
            Player,

            /// <summary> Currently active profile </summary>
            CurProfile,

            /// <summary> Currently in scene </summary>
            CurScene,

            /// <summary> Currently paused </summary>
            Paused,

            /// <summary> Star count </summary>
            Stars,

            /// <summary> File A save location </summary>
            FileA,

            /// <summary> File B save location </summary>
            FileB,

            /// <summary> File C save location </summary>
            FileC,

            /// <summary> File D save location </summary>
            FileD
        }

        public enum GameVersion
        {
            JP_1_0,
            PAL_1_0,
            USA_1_0
        }

        private void SetVersion(GameVersion version)
        {
            Addresses.Clear();

            switch (version)
            {
                case GameVersion.JP_1_0:
                    // Player Data
                    Addresses.Add((int) AddressType.Player, 0x35fde8);

                    // Runtime Data
                    // Addresses.Add((int)AddressType.CurProfile, 0x32ddf5);
                    // Addresses.Add((int)AddressType.CurScene, 0x32ddf8);
                    // Addresses.Add((int)AddressType.Paused, 0x3314f8);
                    Addresses.Add((int) AddressType.Stars, 0x339eaa);

                    // Save Data 
                    Addresses.Add((int) AddressType.FileA, 0x207b00);
                    Addresses.Add((int) AddressType.FileB, 0x207b70);
                    Addresses.Add((int) AddressType.FileC, 0x207be0);
                    Addresses.Add((int) AddressType.FileD, 0x207c50);
                    break;

                case GameVersion.PAL_1_0:
                    // // Player Data
                    // Addresses.Add((int)AddressType.Player, 0x361158);
                    //
                    // // Runtime Data
                    // Addresses.Add((int)AddressType.CurProfile, 0x32ddf5);
                    // Addresses.Add((int)AddressType.CurScene, 0x32ddf8);
                    // Addresses.Add((int)AddressType.Paused, 0x3314f8);
                    // Addresses.Add((int)AddressType.Stars, 0x33b21a);
                    //
                    // // Save Data 
                    // Addresses.Add((int)AddressType.FileA, 0x207700);
                    // Addresses.Add((int)AddressType.FileB, 0x207770);
                    // Addresses.Add((int)AddressType.FileC, 0x2077e0);
                    // Addresses.Add((int)AddressType.FileD, 0x207850);
                    break;

                case GameVersion.USA_1_0:
                    // Player Data
                    Addresses.Add((int) AddressType.Player, 0x361158);

                    // Runtime Data
                    Addresses.Add((int) AddressType.CurProfile, 0x32ddf5);
                    Addresses.Add((int) AddressType.CurScene, 0x32ddf8);
                    Addresses.Add((int) AddressType.Paused, 0x3314f8);
                    Addresses.Add((int) AddressType.Stars, 0x33b21a);

                    // Save Data 
                    Addresses.Add((int) AddressType.FileA, 0x207700);
                    Addresses.Add((int) AddressType.FileB, 0x207770);
                    Addresses.Add((int) AddressType.FileC, 0x2077e0);
                    Addresses.Add((int) AddressType.FileD, 0x207850);
                    break;
            }
        }
    }
}