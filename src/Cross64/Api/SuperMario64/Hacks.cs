namespace Cross64.Api
{
    public partial class SuperMario64
    {
        public enum HackType
        {
            Vanilla
        }

        private void DetectHackType()
        {
            // Perform checks here
            Hack = HackType.Vanilla;

            // Switch for potential romhack/decomp modification
            switch (Hack)
            {
                case HackType.Vanilla:
                    SaveDataSize = 0x70;
                    break;
            }
        }
    }
}