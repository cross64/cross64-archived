using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MajorLeagueBaseballFeaturingKenGriffeyJr : ApiBase
    {
        public override string GameName => "Major League Baseball, Featuring Ken Griffey Jr.";
        public override GameID GameID => GameID.MajorLeagueBaseballFeaturingKenGriffeyJr;

        public MajorLeagueBaseballFeaturingKenGriffeyJr()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
