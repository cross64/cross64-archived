using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DaikatanaJohnRomeros : ApiBase
    {
        public override string GameName => "Daikatana, John Romero's";
        public override GameID GameID => GameID.DaikatanaJohnRomeros;

        public DaikatanaJohnRomeros()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
