using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class QuakeII : ApiBase
    {
        public override string GameName => "Quake II";
        public override GameID GameID => GameID.QuakeII;

        public QuakeII()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
