using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FIFA98RoadtoWorldCup : ApiBase
    {
        public override string GameName => "FIFA '98: Road to World Cup";
        public override GameID GameID => GameID.FIFA98RoadtoWorldCup;

        public FIFA98RoadtoWorldCup()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
