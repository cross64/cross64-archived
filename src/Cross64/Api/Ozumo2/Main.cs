using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Ozumo2 : ApiBase
    {
        public override string GameName => "64 Ōzumō 2";
        public override GameID GameID => GameID.Ozumo2;

        public Ozumo2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
