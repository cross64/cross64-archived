using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AllStarBaseball2000 : ApiBase
    {
        public override string GameName => "All-Star Baseball 2000";
        public override GameID GameID => GameID.AllStarBaseball2000;

        public AllStarBaseball2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
