using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NHLBreakaway98 : ApiBase
    {
        public override string GameName => "NHL Breakaway '98";
        public override GameID GameID => GameID.NHLBreakaway98;

        public NHLBreakaway98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
