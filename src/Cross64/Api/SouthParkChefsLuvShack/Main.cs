using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SouthParkChefsLuvShack : ApiBase
    {
        public override string GameName => "South Park: Chef's Luv Shack";
        public override GameID GameID => GameID.SouthParkChefsLuvShack;

        public SouthParkChefsLuvShack()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
