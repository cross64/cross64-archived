using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SnowboardKids : ApiBase
    {
        public override string GameName => "Snowboard Kids";
        public override GameID GameID => GameID.SnowboardKids;

        public SnowboardKids()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
