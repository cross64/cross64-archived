using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Pachinko365Nichi : ApiBase
    {
        public override string GameName => "Pachinko 365 Nichi";
        public override GameID GameID => GameID.Pachinko365Nichi;

        public Pachinko365Nichi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
