using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBACourtsideKobeBryantin : ApiBase
    {
        public override string GameName => "NBA Courtside, Kobe Bryant in";
        public override GameID GameID => GameID.NBACourtsideKobeBryantin;

        public NBACourtsideKobeBryantin()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
