using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAInTheZone98 : ApiBase
    {
        public override string GameName => "NBA In The Zone '98";
        public override GameID GameID => GameID.NBAInTheZone98;

        public NBAInTheZone98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
