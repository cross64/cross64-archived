using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Wetrix : ApiBase
    {
        public override string GameName => "Wetrix";
        public override GameID GameID => GameID.Wetrix;

        public Wetrix()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
