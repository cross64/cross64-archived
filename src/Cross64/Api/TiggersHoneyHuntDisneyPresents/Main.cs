using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TiggersHoneyHuntDisneyPresents : ApiBase
    {
        public override string GameName => "Tigger's Honey Hunt, Disney Presents";
        public override GameID GameID => GameID.TiggersHoneyHuntDisneyPresents;

        public TiggersHoneyHuntDisneyPresents()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
