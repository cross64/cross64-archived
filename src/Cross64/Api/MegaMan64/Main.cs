using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MegaMan64 : ApiBase
    {
        public override string GameName => "Mega Man 64";
        public override GameID GameID => GameID.MegaMan64;

        public MegaMan64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
