using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarSoldierVanishingEarth : ApiBase
    {
        public override string GameName => "Star Soldier: Vanishing Earth";
        public override GameID GameID => GameID.StarSoldierVanishingEarth;

        public StarSoldierVanishingEarth()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
