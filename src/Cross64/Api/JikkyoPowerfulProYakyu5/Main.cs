using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoPowerfulProYakyu5 : ApiBase
    {
        public override string GameName => "Jikkyō Powerful Pro Yakyū 5";
        public override GameID GameID => GameID.JikkyoPowerfulProYakyu5;

        public JikkyoPowerfulProYakyu5()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
