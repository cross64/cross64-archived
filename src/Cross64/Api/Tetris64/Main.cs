using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Tetris64 : ApiBase
    {
        public override string GameName => "Tetris 64";
        public override GameID GameID => GameID.Tetris64;

        public Tetris64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
