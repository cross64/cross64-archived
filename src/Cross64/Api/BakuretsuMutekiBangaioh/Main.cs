using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BakuretsuMutekiBangaioh : ApiBase
    {
        public override string GameName => "Bakuretsu Muteki Bangaioh";
        public override GameID GameID => GameID.BakuretsuMutekiBangaioh;

        public BakuretsuMutekiBangaioh()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
