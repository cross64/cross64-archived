using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DestructionDerpy64 : ApiBase
    {
        public override string GameName => "Destruction Derby 64";
        public override GameID GameID => GameID.DestructionDerpy64;

        public DestructionDerpy64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
