using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class FZeroX : ApiBase
    {
        public override string GameName => "Currently Unavailable";
        public override GameID GameID => GameID.FZeroX;

        public FZeroX()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
