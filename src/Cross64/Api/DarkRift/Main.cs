using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DarkRift : ApiBase
    {
        public override string GameName => "Dark Rift";
        public override GameID GameID => GameID.DarkRift;

        public DarkRift()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
