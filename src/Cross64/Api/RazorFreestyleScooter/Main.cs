using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RazorFreestyleScooter : ApiBase
    {
        public override string GameName => "Razor Freestyle Scooter";
        public override GameID GameID => GameID.RazorFreestyleScooter;

        public RazorFreestyleScooter()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
