using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TurokRageWars : ApiBase
    {
        public override string GameName => "Turok: Rage Wars";
        public override GameID GameID => GameID.TurokRageWars;

        public TurokRageWars()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
