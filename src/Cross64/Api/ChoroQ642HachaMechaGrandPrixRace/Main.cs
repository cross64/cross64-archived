using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChoroQ642HachaMechaGrandPrixRace : ApiBase
    {
        public override string GameName => "Choro Q 64 2: Hacha-Mecha Grand Prix Race";
        public override GameID GameID => GameID.ChoroQ642HachaMechaGrandPrixRace;

        public ChoroQ642HachaMechaGrandPrixRace()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
