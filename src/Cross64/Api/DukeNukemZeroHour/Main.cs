using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DukeNukemZeroHour : ApiBase
    {
        public override string GameName => "Duke Nukem: Zero Hour";
        public override GameID GameID => GameID.DukeNukemZeroHour;

        public DukeNukemZeroHour()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
