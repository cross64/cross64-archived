using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WheelofFortune : ApiBase
    {
        public override string GameName => "Wheel of Fortune";
        public override GameID GameID => GameID.WheelofFortune;

        public WheelofFortune()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
