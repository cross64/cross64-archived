using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBALive2000 : ApiBase
    {
        public override string GameName => "NBA Live 2000";
        public override GameID GameID => GameID.NBALive2000;

        public NBALive2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
