using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class InternationalSuperstarSoccer2000 : ApiBase
    {
        public override string GameName => "International Superstar Soccer 2000";
        public override GameID GameID => GameID.InternationalSuperstarSoccer2000;

        public InternationalSuperstarSoccer2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
