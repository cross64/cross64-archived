using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarWarsEpisode1Racer : ApiBase
    {
        public override string GameName => "Star Wars: Episode 1 Racer";
        public override GameID GameID => GameID.StarWarsEpisode1Racer;

        public StarWarsEpisode1Racer()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
