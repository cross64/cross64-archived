using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ShinNihonProWrestlingTokonHonoMichi : ApiBase
    {
        public override string GameName => "Shin Nihon Pro Wrestling: Tōkon Honō Michi";
        public override GameID GameID => GameID.ShinNihonProWrestlingTokonHonoMichi;

        public ShinNihonProWrestlingTokonHonoMichi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
