using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioParty : ApiBase
    {
        public override string GameName => "Mario Party";
        public override GameID GameID => GameID.MarioParty;

        public MarioParty()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
