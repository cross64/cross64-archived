using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CharlieBlastsTerritory : ApiBase
    {
        public override string GameName => "Charlie Blast's Territory";
        public override GameID GameID => GameID.CharlieBlastsTerritory;

        public CharlieBlastsTerritory()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
