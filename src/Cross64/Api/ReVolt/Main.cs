using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ReVolt : ApiBase
    {
        public override string GameName => "Re-Volt";
        public override GameID GameID => GameID.ReVolt;

        public ReVolt()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
