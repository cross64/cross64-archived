using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BigMountain2000 : ApiBase
    {
        public override string GameName => "Big Mountain 2000";
        public override GameID GameID => GameID.BigMountain2000;

        public BigMountain2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
