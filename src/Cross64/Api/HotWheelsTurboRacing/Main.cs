using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HotWheelsTurboRacing : ApiBase
    {
        public override string GameName => "Hot Wheels Turbo Racing";
        public override GameID GameID => GameID.HotWheelsTurboRacing;

        public HotWheelsTurboRacing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
