using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LetsSmash : ApiBase
    {
        public override string GameName => "Let's Smash";
        public override GameID GameID => GameID.LetsSmash;

        public LetsSmash()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
