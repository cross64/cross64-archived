using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Bomberman64TheSecondAttack : ApiBase
    {
        public override string GameName => "Bomberman 64: The Second Attack";
        public override GameID GameID => GameID.Bomberman64TheSecondAttack;

        public Bomberman64TheSecondAttack()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
