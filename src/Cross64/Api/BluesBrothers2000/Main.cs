using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BluesBrothers2000 : ApiBase
    {
        public override string GameName => "Blues Brothers 2000";
        public override GameID GameID => GameID.BluesBrothers2000;

        public BluesBrothers2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
