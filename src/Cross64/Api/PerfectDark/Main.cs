using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PerfectDark : ApiBase
    {
        public override string GameName => "Perfect Dark";
        public override GameID GameID => GameID.PerfectDark;

        public PerfectDark()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
