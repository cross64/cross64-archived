using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioParty2J : ApiBase
    {
        public override string GameName => "Mario Party 2 Japanese";
        public override GameID GameID => GameID.MarioParty2J;

        public MarioParty2J()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
