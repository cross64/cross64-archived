using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HybridHeavenJ : ApiBase
    {
        public override string GameName => "Hybrid Heaven Japanese";
        public override GameID GameID => GameID.HybridHeavenJ;

        public HybridHeavenJ()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
