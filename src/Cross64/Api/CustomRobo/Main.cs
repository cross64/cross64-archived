using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CustomRobo : ApiBase
    {
        public override string GameName => "Custom Robo";
        public override GameID GameID => GameID.CustomRobo;

        public CustomRobo()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
