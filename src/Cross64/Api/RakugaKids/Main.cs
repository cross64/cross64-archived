using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RakugaKids : ApiBase
    {
        public override string GameName => "Rakuga Kids";
        public override GameID GameID => GameID.RakugaKids;

        public RakugaKids()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
