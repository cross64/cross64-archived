using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CityTourGrandprixZenNihonGTSenshuken : ApiBase
    {
        public override string GameName => "City Tour Grandprix: Zen Nihon GT Senshuken";
        public override GameID GameID => GameID.CityTourGrandprixZenNihonGTSenshuken;

        public CityTourGrandprixZenNihonGTSenshuken()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
