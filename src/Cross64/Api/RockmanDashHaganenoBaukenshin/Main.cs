using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RockmanDashHaganenoBaukenshin : ApiBase
    {
        public override string GameName => "Rockman Dash: Hagane no Baukenshin";
        public override GameID GameID => GameID.RockmanDashHaganenoBaukenshin;

        public RockmanDashHaganenoBaukenshin()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
