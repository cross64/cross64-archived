using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class F1WorldGrandPrix : ApiBase
    {
        public override string GameName => "F-1 World Grand Prix";
        public override GameID GameID => GameID.F1WorldGrandPrix;

        public F1WorldGrandPrix()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
