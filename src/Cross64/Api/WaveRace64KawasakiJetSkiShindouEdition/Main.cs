using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WaveRace64KawasakiJetSkiShindouEdition : ApiBase
    {
        public override string GameName => "Wave Race 64: Kawasaki Jet Ski Shindou Edition";
        public override GameID GameID => GameID.WaveRace64KawasakiJetSkiShindouEdition;

        public WaveRace64KawasakiJetSkiShindouEdition()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
