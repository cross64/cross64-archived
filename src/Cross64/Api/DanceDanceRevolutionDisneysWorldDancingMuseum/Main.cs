using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DanceDanceRevolutionDisneysWorldDancingMuseum : ApiBase
    {
        public override string GameName => "Dance Dance Revolution: Disney's World Dancing Museum";
        public override GameID GameID => GameID.DanceDanceRevolutionDisneysWorldDancingMuseum;

        public DanceDanceRevolutionDisneysWorldDancingMuseum()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
