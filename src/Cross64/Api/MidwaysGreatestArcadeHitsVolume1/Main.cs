using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MidwaysGreatestArcadeHitsVolume1 : ApiBase
    {
        public override string GameName => "Midway's Greatest Arcade Hits: Volume 1";
        public override GameID GameID => GameID.MidwaysGreatestArcadeHitsVolume1;

        public MidwaysGreatestArcadeHitsVolume1()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
