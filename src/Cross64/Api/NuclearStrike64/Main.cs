using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NuclearStrike64 : ApiBase
    {
        public override string GameName => "Nuclear Strike 64";
        public override GameID GameID => GameID.NuclearStrike64;

        public NuclearStrike64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
