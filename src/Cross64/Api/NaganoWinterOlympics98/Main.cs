using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NaganoWinterOlympics98 : ApiBase
    {
        public override string GameName => "Nagano Winter Olympics '98";
        public override GameID GameID => GameID.NaganoWinterOlympics98;

        public NaganoWinterOlympics98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
