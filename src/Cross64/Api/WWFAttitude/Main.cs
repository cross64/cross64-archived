using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WWFAttitude : ApiBase
    {
        public override string GameName => "WWF Attitude";
        public override GameID GameID => GameID.WWFAttitude;

        public WWFAttitude()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
