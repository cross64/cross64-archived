using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PokemonStadiumJ : ApiBase
    {
        public override string GameName => "Pokémon Stadium	Japanese";
        public override GameID GameID => GameID.PokemonStadiumJ;

        public PokemonStadiumJ()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
