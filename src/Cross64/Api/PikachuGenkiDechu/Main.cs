using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PikachuGenkiDechu : ApiBase
    {
        public override string GameName => "Pikachu Genki Dechu";
        public override GameID GameID => GameID.PikachuGenkiDechu;

        public PikachuGenkiDechu()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
