using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class QuakeP : ApiBase
    {
        public override string GameName => "Quake Korean";
        public override GameID GameID => GameID.QuakeP;

        public QuakeP()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
