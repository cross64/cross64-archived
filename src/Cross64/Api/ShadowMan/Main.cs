using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ShadowMan : ApiBase
    {
        public override string GameName => "Shadow Man";
        public override GameID GameID => GameID.ShadowMan;

        public ShadowMan()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
