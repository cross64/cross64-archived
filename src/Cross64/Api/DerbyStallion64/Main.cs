using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DerbyStallion64 : ApiBase
    {
        public override string GameName => "Derby Stallion 64";
        public override GameID GameID => GameID.DerbyStallion64;

        public DerbyStallion64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
