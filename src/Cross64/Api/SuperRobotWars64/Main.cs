using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperRobotWars64 : ApiBase
    {
        public override string GameName => "Super Robot Wars 64";
        public override GameID GameID => GameID.SuperRobotWars64;

        public SuperRobotWars64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
