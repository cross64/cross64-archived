using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ElmosNumberJouney : ApiBase
    {
        public override string GameName => "Elmo's Number Journey";
        public override GameID GameID => GameID.ElmosNumberJouney;

        public ElmosNumberJouney()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
