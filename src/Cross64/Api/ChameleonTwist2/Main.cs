using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChameleonTwist2 : ApiBase
    {
        public override string GameName => "Chameleon Twist 2";
        public override GameID GameID => GameID.ChameleonTwist2;

        public ChameleonTwist2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
