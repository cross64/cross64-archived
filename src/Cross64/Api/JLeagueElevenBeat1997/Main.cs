using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JLeagueElevenBeat1997 : ApiBase
    {
        public override string GameName => "J-League Eleven Beat 1997";
        public override GameID GameID => GameID.JLeagueElevenBeat1997;

        public JLeagueElevenBeat1997()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
