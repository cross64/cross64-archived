using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TwistedEdgeExtremeSnowboarding : ApiBase
    {
        public override string GameName => "Twisted Edge Extreme Snowboarding";
        public override GameID GameID => GameID.TwistedEdgeExtremeSnowboarding;

        public TwistedEdgeExtremeSnowboarding()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
