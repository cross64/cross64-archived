using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class BakuBomberman2 : ApiBase
    {
        public override string GameName => "Baku Bomberman 2";
        public override GameID GameID => GameID.BakuBomberman2;

        public BakuBomberman2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
