using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PuzzleBobble64 : ApiBase
    {
        public override string GameName => "Puzzle Bobble 64";
        public override GameID GameID => GameID.PuzzleBobble64;

        public PuzzleBobble64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
