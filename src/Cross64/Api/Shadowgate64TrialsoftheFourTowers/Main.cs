using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Shadowgate64TrialsoftheFourTowers : ApiBase
    {
        public override string GameName => "Shadowgate 64: Trials of the Four Towers";
        public override GameID GameID => GameID.Shadowgate64TrialsoftheFourTowers;

        public Shadowgate64TrialsoftheFourTowers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
