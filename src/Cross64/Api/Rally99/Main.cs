using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Rally99 : ApiBase
    {
        public override string GameName => "Rally '99";
        public override GameID GameID => GameID.Rally99;

        public Rally99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
