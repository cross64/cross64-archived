using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ProShinanMahjongTsuwamono64JansoBattleniChosen : ApiBase
    {
        public override string GameName => "Pro Shinan Mahjong Tsuwamono 64: Jansō Battle ni Chōsen";
        public override GameID GameID => GameID.ProShinanMahjongTsuwamono64JansoBattleniChosen;

        public ProShinanMahjongTsuwamono64JansoBattleniChosen()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
