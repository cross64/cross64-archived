using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Ready2RumbleBoxing : ApiBase
    {
        public override string GameName => "Ready 2 Rumble Boxing";
        public override GameID GameID => GameID.Ready2RumbleBoxing;

        public Ready2RumbleBoxing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
