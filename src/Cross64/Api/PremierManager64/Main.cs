using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PremierManager64 : ApiBase
    {
        public override string GameName => "Premier Manager 64";
        public override GameID GameID => GameID.PremierManager64;

        public PremierManager64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
