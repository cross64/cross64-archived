using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HerculesTheLegendaryJourneys : ApiBase
    {
        public override string GameName => "Hercules: The Legendary Journeys";
        public override GameID GameID => GameID.HerculesTheLegendaryJourneys;

        public HerculesTheLegendaryJourneys()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
