using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MarioTennis : ApiBase
    {
        public override string GameName => "Mario Tennis";
        public override GameID GameID => GameID.MarioTennis;

        public MarioTennis()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
