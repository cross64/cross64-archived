using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HiryuNoKenTsuin : ApiBase
    {
        public override string GameName => "Hiryu No Ken Tsuin";
        public override GameID GameID => GameID.HiryuNoKenTsuin;

        public HiryuNoKenTsuin()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
