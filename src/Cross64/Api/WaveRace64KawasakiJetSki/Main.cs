using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WaveRace64KawasakiJetSki : ApiBase
    {
        public override string GameName => "Wave Race 64: Kawasaki Jet Ski";
        public override GameID GameID => GameID.WaveRace64KawasakiJetSki;

        public WaveRace64KawasakiJetSki()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
