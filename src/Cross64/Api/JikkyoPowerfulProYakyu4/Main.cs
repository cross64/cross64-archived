using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoPowerfulProYakyu4 : ApiBase
    {
        public override string GameName => "Jikkyō Powerful Pro Yakyū 4";
        public override GameID GameID => GameID.JikkyoPowerfulProYakyu4;

        public JikkyoPowerfulProYakyu4()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
