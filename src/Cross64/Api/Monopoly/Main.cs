using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Monopoly : ApiBase
    {
        public override string GameName => "Monopoly";
        public override GameID GameID => GameID.Monopoly;

        public Monopoly()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
