using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HumanGrandPrixTheNewGeneration : ApiBase
    {
        public override string GameName => "Human Grand Prix: The New Generation";
        public override GameID GameID => GameID.HumanGrandPrixTheNewGeneration;

        public HumanGrandPrixTheNewGeneration()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
