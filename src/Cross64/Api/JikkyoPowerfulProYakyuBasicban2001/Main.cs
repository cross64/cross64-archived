using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoPowerfulProYakyuBasicban2001 : ApiBase
    {
        public override string GameName => "Jikkyō Powerful Pro Yakyū Basic-ban 2001";
        public override GameID GameID => GameID.JikkyoPowerfulProYakyuBasicban2001;

        public JikkyoPowerfulProYakyuBasicban2001()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
