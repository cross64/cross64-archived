using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLBlitz : ApiBase
    {
        public override string GameName => "NFL Blitz";
        public override GameID GameID => GameID.NFLBlitz;

        public NFLBlitz()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
