using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LodeRunner3D : ApiBase
    {
        public override string GameName => "Lode Runner 3-D";
        public override GameID GameID => GameID.LodeRunner3D;

        public LodeRunner3D()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
