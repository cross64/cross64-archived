using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Airboarder64 : ApiBase
    {
        public override string GameName => "Airboarder 64";
        public override GameID GameID => GameID.Airboarder64;

        public Airboarder64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
