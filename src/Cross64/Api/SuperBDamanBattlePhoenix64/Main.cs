using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperBDamanBattlePhoenix64 : ApiBase
    {
        public override string GameName => "Super B-Daman: Battle Phoenix 64";
        public override GameID GameID => GameID.SuperBDamanBattlePhoenix64;

        public SuperBDamanBattlePhoenix64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
