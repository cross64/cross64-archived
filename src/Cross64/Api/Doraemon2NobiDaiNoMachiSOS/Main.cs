using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Doraemon2NobiDaiNoMachiSOS : ApiBase
    {
        public override string GameName => "Doraemon 3: Nobi Dai No Machi SOS!";
        public override GameID GameID => GameID.Doraemon2NobiDaiNoMachiSOS;

        public Doraemon2NobiDaiNoMachiSOS()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
