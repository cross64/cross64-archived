using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ClayFighter63_1_3_SculptorsCut : ApiBase
    {
        public override string GameName => "ClayFighter 63 1/3 Sculptor's Cut";
        public override GameID GameID => GameID.ClayFighter63_1_3_SculptorsCut;

        public ClayFighter63_1_3_SculptorsCut()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
