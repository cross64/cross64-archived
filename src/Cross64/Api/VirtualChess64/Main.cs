using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class VirtualChess64 : ApiBase
    {
        public override string GameName => "Virtual Chess 64";
        public override GameID GameID => GameID.VirtualChess64;

        public VirtualChess64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
