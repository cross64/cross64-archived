using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GoldenEye007 : ApiBase
    {
        public override string GameName => "007: Golden Eye";
        public override GameID GameID => GameID.GoldenEye007;

        public GoldenEye007()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
