using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class F1PolePosition64 : ApiBase
    {
        public override string GameName => "F1 Pole Position 64";
        public override GameID GameID => GameID.F1PolePosition64;

        public F1PolePosition64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
