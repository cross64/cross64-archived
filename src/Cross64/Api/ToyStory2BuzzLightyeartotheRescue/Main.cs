using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ToyStory2BuzzLightyeartotheRescue : ApiBase
    {
        public override string GameName => "Toy Story 2: Buzz Lightyear to the Rescue";
        public override GameID GameID => GameID.ToyStory2BuzzLightyeartotheRescue;

        public ToyStory2BuzzLightyeartotheRescue()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
