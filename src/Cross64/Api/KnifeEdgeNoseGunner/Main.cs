using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class KnifeEdgeNoseGunner : ApiBase
    {
        public override string GameName => "Knife Edge: Nose Gunner";
        public override GameID GameID => GameID.KnifeEdgeNoseGunner;

        public KnifeEdgeNoseGunner()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
