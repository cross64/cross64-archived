using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DrMario64 : ApiBase
    {
        public override string GameName => "Dr. Mario 64";
        public override GameID GameID => GameID.DrMario64;

        public DrMario64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
