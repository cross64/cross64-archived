using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DoraemonNobitaTo3tsuNoSeireiIshi : ApiBase
    {
        public override string GameName => "Doraemon: Nobita to 3-tsu no Seirei Ishi";
        public override GameID GameID => GameID.DoraemonNobitaTo3tsuNoSeireiIshi;

        public DoraemonNobitaTo3tsuNoSeireiIshi()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
