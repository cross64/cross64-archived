using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLQuarterbackClub99 : ApiBase
    {
        public override string GameName => "NFL Quarterback Club '99";
        public override GameID GameID => GameID.NFLQuarterbackClub99;

        public NFLQuarterbackClub99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
