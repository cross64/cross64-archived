using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RampageWorldTour : ApiBase
    {
        public override string GameName => "Rampage: World Tour";
        public override GameID GameID => GameID.RampageWorldTour;

        public RampageWorldTour()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
