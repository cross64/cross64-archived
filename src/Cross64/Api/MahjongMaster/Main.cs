using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MahjongMaster : ApiBase
    {
        public override string GameName => "Mahjong Master";
        public override GameID GameID => GameID.MahjongMaster;

        public MahjongMaster()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
