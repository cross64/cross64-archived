using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SanFranciscoRushExtremeRacing : ApiBase
    {
        public override string GameName => "San Francisco Rush: Extreme Racing";
        public override GameID GameID => GameID.SanFranciscoRushExtremeRacing;

        public SanFranciscoRushExtremeRacing()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
