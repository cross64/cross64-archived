using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MahjongHourouKiClassic : ApiBase
    {
        public override string GameName => "Mahjong Hourou Ki Classic";
        public override GameID GameID => GameID.MahjongHourouKiClassic;

        public MahjongHourouKiClassic()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
