using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MaddenNFL2002 : ApiBase
    {
        public override string GameName => "Madden NFL 2002";
        public override GameID GameID => GameID.MaddenNFL2002;

        public MaddenNFL2002()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
