using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ArmyMenSargesHeroes : ApiBase
    {
        public override string GameName => "Army Men: Sarge's Heroes";
        public override GameID GameID => GameID.ArmyMenSargesHeroes;

        public ArmyMenSargesHeroes()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
