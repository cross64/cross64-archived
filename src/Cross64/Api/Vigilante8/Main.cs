using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Vigilante8 : ApiBase
    {
        public override string GameName => "Vigilante 8";
        public override GameID GameID => GameID.Vigilante8;

        public Vigilante8()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
