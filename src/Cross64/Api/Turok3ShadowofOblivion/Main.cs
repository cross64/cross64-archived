using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Turok3ShadowofOblivion : ApiBase
    {
        public override string GameName => "Turok 3: Shadow of Oblivion";
        public override GameID GameID => GameID.Turok3ShadowofOblivion;

        public Turok3ShadowofOblivion()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
