﻿using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class LegendofZeldaMajorasMask : ApiBase
    {
        public override string GameName => "The Legend of Zelda: Majora's Mask";
        public override GameID GameID => GameID.LegendofZeldaMajorasMask;

        public LegendofZeldaMajorasMask()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}