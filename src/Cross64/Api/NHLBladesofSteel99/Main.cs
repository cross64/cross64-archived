using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NHLBladesofSteel99 : ApiBase
    {
        public override string GameName => "NHL Blades of Steel '99";
        public override GameID GameID => GameID.NHLBladesofSteel99;

        public NHLBladesofSteel99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
