using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MoritaShogi64 : ApiBase
    {
        public override string GameName => "Morita Shogi 64";
        public override GameID GameID => GameID.MoritaShogi64;

        public MoritaShogi64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
