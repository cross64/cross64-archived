using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SuperSmashBros : ApiBase
    {
        public override string GameName => "Super Smash Bros.";
        public override GameID GameID => GameID.SuperSmashBros;

        public SuperSmashBros()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
