using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class OlympicHockey98 : ApiBase
    {
        public override string GameName => "Olympic Hockey 98";
        public override GameID GameID => GameID.OlympicHockey98;

        public OlympicHockey98()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
