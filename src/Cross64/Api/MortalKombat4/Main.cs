using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MortalKombat4 : ApiBase
    {
        public override string GameName => "Mortal Kombat 4";
        public override GameID GameID => GameID.MortalKombat4;

        public MortalKombat4()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
