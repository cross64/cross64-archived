using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class DualHeroes : ApiBase
    {
        public override string GameName => "Dual Heroes";
        public override GameID GameID => GameID.DualHeroes;

        public DualHeroes()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
