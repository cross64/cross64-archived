using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SouthPark : ApiBase
    {
        public override string GameName => "South Park";
        public override GameID GameID => GameID.SouthPark;

        public SouthPark()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
