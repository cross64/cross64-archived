using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class TonicTrouble : ApiBase
    {
        public override string GameName => "Tonic Trouble";
        public override GameID GameID => GameID.TonicTrouble;

        public TonicTrouble()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
