using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Carmageddon64 : ApiBase
    {
        public override string GameName => "Carmageddon 64";
        public override GameID GameID => GameID.Carmageddon64;

        public Carmageddon64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
