using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PokemonPuzzleLeague : ApiBase
    {
        public override string GameName => "Pokémon Puzzle League";
        public override GameID GameID => GameID.PokemonPuzzleLeague;

        public PokemonPuzzleLeague()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
