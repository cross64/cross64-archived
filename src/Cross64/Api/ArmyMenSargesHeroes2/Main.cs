using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ArmyMenSargesHeroes2 : ApiBase
    {
        public override string GameName => "Army Men: Sarge's Heroes 2";
        public override GameID GameID => GameID.ArmyMenSargesHeroes2;

        public ArmyMenSargesHeroes2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
