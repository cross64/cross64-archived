using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class VirtualPool64 : ApiBase
    {
        public override string GameName => "Virtual Pool 64";
        public override GameID GameID => GameID.VirtualPool64;

        public VirtualPool64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
