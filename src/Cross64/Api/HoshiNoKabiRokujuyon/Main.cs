using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HoshiNoKabiRokujuyon : ApiBase
    {
        public override string GameName => "Hoshi No Kābī Rokujūyon";
        public override GameID GameID => GameID.HoshiNoKabiRokujuyon;

        public HoshiNoKabiRokujuyon()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
