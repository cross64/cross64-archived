using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MsPacManMazeMadness : ApiBase
    {
        public override string GameName => "Ms. Pac-Man Maze Madness";
        public override GameID GameID => GameID.MsPacManMazeMadness;

        public MsPacManMazeMadness()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
