using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JLeagueTacticsSoccer : ApiBase
    {
        public override string GameName => "J-League Tactics Soccer";
        public override GameID GameID => GameID.JLeagueTacticsSoccer;

        public JLeagueTacticsSoccer()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
