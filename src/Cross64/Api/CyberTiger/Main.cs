using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CyberTiger : ApiBase
    {
        public override string GameName => "CyberTiger";
        public override GameID GameID => GameID.CyberTiger;

        public CyberTiger()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
