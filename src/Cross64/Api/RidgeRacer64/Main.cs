using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RidgeRacer64 : ApiBase
    {
        public override string GameName => "Ridge Racer 64";
        public override GameID GameID => GameID.RidgeRacer64;

        public RidgeRacer64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
