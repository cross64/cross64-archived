using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MicroMachines64Turbo : ApiBase
    {
        public override string GameName => "Micro Machines 64 Turbo";
        public override GameID GameID => GameID.MicroMachines64Turbo;

        public MicroMachines64Turbo()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
