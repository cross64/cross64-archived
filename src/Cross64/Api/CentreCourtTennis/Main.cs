using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CentreCourtTennis : ApiBase
    {
        public override string GameName => "Centre Court Tennis";
        public override GameID GameID => GameID.CentreCourtTennis;

        public CentreCourtTennis()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
