using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class WildChoppers : ApiBase
    {
        public override string GameName => "Wild Choppers";
        public override GameID GameID => GameID.WildChoppers;

        public WildChoppers()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
