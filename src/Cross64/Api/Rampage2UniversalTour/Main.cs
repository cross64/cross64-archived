using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Rampage2UniversalTour : ApiBase
    {
        public override string GameName => "Rampage 2: Universal Tour";
        public override GameID GameID => GameID.Rampage2UniversalTour;

        public Rampage2UniversalTour()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
