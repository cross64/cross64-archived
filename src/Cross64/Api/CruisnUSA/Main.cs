using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class CruisnUSA : ApiBase
    {
        public override string GameName => "Cruis'n USA";
        public override GameID GameID => GameID.CruisnUSA;

        public CruisnUSA()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
