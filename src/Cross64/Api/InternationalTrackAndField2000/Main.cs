using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class InternationalTrackAndField2000 : ApiBase
    {
        public override string GameName => "International Track & Field 2000";
        public override GameID GameID => GameID.InternationalTrackAndField2000;

        public InternationalTrackAndField2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
