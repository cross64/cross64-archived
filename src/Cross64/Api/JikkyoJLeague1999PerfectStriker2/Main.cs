using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JikkyoJLeague1999PerfectStriker2 : ApiBase
    {
        public override string GameName => "Jikkyō J-League 1999: Perfect Striker 2";
        public override GameID GameID => GameID.JikkyoJLeague1999PerfectStriker2;

        public JikkyoJLeague1999PerfectStriker2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
