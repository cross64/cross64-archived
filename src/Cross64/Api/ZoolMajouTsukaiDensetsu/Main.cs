using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ZoolMajouTsukaiDensetsu : ApiBase
    {
        public override string GameName => "Zool: Majou Tsukai Densetsu";
        public override GameID GameID => GameID.ZoolMajouTsukaiDensetsu;

        public ZoolMajouTsukaiDensetsu()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
