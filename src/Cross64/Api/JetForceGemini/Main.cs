using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JetForceGemini : ApiBase
    {
        public override string GameName => "Jet Force Gemini";
        public override GameID GameID => GameID.JetForceGemini;

        public JetForceGemini()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
