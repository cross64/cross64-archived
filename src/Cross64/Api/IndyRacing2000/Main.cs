using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class IndyRacing2000 : ApiBase
    {
        public override string GameName => "Indy Racing 2000";
        public override GameID GameID => GameID.IndyRacing2000;

        public IndyRacing2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
