using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RainbowSixTomClancys : ApiBase
    {
        public override string GameName => "Rainbow Six, Tom Clancy's";
        public override GameID GameID => GameID.RainbowSixTomClancys;

        public RainbowSixTomClancys()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
