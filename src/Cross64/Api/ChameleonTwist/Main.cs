using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ChameleonTwist : ApiBase
    {
        public override string GameName => "Chameleon Twist";
        public override GameID GameID => GameID.ChameleonTwist;

        public ChameleonTwist()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
