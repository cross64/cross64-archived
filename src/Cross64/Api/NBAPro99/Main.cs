using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NBAPro99 : ApiBase
    {
        public override string GameName => "NBA Pro '99";
        public override GameID GameID => GameID.NBAPro99;

        public NBAPro99()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
