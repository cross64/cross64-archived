using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class StarWarsShadowsoftheEmpire : ApiBase
    {
        public override string GameName => "Star Wars: Shadows of the Empire";
        public override GameID GameID => GameID.StarWarsShadowsoftheEmpire;

        public StarWarsShadowsoftheEmpire()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
