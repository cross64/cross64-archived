using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NightmareCreatures : ApiBase
    {
        public override string GameName => "Nightmare Creatures";
        public override GameID GameID => GameID.NightmareCreatures;

        public NightmareCreatures()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
