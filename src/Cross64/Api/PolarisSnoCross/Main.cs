using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class PolarisSnoCross : ApiBase
    {
        public override string GameName => "Polaris SnoCross";
        public override GameID GameID => GameID.PolarisSnoCross;

        public PolarisSnoCross()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
