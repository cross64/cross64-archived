using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GauntletLegendsJ : ApiBase
    {
        public override string GameName => "Gauntlet Legends Japanese";
        public override GameID GameID => GameID.GauntletLegendsJ;

        public GauntletLegendsJ()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
