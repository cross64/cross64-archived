using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class GetterLoveChoRenaiPartyGame : ApiBase
    {
        public override string GameName => "Getter Love!!: Cho Renai Party Game";
        public override GameID GameID => GameID.GetterLoveChoRenaiPartyGame;

        public GetterLoveChoRenaiPartyGame()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
