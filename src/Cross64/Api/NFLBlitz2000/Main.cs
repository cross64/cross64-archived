using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class NFLBlitz2000 : ApiBase
    {
        public override string GameName => "NFL Blitz 2000";
        public override GameID GameID => GameID.NFLBlitz2000;

        public NFLBlitz2000()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
