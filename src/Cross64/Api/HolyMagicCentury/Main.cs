using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class HolyMagicCentury : ApiBase
    {
        public override string GameName => "Holy Magic Century";
        public override GameID GameID => GameID.HolyMagicCentury;

        public HolyMagicCentury()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
