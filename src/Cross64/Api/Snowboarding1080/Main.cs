using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Snowboarding1080 : ApiBase
    {
        public override string GameName => "1080° Snowboarding";
        public override GameID GameID => GameID.Snowboarding1080;

        public Snowboarding1080()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
