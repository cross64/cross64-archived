using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Robotron64 : ApiBase
    {
        public override string GameName => "Robotron 64";
        public override GameID GameID => GameID.Robotron64;

        public Robotron64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
