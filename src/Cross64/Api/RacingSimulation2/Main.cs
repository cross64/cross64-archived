using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class RacingSimulation2 : ApiBase
    {
        public override string GameName => "Racing Simulation 2";
        public override GameID GameID => GameID.RacingSimulation2;

        public RacingSimulation2()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
