using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class AeroGauge : ApiBase
    {
        public override string GameName => "Aero Gauge";
        public override GameID GameID => GameID.AeroGauge;

        public AeroGauge()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
