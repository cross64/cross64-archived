using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class MultiRacingChampionship : ApiBase
    {
        public override string GameName => "Multi Racing Championship";
        public override GameID GameID => GameID.MultiRacingChampionship;

        public MultiRacingChampionship()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
