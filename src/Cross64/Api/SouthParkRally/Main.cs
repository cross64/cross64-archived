using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class SouthParkRally : ApiBase
    {
        public override string GameName => "South Park Rally";
        public override GameID GameID => GameID.SouthParkRally;

        public SouthParkRally()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
