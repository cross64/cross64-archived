using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class ItoiShigesatosNo1BassFishingDefinitiveEdition : ApiBase
    {
        public override string GameName => "Itoi Shigesato's No. 1 Bass Fishing: Definitive Edition";
        public override GameID GameID => GameID.ItoiShigesatosNo1BassFishingDefinitiveEdition;

        public ItoiShigesatosNo1BassFishingDefinitiveEdition()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
