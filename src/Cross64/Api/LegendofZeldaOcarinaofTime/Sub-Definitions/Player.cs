﻿using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime
    {
        public class PlayerDef
        {
            private Emulator.Pointer _instance = Addresses[(int) AddressType.Player];
            private Emulator.Pointer addrSword = Addresses[(int) AddressType.Save] + 0x71;

            private Utility.Vector3 oldPos = new();
            private Utility.Vector3 newPos = new();
            private Utility.Vector3 oldVel = new();
            private Utility.Vector3 newVel = new();

            public ushort AnimationId => _instance.Read16(0x1ae);

            public ushort AnimationFrame => _instance.Read16(0x1f4);

            public ushort ActorId => _instance.U16;

            public ActorType ActorType => (ActorType) _instance.Read8(0x2);

            public byte Room
            {
                get => _instance.Read8(0x3);
                set => _instance.Write8(value, 0x3);
            }

            public uint RenderingFlag
            {
                get => _instance.Read32(0x4);
                set => _instance.Write32(value, 0x4);
            }

            public ushort Variable => _instance.Read16(0x1c);

            public byte ObjectTableIndex => _instance.Read8(0x1e);

            public ushort SoundEffect
            {
                get => _instance.Read16(0x20);
                set => _instance.Write16(value, 0x20);
            }

            public byte Health
            {
                get => _instance.Read8(0xaf);
                set => _instance.Write8(value, 0xaf);
            }

            public short RedeadFreeze
            {
                get => (short) _instance.Read16(0x110);
                set => _instance.Write16((ushort) value, 0x110);
            }

            public bool Exists => _instance.U32 == 0x2ff;

            public TunicType Tunic
            {
                get => (TunicType) _instance.Read8(0x013c);
                set => _instance.Write8((byte) value, 0x013c);
            }

            public ShieldType Shield
            {
                get => (ShieldType) _instance.Read8(0x013e);
                set => _instance.Write8((byte) value, 0x013e);
            }

            public SwordType Sword
            {
                get => (SwordType) addrSword.U8;
                set => addrSword.Write8((byte) value);
            }

            public BootsType Boots
            {
                get => (BootsType) _instance.Read8(0x013f);
                set => _instance.Write8((byte) value, 0x013f);
            }

            public MaskType Mask
            {
                get => (MaskType) _instance.Read8(0x014f);
                set => _instance.Write8((byte) value, 0x014f);
            }

            #region Position

            public Utility.Vector3 PrevPosition => new(oldPos);
            public float PrevPosX => oldPos.X;
            public float PrevPosY => oldPos.Y;
            public float PrevPosZ => oldPos.Z;

            public Utility.Vector3 Position
            {
                get => new(newPos);
                set
                {
                    newPos = value;
                    _instance.WriteB(value.ToArray(), 0x24);
                }
            }

            public float PosX
            {
                get => newPos.X;
                set
                {
                    newPos.X = value;
                    _instance.WriteF(value, 0x24);
                }
            }

            public float PosY
            {
                get => newPos.Y;
                set
                {
                    newPos.Y = value;
                    _instance.WriteF(value, 0x28);
                }
            }

            public float PosZ
            {
                get => newPos.Z;
                set
                {
                    newPos.Z = value;
                    _instance.WriteF(value, 0x2c);
                }
            }

            #endregion

            #region Velocity

            public Utility.Vector3 PrevVelocity => new(oldVel);
            public float PrevVelX => oldVel.X;
            public float PrevVelY => oldVel.Y;
            public float PrevVelZ => oldVel.Z;

            public Utility.Vector3 Velocity
            {
                get => new(newVel);
                set
                {
                    newVel = value;
                    _instance.WriteB(value.ToArray(), 0x5c);
                }
            }

            public float VelX
            {
                get => newVel.X;
                set
                {
                    newVel.X = value;
                    _instance.WriteF(value, 0x5c);
                }
            }

            public float VelY
            {
                get => newVel.Y;
                set
                {
                    newVel.Y = value;
                    _instance.WriteF(value, 0x60);
                }
            }

            public float VelZ
            {
                get => newVel.Z;
                set
                {
                    newVel.Z = value;
                    _instance.WriteF(value, 0x64);
                }
            }

            #endregion

            #region Rotation

            public Utility.Vector3H Rotation
            {
                get => new(_instance.ReadB(6, 0xb4));
                set => _instance.WriteB(value.ToArray(), 0xb4);
            }

            public short RotX
            {
                get => (short) _instance.Read16(0xb4);
                set => _instance.Write16((ushort) value, 0xb4);
            }

            public short RotY
            {
                get => (short) _instance.Read16(0xb6);
                set => _instance.Write16((ushort) value, 0xb6);
            }

            public short RotZ
            {
                get => (short) _instance.ReadF(0xb8);
                set => _instance.Write16((ushort) value, 0xb8);
            }

            #endregion

            #region States

            public enum State1 : uint
            {
                /// <summary> Changing maps </summary>
                SceneTransition = 0x00000001,

                /// <summary> Changing between child and adult at the Master Sword </summary>
                Transforming = 0x00000002,

                /// <summary> Transition state from hanging to standing on the ledge </summary>
                ClimbingLedge = 0x00000004,

                /// <summary> Holding bow or hoookshot </summary>
                HoldingRangedWeapon = 0x00000008,

                /// <summary> Z-Targeting an enemy </summary>
                LockedOnEnemy = 0x00000010,

                /// <summary> In conversation with npc </summary>
                Talking = 0x00000040,

                /// <summary> Playing death animation </summary>
                Dying = 0x00000080,

                /// <summary> Item collection that causes the 'get item' animation </summary>
                GettingItem = 0x00000400,

                /// <summary> Holding rock, grass, Ruto, etc </summary>
                HoldingActor = 0x00000800,

                /// <summary> Holding links sword out, whether glowing or not </summary>
                ChargingSword = 0x00001000,

                /// <summary> Holding onto the ledge waiting to drop or climb up </summary>
                HangingFromLedge = 0x00002000,

                /// <summary> Transition state from swimming to standing on the ledge </summary>
                ClimbingOutOfWater = 0x00004000,

                /// <summary>
                /// Generic 'IsZTargeting'.
                /// Should be active during LockedOnEnemy,
                /// LockedOnOther, and CameraFixed
                /// </summary>
                ZTargeting = 0x00008000,

                /// <summary>
                /// Locked on any non enemy target like friendly npcs,
                /// the target rocks, and signs etc
                /// </summary>
                LockedOnOther = 0x00010000,

                /// <summary> When Z targetting and no target is locked on </summary>
                CameraFixed = 0x00020000,

                /// <summary>
                /// Link is mid-jump or falling from low enough
                /// he wont receive fall damage
                /// </summary>
                Jumping = 0x00040000,

                /// <summary> Enter state of falling where link received fall damage </summary>
                Falling = 0x00080000,

                /// <summary> Looking around (C-Up) </summary>
                FirstPerson = 0x00100000,

                /// <summary> Climbing vines or web and have control over links movement </summary>
                Climbing = 0x00200000,

                /// <summary> Holding links shield out </summary>
                Shielding = 0x00400000,

                /// <summary> Sitting on the horse </summary>
                RidingEpona = 0x00800000,

                /// <summary> Flashing red </summary>
                Damaged = 0x04000000,

                /// <summary> In the water </summary>
                Swimming = 0x08000000,

                /// <summary>
                /// Using a non weapon item such as
                /// Ocarina, Event item, Bottle, Spells
                /// </summary>
                UseItem = 0x10000000,

                /// <summary>
                /// Used in cutscenes. Also differentiates between
                /// falling into a grotto and falling into a void
                /// </summary>
                Busy = 0x20000000,

                /// <summary> Used for falling into grottos/short voids </summary>
                DisabledFloorCollision = 0x80000000
            }

            public enum State2 : uint
            {
                /// <summary> Can talk to sign or npc </summary>
                CanRead = 0x00000002,

                /// <summary> Attached to enemy </summary>
                ConnectedToEnemy = 0x00000080,

                /// <summary>
                /// Is oriented horizontal - eg:
                /// in crawlspace, hookshot or
                /// vine grab state or being knocked back
                /// </summary>
                Horizontal = 0x00000040,

                /// <summary> Below water, whether diving or not </summary>
                UnderWater = 0x00000400,

                /// <summary> In water diving </summary>
                Diving = 0x00000800,

                /// <summary> Z-Targeting a target </summary>
                ZTarget = 0x00002000,

                /// <summary> Standing in-front of a crawlspace </summary>
                CanCrawl = 0x00010000,

                /// <summary> Inside and colliding with crawl space </summary>
                Crawling = 0x00040000,

                /// <summary> When crawling or Z-Targeting </summary>
                CameraFixed = 0x00080000,

                /// <summary> When navi is floating around links head or target </summary>
                NaviOut = 0x00100000,

                /// <summary> Navi wants to talk </summary>
                NaviAlert = 0x00200000,

                /// <summary> Idle Animation 1 </summary>
                Idle1 = 0x01000000,

                /// <summary> Playing Ocarina </summary>
                Ocarina = 0x08000000,

                /// <summary> Idle Animation 2 </summary>
                Idle2 = 0x10000000,

                /// <summary> Shopping </summary>
                Shopping = 0x20000000,

                /// <summary>
                /// Sword attack while moving forward
                /// Includes moving while releasing sword beam
                /// and forward with B
                /// </summary>
                MoveSword = 0x40000000,
            }

            public uint GetState1()
            {
                return _instance.Read32(0x066c);
            }

            public uint GetState2()
            {
                return _instance.Read32(0x0670);
            }

            public void SetState1(uint value)
            {
                _instance.Write32(value, 0x066c);
            }

            public void SetState2(uint value)
            {
                _instance.Write32(value, 0x0670);
            }

            public void AddState1(uint value)
            {
                var curState = _instance.Read32(0x066c);
                curState |= (ushort) value;
                _instance.Write32(curState, 0x066c);
            }

            public void AddState2(uint value)
            {
                var curState = _instance.Read32(0x0670);
                curState |= (ushort) value;
                _instance.Write32(curState, 0x0670);
            }

            public void AddState1(params State1[] values)
            {
                var curState = _instance.Read32(0x066c);
                foreach (var state in values)
                    curState |= (ushort) state;
                _instance.Write32(curState, 0x066c);
            }

            public void AddState2(params State2[] values)
            {
                var curState = _instance.Read32(0x0670);
                foreach (var state in values)
                    curState |= (ushort) state;
                _instance.Write32(curState, 0x0670);
            }

            public void RemoveState1(uint value)
            {
                var curState = _instance.Read32(0x066c);
                curState &= (ushort) ~value;
                _instance.Write32(curState, 0x066c);
            }

            public void RemoveState2(uint value)
            {
                var curState = _instance.Read32(0x0670);
                curState &= (ushort) ~value;
                _instance.Write32(curState, 0x0670);
            }

            public void RemoveState1(params State1[] values)
            {
                var curState = _instance.Read32(0x066c);
                foreach (var state in values)
                    curState |= (ushort) ~state;
                _instance.Write32(curState, 0x066c);
            }

            public void RemoveState2(params State2[] values)
            {
                var curState = _instance.Read32(0x0670);
                foreach (var state in values)
                    curState &= (ushort) ~state;
                _instance.Write32(curState, 0x0670);
            }

            public bool IsState1(uint value)
            {
                return (_instance.Read32(0x066c) & value) != 0;
            }

            public bool IsState2(uint value)
            {
                return (_instance.Read32(0x066c) & value) != 0;
            }

            public bool IsState1(params State1[] values)
            {
                var curState = _instance.Read32(0x066c);
                foreach (var state in values)
                    if ((curState & (uint) state) != 0)
                        return true;
                return false;
            }

            public bool IsState2(params State2[] values)
            {
                var curState = _instance.Read32(0x0670);
                foreach (var state in values)
                    if ((curState & (uint) state) != 0)
                        return true;
                return false;
            }

            #endregion

            internal void OnTick()
            {
                oldPos = newPos;
                oldVel = newVel;

                newPos = new Utility.Vector3(_instance.ReadB(12, 0x24));
                newVel = new Utility.Vector3(_instance.ReadB(12, 0x5c));
            }

            #region Utility

            public bool Moved => newPos != oldPos;

            public bool Moving => newVel != oldVel;

            public bool Elevating => oldPos.Y < newPos.Y;

            public bool Lowering => oldPos.Y > newPos.Y;

            public void MultiplyPosition(float x = 1.0f, float y = 1.0f, float z = 1.0f)
            {
                PosX += ((newPos.X - oldPos.X) * x);
                PosY += ((newPos.Y - oldPos.Y) * y);
                PosZ += ((newPos.Z - oldPos.Z) * z);
            }

            public void ResetPosition(bool x = false, bool y = false, bool z = false)
            {
                if (x) newPos.X = oldPos.X;
                if (y) newPos.Y = oldPos.Y;
                if (z) newPos.Z = oldPos.Z;
            }

            #endregion
        }
    }
}