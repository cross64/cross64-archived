﻿using Cross64.Runtimes;

namespace Cross64.Api
{
    public partial class LegendofZeldaOcarinaofTime
    {
        public class RuntimeDef
        {
            private Emulator.Pointer _interactState = Addresses[(int) AddressType.InteractGuiState];

            public ushort GetInteractState()
            {
                return _interactState.U16;
            }

            public void SetInteractState(ushort value)
            {
                _interactState.Write16(value);
            }

            public void SetInteractState(InteractState value)
            {
                _interactState.Write16((ushort) value);
            }

            public bool IsInteractState(params InteractState[] values)
            {
                var curState = _interactState.U16;
                foreach (var state in values)
                    if (curState == (ushort) state)
                        return true;
                return false;
            }

            public enum InteractState : ushort
            {
                Attack = 0x00,
                Check = 0x01,
                Blank = 0x0a,
                PutAway = 0x13,
                Speak = 0x0f
            }
        }
    }
}