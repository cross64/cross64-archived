using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class JLeagueLive64 : ApiBase
    {
        public override string GameName => "J-League Live 64";
        public override GameID GameID => GameID.JLeagueLive64;

        public JLeagueLive64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
