using System;
using Cross64.Runtimes.Modding;

namespace Cross64.Api
{
    public partial class Quest64 : ApiBase
    {
        public override string GameName => "Quest 64";
        public override GameID GameID => GameID.Quest64;

        public Quest64()
        {
        }

        internal override void Initialize()
        {
        }

        internal override void Destroy()
        {
        }

        public override void OnTick(uint frame)
        {
        }
    }
}
