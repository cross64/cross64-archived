﻿using System;
using System.Runtime.InteropServices;

namespace Cross64
{
    internal static partial class M64P
    {
        public struct Size2D
        {
            public uint uiWidth;
            public uint uiHeight;
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncInit();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncQuit();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncListModes(ref Size2D[] sizeArray, out int numSizes);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncListRates(Size2D[] size, out int numRates, out int rates);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncSetMode(int width, int height, int bitsPerPixel, VideoMode screenMode,
            VideoFlags flags);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncSetModeWithRate(int width, int height, int refreshRate, int bitsPerPixel,
            VideoMode screenMode, VideoFlags flags);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate IntPtr _VidExtFuncGLGetProc(string proc);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncGLSetAttr(GL_Attribute attr, int value);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncGLGetAttr(GL_Attribute attr, out int pValue);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncGLSwapBuf();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncSetCaption(string title);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncToggleFS();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _VidExtFuncResizeWindow(int width, int height);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate uint _VidExtFuncGLGetDefaultFramebuffer();

        public static VidExtFuncList VidExtFuncs = new (true);
        public struct VidExtFuncList
        {
            public uint FuncCount;
            public _VidExtFuncInit Init;
            public _VidExtFuncQuit Quit;
            public _VidExtFuncListModes ListModes;
            public _VidExtFuncListRates ListRates;
            public _VidExtFuncSetMode SetMode;
            public _VidExtFuncSetModeWithRate SetModeWithRate;
            public _VidExtFuncGLGetProc GLGetProc;
            public _VidExtFuncGLSetAttr GLSetAttr;
            public _VidExtFuncGLGetAttr GLGetAttr;
            public _VidExtFuncGLSwapBuf GLSwapBuf;
            public _VidExtFuncSetCaption SetCaption;
            public _VidExtFuncToggleFS ToggleFS;
            public _VidExtFuncResizeWindow ResizeWindow;
            public _VidExtFuncGLGetDefaultFramebuffer GLGetDefaultFramebuffer;

            public VidExtFuncList(bool udt)
            {
                FuncCount = 0; // Set to 14 to fix
                Init = VidExtInit;
                Quit = VidExtQuit;
                ListModes = VidExtListModes;
                ListRates = VidExtListRates;
                SetMode = VidExtSetMode;
                SetModeWithRate = VidExtSetModeWithRate;
                GLGetProc = VidExtGLGetProc;
                GLSetAttr = VidExtGLSetAttr;
                GLGetAttr = VidExtGLGetAttr;
                GLSwapBuf = VidExtGLSwapBuf;
                SetCaption = VidExtSetCaption;
                ToggleFS = VidExtToggleFS;
                ResizeWindow = VidExtResizeWindow;
                GLGetDefaultFramebuffer = VidExtGLGetDefaultFramebuffer;
            }

            public static ErrorCode VidExtInit()
            {
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtQuit()
            {
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtListModes(ref Size2D[] sizeArray, out int numSizes)
            {
                Console.WriteLine("Called Ext ListModes");
                if (sizeArray != null && sizeArray.Length > 0)
                {
                    sizeArray[0].uiWidth = 800;
                    sizeArray[0].uiHeight = 600;
                    numSizes = 1;
                }
                else numSizes = 0;

                return ErrorCode.Success;
            }

            public static ErrorCode VidExtListRates(Size2D[] size, out int numRates, out int rates)
            {
                Console.WriteLine("Called Ext ListRates");
                numRates = 0;
                rates = 0;
                return ErrorCode.Unsupported;
            }

            public static ErrorCode VidExtSetMode(int width, int height, int bitsPerPixel, VideoMode screenMode,
                VideoFlags flags)
            {
                Console.WriteLine("Called Ext SetMode");
                MakeCurrent();
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtSetModeWithRate(int width, int height, int refreshRate, int bitsPerPixel,
                VideoMode screenMode, VideoFlags flags)
            {
                Console.WriteLine("Called Ext SetModeWithRate");
                return ErrorCode.Unsupported;
            }

            public static IntPtr VidExtGLGetProc(string proc)
            {
                Console.WriteLine("Called Ext GetProc");
                return IntPtr.Zero; // SDL.SDL_GL_GetProcAddress(proc);
            }

            public static ErrorCode VidExtGLSetAttr(GL_Attribute attr, int value)
            {
                Console.WriteLine("Called Ext GLSetAttr");

                // SDL.SDL_GLattr sdlAttr;
                //
                // switch (attr)
                // {
                //     case GL_Attribute.DoubleBuffer:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_DOUBLEBUFFER;
                //         break;
                //     case GL_Attribute.BufferSize:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_BUFFER_SIZE;
                //         break;
                //     case GL_Attribute.DepthSize:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_DEPTH_SIZE;
                //         break;
                //     case GL_Attribute.RedSize:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_RED_SIZE;
                //         break;
                //     case GL_Attribute.GreenSize:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_GREEN_SIZE;
                //         break;
                //     case GL_Attribute.BlueSize:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_BLUE_SIZE;
                //         break;
                //     case GL_Attribute.AlphaSize:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_ALPHA_SIZE;
                //         break;
                //     case GL_Attribute.SwapControl:
                //         return ErrorCode.Success;
                //     case GL_Attribute.MultiSampleBuffers:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_MULTISAMPLEBUFFERS;
                //         break;
                //     case GL_Attribute.MultiSampleSamples:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_MULTISAMPLESAMPLES;
                //         break;
                //     case GL_Attribute.ContextMajorVersion:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_CONTEXT_MAJOR_VERSION;
                //         break;
                //     case GL_Attribute.ContextMinorVersion:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_CONTEXT_MINOR_VERSION;
                //         break;
                //     case GL_Attribute.ContextProfileMask:
                //         sdlAttr = SDL.SDL_GLattr.SDL_GL_CONTEXT_PROFILE_MASK;
                //         break;
                //     default:
                //         return ErrorCode.SystemFail;
                // }
                //
                // if (SDL.SDL_GL_SetAttribute(sdlAttr, value) != 0)
                //     return ErrorCode.SystemFail;
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtGLGetAttr(GL_Attribute Attr, out int pValue)
            {
                Console.WriteLine("Called Ext GLGetAttr");
                // Needs impl
                pValue = 0;
                return ErrorCode.Unsupported;
            }

            public static ErrorCode VidExtGLSwapBuf()
            {
                Console.WriteLine("Called Ext SwapBuffer");
                // SDL.SDL_GL_SwapWindow(Window.Handle);
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtSetCaption(string title)
            {
                Console.WriteLine("Called Ext SetCaption");
                // Do Something?
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtToggleFS()
            {
                Console.WriteLine("Called Ext ToggleFS");
                // Needs impl
                return ErrorCode.Success;
            }

            public static ErrorCode VidExtResizeWindow(int width, int height)
            {
                Console.WriteLine("Called Ext ResizeWindow");
                // Needs impl
                return ErrorCode.Success;
            }

            public static uint VidExtGLGetDefaultFramebuffer()
            {
                Console.WriteLine("Called Ext DefaultFrameBuffer");
                return 0;
            }

            private static void MakeCurrent()
            {
                // var context = SDL.SDL_GL_GetCurrentContext();
                // var err = SDL.SDL_GL_MakeCurrent(Window.Handle, context);
                // if (err != 0) throw new Exception($@"SDL Swap Buffer: {SDL.SDL_GetError()}");
            }
        }
    }
}