﻿using System;
using System.Runtime.InteropServices;

namespace Cross64
{
    internal static partial class M64P
    {
        #region Rdram

        /// <summary>
        /// Returns a pointer to the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate IntPtr _GetRdRam();

        public static _GetRdRam GetRdRam = null!;

        /// <summary>
        /// Returns the RdRam size
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int _GetRdRamSize();

        public static _GetRdRamSize GetRdRamSize = null!;

        #region Read

        /// <summary>
        /// Returns a byte[] from the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate IntPtr _ReadRdRamBuffer(uint address, int length);

        public static _ReadRdRamBuffer ReadRdRamBuffer = null!;

        /// <summary>
        /// Returns a ulong from the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ulong _ReadRdRam64(uint address);

        public static _ReadRdRam64 ReadRdRam64 = null!;

        /// <summary>
        /// Returns a uint from the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate uint _ReadRdRam32(uint address);

        public static _ReadRdRam32 ReadRdRam32 = null!;

        /// <summary>
        /// Returns a ushort from the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ushort _ReadRdRam16(uint address);

        public static _ReadRdRam16 ReadRdRam16 = null!;

        /// <summary>
        /// Returns a byte from the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate byte _ReadRdRam8(uint address);

        public static _ReadRdRam8 ReadRdRam8 = null!;

        #endregion

        #region Write

        /// <summary>
        /// Writes a byte[] to the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRdRamBuffer(uint address, byte[] value, int length);

        public static _WriteRdRamBuffer WriteRdRamBuffer = null!;

        /// <summary>
        /// Writes a ulong to the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRdRam64(uint address, ulong value);

        public static _WriteRdRam64 WriteRdRam64 = null!;

        /// <summary>
        /// Writes a uint to the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRdRam32(uint address, uint value);

        public static _WriteRdRam32 WriteRdRam32 = null!;

        /// <summary>
        /// Writes a ushort to the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRdRam16(uint address, ushort value);

        public static _WriteRdRam16 WriteRdRam16 = null!;

        /// <summary>
        /// Writes a byte to the RdRam
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRdRam8(uint address, byte value);

        public static _WriteRdRam8 WriteRdRam8 = null!;

        #endregion

        #endregion

        #region Rom

        /// <summary>
        /// Returns a pointer to the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate IntPtr _GetRom();

        public static _GetRom GetRom = null!;

        /// <summary>
        /// Returns the Rom size
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int _GetRomSize();

        public static _GetRomSize GetRomSize = null!;

        #region Read

        /// <summary>
        /// Returns a byte[] from the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate IntPtr _ReadRomBuffer(uint address, int length);

        public static _ReadRomBuffer ReadRomBuffer = null!;

        /// <summary>
        /// Returns a ulong from the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ulong _ReadRom64(uint address);

        public static _ReadRom64 ReadRom64 = null!;

        /// <summary>
        /// Returns a uint from the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate uint _ReadRom32(uint address);

        public static _ReadRom32 ReadRom32 = null!;

        /// <summary>
        /// Returns a ushort from the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ushort _ReadRom16(uint address);

        public static _ReadRom16 ReadRom16 = null!;

        /// <summary>
        /// Returns a byte from the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate byte _ReadRom8(uint address);

        public static _ReadRom8 ReadRom8 = null!;

        #endregion

        #region Write

        /// <summary>
        /// Writes a byte[] to the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRomBuffer(uint address, byte[] value, int length);

        public static _WriteRomBuffer WriteRomBuffer = null!;

        /// <summary>
        /// Writes a ulong to the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRom64(uint address, ulong value);

        public static _WriteRom64 WriteRom64 = null!;

        /// <summary>
        /// Writes a uint to the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRom32(uint address, uint value);

        public static _WriteRom32 WriteRom32 = null!;

        /// <summary>
        /// Writes a ushort to the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRom16(uint address, ushort value);

        public static _WriteRom16 WriteRom16 = null!;

        /// <summary>
        /// Writes a byte to the Rom
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _WriteRom8(uint address, byte value);

        public static _WriteRom8 WriteRom8 = null!;

        #endregion

        #endregion

        /// <summary>
        /// Refreshes the dynarec cache to reload new ASM injection
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _RefreshDynarec();

        public static _RefreshDynarec RefreshDynarec = null!;
    }
}