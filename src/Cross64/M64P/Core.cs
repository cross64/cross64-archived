﻿using System;
using System.Runtime.InteropServices;
using Cross64.Runtimes;

namespace Cross64
{
    internal static partial class M64P
    {
        /// <summary>
        /// Handles a debug message from mupen64plus
        /// </summary>
        /// <param name="context"></param>
        /// <param name="level"></param>
        /// <param name="message">The message to display</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _DebugCallback(IntPtr context, int level, string message);

        public static readonly _DebugCallback DebugCallback = Emulator.LogMupen;


        /// <summary>
        /// Handles a debug message from mupen64plus
        /// </summary>
        /// <param name="context"></param>
        /// <param name="param"></param>
        /// <param name="level"></param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void StateCallback(IntPtr context, CoreParam param, int level);

        /// <summary>
        /// Initializes the the core DLL
        /// </summary>
        /// <param name="apiVersion">Specifies what API version our app is using. Just set this to 0x20001</param>
        /// <param name="configPath">Directory to have the DLL look for config data. "" seems to disable this</param>
        /// <param name="dataPath">Directory to have the DLL look for user data. "" seems to disable this</param>
        /// <param name="context">Use "Core"</param>
        /// <param name="debugCallback">A function to use when the core wants to output debug messages</param>
        /// <param name="context2">Use ""</param>
        /// <param name="stateCallback">Use null</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreStartup(int apiVersion, string configPath, string dataPath, IntPtr context,
            _DebugCallback debugCallback, IntPtr context2, StateCallback stateCallback);

        public static _CoreStartup CoreStartup = null!;

        /// <summary>
        /// Cleans up the core
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreShutdown();

        public static _CoreShutdown CoreShutdown = null!;

        /// <summary>
        /// Connects a plugin DLL to the core DLL
        /// </summary>
        /// <param name="pluginType">The type of plugin that is being connected</param>
        /// <param name="pluginLibHandle">The DLL handle for the plugin</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreAttachPlugin(PluginType pluginType, IntPtr pluginLibHandle);

        public static _CoreAttachPlugin CoreAttachPlugin = null!;

        /// <summary>
        /// Disconnects a plugin DLL from the core DLL
        /// </summary>
        /// <param name="pluginType">The type of plugin to be disconnected</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreDetachPlugin(PluginType pluginType);

        public static _CoreDetachPlugin CoreDetachPlugin = null!;

        #region CoreDoCommand

        /// <summary>
        /// This is a generic reference for callback functions
        /// </summary>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void _Callback(uint val);

        public static readonly _Callback FrameCallback = Program.FrameCallback;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreDoCommand(Command command, int param, IntPtr reference);

        public static _CoreDoCommand CoreDoCommand = null!;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreDoCommandBytes(Command command, int param, byte[] reference);

        public static _CoreDoCommandBytes CoreDoCommandBytes = null!;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreDoCommandString(Command command, int param, ref string reference);

        public static _CoreDoCommandString CoreDoCommandString = null!;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreDoCommandInt(Command command, int param, ref int reference);

        public static _CoreDoCommandInt CoreDoCommandInt = null!;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreDoCommandCallback(Command command, int param, _Callback reference);

        public static _CoreDoCommandCallback CoreDoCommandCallback = null!;

        #endregion

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreOverrideVidExt(ref VidExtFuncList funcs);

        public static _CoreOverrideVidExt CoreOverrideVidExt = null!;

        // public static ErrorCode CoreAddCheat(string, M64F.CheatCode*, int);

        //public static ErrorCode CoreCheatEnabled(string, int);

        /// <summary>
        /// Relocate mupen save files
        /// </summary>
        /// <param name="savePath">The new location for mupen save files</param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate ErrorCode _CoreSaveOverride(IntPtr savePath);

        public static _CoreSaveOverride CoreSaveOverride = null!;
    }
}