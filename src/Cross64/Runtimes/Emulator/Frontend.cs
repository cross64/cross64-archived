﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Cross64.Runtimes
{
    public static partial class Emulator
    {
        private const float CONFIG_VERSION = 1f;
        private static bool MupenLogging = true;
        private static bool VerboseLogging = false;

        internal static void LogFrontend(string input)
        {
            Console.WriteLine("M64P [Frontend]: " + input);
        }

        internal static void LogMupen(IntPtr context, int level, string message)
        {
            if (!MupenLogging) return;

            var lib = Marshal.PtrToStringAnsi(context);
            switch (level)
            {
                case (int) M64P.MsgType.Error:
                    Console.WriteLine($"{lib} Error: {message}");
                    break;
                case (int) M64P.MsgType.Warning:
                    Console.WriteLine($"{lib} Warning: {message}");
                    break;
                case (int) M64P.MsgType.Info:
                    Console.WriteLine($"{lib} Info: {message}");
                    break;
                case (int) M64P.MsgType.Status:
                    Console.WriteLine($"{lib} Status: {message}");
                    break;
                case (int) M64P.MsgType.Verbos:
                    if (VerboseLogging) Console.WriteLine($"{lib}: {message}");
                    break;
                default:
                    Console.WriteLine($"{lib} Unknown: {message}");
                    break;
            }
        }

        #region Mupen Interface

        internal static void SaveDirectory(string input)
        {
            var path = $"{Environment.CurrentDirectory}/saves/{input}/";
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            M64P.CoreSaveOverride(Marshal.StringToHGlobalAnsi(path));
        }

        internal static int EmulatorState()
        {
            var value = 0;

            if (M64P.CoreDoCommandInt(M64P.Command.CoreStateQuery, (int) M64P.CoreParam.EmuState, ref value) !=
                M64P.ErrorCode.Success) return -1;

            return value;
        }

        internal static void ResetMupen()
        {
            M64P.CoreDoCommand(M64P.Command.RomClose, 0, IntPtr.Zero);
            M64P.PluginLoader.UnloadPlugins();
        }

        internal static void InitMupen()
        {
            var cd = Environment.CurrentDirectory;
            var emuDir = cd + "/emulator/";
            var context = Marshal.StringToHGlobalAnsi("m64p-core");
            var version = 0x020001;
            M64P.ErrorCode rval;

            var testPtr = IntPtr.Zero;

            rval = M64P.CoreStartup(version, cd, emuDir, context, M64P.DebugCallback, IntPtr.Zero, null!);

            if (rval != M64P.ErrorCode.Success)
                throw new Exception($@"Couldn't initialize Mupen64Plus-Core library. [{rval.ToString()}]");

            rval = M64P.ConfigOpenSection("Core", out testPtr);
            if (rval != M64P.ErrorCode.Success)
                throw new Exception($@"Couldn't open 'Core' configuration section. [{rval.ToString()}]");

            rval = M64P.ConfigOpenSection("Video-General", out testPtr);
            if (rval != M64P.ErrorCode.Success)
                throw new Exception($@"Couldn't open 'Video-General' configuration section. [{rval.ToString()}]");

            // Cross64 configuration
            rval = M64P.ConfigOpenSection("Cross64", out testPtr);
            if (rval != M64P.ErrorCode.Success)
                throw new Exception($@"Couldn't open 'Cross64' configuration section. [{rval.ToString()}]");

            M64P.ConfigSetDefaultFloat(testPtr, "Version", CONFIG_VERSION,
                "Cross64 config parameter version number.  Please don't change this version number.");
            M64P.ConfigSetDefaultInt(testPtr, "Mupen-Logging", 0,
                "Set to 1 for Mupen output in the console. 0 for silent mode");
            M64P.ConfigSetDefaultInt(testPtr, "Verbose-Logging", 0,
                "Set to 1 for verbose-mupen output in the console. 0 for silent mode");
            M64P.ConfigSetDefaultInt(testPtr, "Video-Plugin", 0,
                "[0] GLideN64, [1] Angrylion-Plus");
            M64P.ConfigSetDefaultInt(testPtr, "Force-Rsp-CXD4", 0,
                "Set to 1 to force rsp cxd4 with every graphics plugin option");

            // Save all changes
            M64P.ConfigSaveFile();

            // Make sure loaded settings are set
            MupenLogging = M64P.ConfigGetParamInt(testPtr, "Mupen-Logging") != 0;
            VerboseLogging = M64P.ConfigGetParamInt(testPtr, "Verbose-Logging") != 0;
        }

        internal static int LoadGame(string input)
        {
            var file = $"{Environment.CurrentDirectory}/roms/{input}";
            var exts = new[] {".z64", ".n64", ".v64"};
            var found = false;

            foreach (var ext in exts)
            {
                var tmpFile = file;
                if (!file.EndsWith(ext)) tmpFile += ext;
                if (!File.Exists(tmpFile)) continue;
                file = tmpFile;
                found = true;
                break;
            }

            if (!found)
            {
                LogFrontend($"Rom file [{input}] not found!");
                return 0;
            }

            var romData = File.ReadAllBytes(file);
            if (M64P.CoreDoCommandBytes(M64P.Command.RomOpen, romData.Length, romData) != M64P.ErrorCode.Success)
            {
                LogFrontend("Failed to load rom in Mupen64Plus-Core!");
                return 0;
            }

            return romData.Length;
        }

        internal static void RunGame()
        {
            M64P.ConfigOpenSection("Cross64", out var testPtr);
            var videoPlugin = M64P.ConfigGetParamInt(testPtr, "Video-Plugin");
            var forceCxd4 = M64P.ConfigGetParamInt(testPtr, "Force-Rsp-CXD4") == 1;

            // Ensure plugin is a valid value
            if (videoPlugin < 0 || videoPlugin > 3)
            {
                videoPlugin = 0;
                M64P.ConfigSetParamInt(testPtr, "Video-Plugin", 0);
                M64P.ConfigSaveSection("Cross64");
            }

            // Pre Boot Emulator
            var path = $"{Environment.CurrentDirectory}/Memory_Logger.txt";
            memLogger = new StreamWriter(path, true);

            // Boot Emulator
            {
                // Attach Plugins
                M64P.PluginLoader.LoadPlugins(videoPlugin, forceCxd4);

                // Attach Callbacks
                M64P.CoreDoCommandCallback(M64P.Command.SetFrameCallback, 0, M64P.FrameCallback);

                // Override Video Extensions
                M64P.CoreOverrideVidExt(ref M64P.VidExtFuncs);

                // Run Emulator
                M64P.CoreDoCommand(M64P.Command.Execute, 0, IntPtr.Zero);
            }

            // Close Emulator
            memLogger.Close();
            memLogger.Dispose();
        }

        internal static void PauseGame()
        {
            M64P.CoreDoCommand(M64P.Command.Pause, 0, IntPtr.Zero);
        }

        internal static void ResumeGame()
        {
            M64P.CoreDoCommand(M64P.Command.Resume, 0, IntPtr.Zero);
        }

        internal static void StopGame()
        {
            M64P.CoreDoCommand(M64P.Command.Stop, 0, IntPtr.Zero);
        }

        internal static void SoftReset()
        {
            M64P.CoreDoCommand(M64P.Command.Reset, 0, IntPtr.Zero);
        }

        internal static void HardReset()
        {
            M64P.CoreDoCommand(M64P.Command.Reset, 1, IntPtr.Zero);
        }

        internal static void SaveState(string input)
        {
            M64P.CoreDoCommand(M64P.Command.StateSave, 2, Marshal.StringToHGlobalAnsi(input));
        }

        internal static void LoadState(string input)
        {
            M64P.CoreDoCommand(M64P.Command.StateLoad, 0, Marshal.StringToHGlobalAnsi(input));
        }

        #endregion

        #region General N64

        public enum ButtonType : ushort
        {
            CRight = 0x0001,
            CLeft = 0x0002,
            CDown = 0x0004,
            CUp = 0x0008,
            R = 0x0010,
            L = 0x0020,
            DRight = 0x0100,
            DLeft = 0x0200,
            DDown = 0x0400,
            DUp = 0x0800,
            Start = 0x1000,
            Z = 0x2000,
            B = 0x4000,
            A = 0x8000,
        }

        #endregion
    }
}