﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Cross64.Runtimes
{
    public static partial class Emulator
    {
        private static StreamWriter memLogger = null!;
        public static bool MemoryLogging;

        private static void MemLog(string info)
        {
            memLogger.Write(info + ": " + DateTime.Now);
        }

        public static class Cartridge
        {
            private static string ReadS(uint addr, int length)
            {
                var ret = length < 1 ? new byte[0] : new byte[length];
                var buffer = M64P.ReadRomBuffer(addr, length);
                Marshal.Copy(buffer, ret, 0, length);
                return Encoding.ASCII.GetString(ret).Trim();
            }

            /// <summary>
            /// Return the Game Name
            /// </summary>
            public static string Name => ReadS(0x20, 0x14);

            /// <summary>
            /// Return the serial-number/game-id
            /// </summary>
            public static ushort Serial => M64P.ReadRom16(0x3c);

            /// <summary>
            /// Return the Country/Region Code
            /// </summary>
            public static string Country => ReadS(0x3e, 1);

            /// <summary>
            /// Return the cart regions re-release number
            /// </summary>
            public static uint Revision => M64P.ReadRom8(0x3f);

            /// <summary>
            /// Return the cpu clock rate specified in the rom
            /// </summary>
            public static uint ClockRate => M64P.ReadRom32(0x04);

            /// <summary>
            /// Return the program counter specified in the rom
            /// </summary>
            public static uint ProgramCounter => M64P.ReadRom32(0x08);
        }

        public static class RdRam
        {
            private static void SafetyCheck(uint address, int size)
            {
                if (((address + size) & 0xffffff) >= Length)
                    throw new Exception($"Address[{address}] or desired read/write Size[{size}] is out of bounds!");
            }

            private static void Log(string info, bool forceLog)
            {
                if (!MemoryLogging && !forceLog) return;
                MemLog("RdRam." + info);
            }

            public static IntPtr Raw => M64P.GetRdRam();

            public static int Length => M64P.GetRdRamSize();

            #region Read

            /// <summary> Reads a String </summary>
            public static string ReadS(uint address, int length = 0)
            {
                SafetyCheck(address, 1);

                var offset = new IntPtr(Raw.ToInt64() + address);
                var ret = Marshal.PtrToStringAnsi(offset);

                if (ret == null) return "";
                if (length < 1) return ret;

                if (length > ret.Length) length = ret.Length;
                ret = ret.Substring(0, length);

                return ret;
            }

            /// <summary> Reads a Byte Array </summary>
            public static byte[] ReadB(uint address, int length)
            {
                SafetyCheck(address, length);

                var ret = length < 1 ? new byte[0] : new byte[length];
                var buffer = M64P.ReadRdRamBuffer(address, length);
                Marshal.Copy(buffer, ret, 0, length);
                return ret;
            }

            /// <summary> Reads a Double Floating Point </summary>
            public static double ReadD(uint address)
            {
                return BitConverter.Int64BitsToDouble((long) Read64(address));
            }

            /// <summary> Reads a Single Floating Point </summary>
            public static float ReadF(uint address)
            {
                return BitConverter.Int32BitsToSingle((int) Read32(address));
            }

            /// <summary> Reads an Unsigned Long </summary>
            public static ulong Read64(uint address)
            {
                SafetyCheck(address, 7);
                return M64P.ReadRdRam64(address);
            }

            /// <summary> Reads an Unsigned Integer </summary>
            public static uint Read32(uint address)
            {
                SafetyCheck(address, 3);
                return M64P.ReadRdRam32(address);
            }

            /// <summary> Reads an Unsigned Short </summary>
            public static ushort Read16(uint address)
            {
                SafetyCheck(address, 1);
                return M64P.ReadRdRam16(address);
            }

            /// <summary> Reads a Byte </summary>
            public static byte Read8(uint address)
            {
                SafetyCheck(address, 0);
                return M64P.ReadRdRam8(address);
            }

            /// <summary> Reads a String from an Address-Pointed offset </summary>
            public static string ReadPS(uint address, int offset, int length = 0)
            {
                return ReadS(Read32(address) + (uint) offset, length);
            }

            /// <summary> Reads a Byte Array from an Address-Pointed offset </summary>
            public static byte[] ReadPB(uint address, int offset, int length)
            {
                return ReadB(Read32(address) + (uint) offset, length);
            }

            /// <summary> Reads a Double Floating Point from an Address-Pointed offset </summary>
            public static double ReadPD(uint address, int offset)
            {
                return ReadD(Read32(address) + (uint) offset);
            }

            /// <summary> Reads aSingle Floating Point from an Address-Pointed offset </summary>
            public static float ReadPF(uint address, int offset)
            {
                return ReadF(Read32(address) + (uint) offset);
            }

            /// <summary> Reads an Unsigned Long from an Address-Pointed offset </summary>
            public static ulong ReadP64(uint address, int offset)
            {
                return Read64(Read32(address) + (uint) offset);
            }

            /// <summary> Reads an Unsigned Integer from an Address-Pointed offset </summary>
            public static uint ReadP32(uint address, int offset)
            {
                return Read32(Read32(address) + (uint) offset);
            }

            /// <summary> Reads an Unsigned Short from an Address-Pointed offset </summary>
            public static ushort ReadP16(uint address, int offset)
            {
                return Read16(Read32(address) + (uint) offset);
            }

            /// <summary> Reads a Byte from an Address-Pointed offset </summary>
            public static byte ReadP8(uint address, int offset)
            {
                return Read8(Read32(address) + (uint) offset);
            }

            #endregion

            #region Write

            /// <summary> Writes a String </summary>
            public static void WriteS(uint address, string value, int offset = 0, int length = 0, bool forceLog = false)
            {
                WriteB(address, Encoding.UTF8.GetBytes(value), offset, length, forceLog);
            }

            /// <summary> Writes a Byte Array </summary>
            public static void WriteB(uint address, byte[] value, int offset = 0, int length = 0, bool forceLog = false)
            {
                if (length < 1)
                    length = value.Length;

                if (offset + length > value.Length)
                    length = value.Length - offset;

                // There was an error if this returns
                if (length < 1) return;

                byte[] buffer;
                if (length != value.Length)
                {
                    buffer = new byte[length];
                    Buffer.BlockCopy(value, offset, buffer, 0, length);
                }
                else buffer = value;

                Log($"WriteBuffer[{address}, {length}]", forceLog);
                SafetyCheck(address, length);
                M64P.WriteRdRamBuffer(address, buffer, length);
            }

            /// <summary> Writes a Double Floating Point </summary>
            public static void WriteD(uint address, double value, bool forceLog = false)
            {
                var hexVal = (ulong) BitConverter.DoubleToInt64Bits(value);
                Log($"WriteD[{address}, {value}/{hexVal:X16}]", forceLog);
                // TODO: test +/- for ulong cast
                write64(address, hexVal);
            }

            /// <summary> Writes a Single Floating Point </summary>
            public static void WriteF(uint address, float value, bool forceLog = false)
            {
                var hexVal = (uint) BitConverter.SingleToInt32Bits(value);
                Log($"WriteF[{address}, {value}/{hexVal:X8}]", forceLog);
                // TODO: test +/- for uint cast
                write32(address, hexVal);
            }

            /// <summary> Writes an Unsigned Long </summary>
            public static void Write64(uint address, ulong value, bool forceLog = false)
            {
                Log($"Write64[{address}, {value}/{value:X16}]", forceLog);
                write64(address, value);
            }

            private static void write64(uint address, ulong value)
            {
                SafetyCheck(address, 7);
                M64P.WriteRdRam64(address, value);
            }

            /// <summary> Writes an Unsigned Integer </summary>
            public static void Write32(uint address, uint value, bool forceLog = false)
            {
                Log($"Write32[{address}, {value}/{value:X8}]", forceLog);
                write32(address, value);
            }

            private static void write32(uint address, uint value)
            {
                SafetyCheck(address, 3);
                M64P.WriteRdRam32(address, value);
            }

            /// <summary> Writes an Unsigned Short </summary>
            public static void Write16(uint address, ushort value, bool forceLog = false)
            {
                Log($"Write16[{address}, {value}/{value:X4}]", forceLog);
                SafetyCheck(address, 1);
                M64P.WriteRdRam16(address, value);
            }

            /// <summary> Writes a Byte </summary>
            public static void Write8(uint address, byte value, bool forceLog = false)
            {
                Log($"Write8[{address}, {value}/{value:X}]", forceLog);
                SafetyCheck(address, 0);
                M64P.WriteRdRam8(address, value);
            }

            /// <summary> Writes a String to an Address-Pointed offset </summary>
            public static void WritePS(uint address, int offset, string value, int sOffset = 0, int length = 0,
                bool forceLog = false)
            {
                WriteS(Read32(address) + (uint) offset, value, sOffset, length, forceLog);
            }

            /// <summary> Writes a Byte Array to an Address-Pointed offset </summary>
            public static void WritePB(uint address, int offset, byte[] value, int bOffset = 0, int length = 0,
                bool forceLog = false)
            {
                WriteB(Read32(address) + (uint) offset, value, bOffset, length, forceLog);
            }

            /// <summary> Writes a Double Floating Point to an Address-Pointed offset </summary>
            public static void WritePD(uint address, int offset, double value, bool forceLog = false)
            {
                WriteD(Read32(address) + (uint) offset, value, forceLog);
            }

            /// <summary> Writes a Single Floating Point to an Address-Pointed offset </summary>
            public static void WritePF(uint address, int offset, float value, bool forceLog = false)
            {
                WriteF(Read32(address) + (uint) offset, value, forceLog);
            }

            /// <summary> Writes an Unsigned Long to an Address-Pointed offset </summary>
            public static void WriteP64(uint address, int offset, ulong value, bool forceLog = false)
            {
                Write64(Read32(address) + (uint) offset, value, forceLog);
            }

            /// <summary> Writes a Unsigned Integer to an Address-Pointed offset </summary>
            public static void WriteP32(uint address, int offset, uint value, bool forceLog = false)
            {
                Write32(Read32(address) + (uint) offset, value, forceLog);
            }

            /// <summary> Writes an Unsigned Short to an Address-Pointed offset </summary>
            public static void WriteP16(uint address, int offset, ushort value, bool forceLog = false)
            {
                Write16(Read32(address) + (uint) offset, value, forceLog);
            }

            /// <summary> Writes a Byte to an Address-Pointed offset </summary>
            public static void WriteP8(uint address, int offset, byte value, bool forceLog = false)
            {
                Write8(Read32(address) + (uint) offset, value, forceLog);
            }

            #endregion
        }

        public static class Rom
        {
            private static void SafetyCheck(uint address, int size)
            {
                if (address + size >= Length)
                    throw new Exception($"Address[{address}] or desired read/write Size[{size}] is out of bounds!");
            }

            private static void Log(string info, bool forceLog)
            {
                if (!MemoryLogging && !forceLog) return;
                MemLog("Rom." + info);
            }

            public static IntPtr Raw => M64P.GetRom();

            public static int Length => M64P.GetRomSize();

            #region Read

            public static string ReadS(uint address, int length, bool trim)
            {
                SafetyCheck(address, length);

                var ret = length < 1 ? new byte[0] : new byte[length];
                var buffer = M64P.ReadRomBuffer(address, length);
                Marshal.Copy(buffer, ret, 0, length);

                return !trim ? Encoding.ASCII.GetString(ret) : Encoding.ASCII.GetString(ret).Trim();
            }

            public static byte[] ReadB(uint address, int length)
            {
                SafetyCheck(address, length);

                var ret = length < 1 ? new byte[0] : new byte[length];
                var buffer = M64P.ReadRomBuffer(address, length);
                Marshal.Copy(buffer, ret, 0, length);
                return ret;
            }

            public static double ReadD(uint address)
            {
                return BitConverter.Int64BitsToDouble((long) Read64(address));
            }

            public static float ReadF(uint address)
            {
                return BitConverter.Int32BitsToSingle((int) Read32(address));
            }

            public static ulong Read64(uint address)
            {
                SafetyCheck(address, 7);
                return M64P.ReadRom64(address);
            }

            public static uint Read32(uint address)
            {
                SafetyCheck(address, 3);
                return M64P.ReadRom32(address);
            }

            public static ushort Read16(uint address)
            {
                SafetyCheck(address, 1);
                return M64P.ReadRom16(address);
            }

            public static byte Read8(uint address)
            {
                SafetyCheck(address, 0);
                return M64P.ReadRom8(address);
            }

            #endregion

            #region Write

            public static void WriteS(uint address, string value, int offset = 0, int length = 0,
                bool forceLog = false)
            {
                WriteB(address, Encoding.UTF8.GetBytes(value), offset, length, forceLog);
            }

            public static void WriteB(uint address, byte[] value, int offset = 0, int length = 0,
                bool forceLog = false)
            {
                if (length < 1)
                    length = value.Length;

                if (offset + length > value.Length)
                    length = value.Length - offset;

                // There was an error if this returns
                if (length < 1) return;

                byte[] buffer;
                if (length != value.Length)
                {
                    buffer = new byte[length];
                    Buffer.BlockCopy(value, offset, buffer, 0, length);
                }
                else buffer = value;

                Log($"WriteBuffer[{address}, {length}]", forceLog);
                SafetyCheck(address, length);
                M64P.WriteRomBuffer(address, buffer, length);
            }

            public static void WriteD(uint address, double value, bool forceLog = false)
            {
                var hexVal = (ulong) BitConverter.DoubleToInt64Bits(value);
                Log($"WriteD[{address}, {value}/{hexVal:X16}]", forceLog);
                // TODO: test +/- for ulong cast
                write64(address, hexVal);
            }

            public static void WriteF(uint address, float value, bool forceLog = false)
            {
                var hexVal = (uint) BitConverter.SingleToInt32Bits(value);
                Log($"WriteF[{address}, {value}/{hexVal:X8}]", forceLog);
                // TODO: test +/- for uint cast
                write32(address, hexVal);
            }

            public static void Write64(uint address, ulong value, bool forceLog = false)
            {
                Log($"Write64[{address}, {value}/{value:X16}]", forceLog);
                write64(address, value);
            }

            private static void write64(uint address, ulong value)
            {
                SafetyCheck(address, 7);
                M64P.WriteRom64(address, value);
            }

            public static void Write32(uint address, uint value, bool forceLog = false)
            {
                Log($"Write32[{address}, {value}/{value:X8}]", forceLog);
                write32(address, value);
            }

            private static void write32(uint address, uint value)
            {
                SafetyCheck(address, 3);
                M64P.WriteRom32(address, value);
            }

            public static void Write16(uint address, ushort value, bool forceLog = false)
            {
                Log($"Write16[{address}, {value}/{value:X4}]", forceLog);
                SafetyCheck(address, 1);
                M64P.WriteRom16(address, value);
            }

            public static void Write8(uint address, byte value, bool forceLog = false)
            {
                Log($"Write8[{address}, {value}/{value:X}]", forceLog);
                SafetyCheck(address, 0);
                M64P.WriteRom8(address, value);
            }

            #endregion
        }

        /// <summary>
        /// A 'uint' that uses RdRam functions.
        /// </summary>
        public struct Pointer
        {
            private uint _real;

            #region Operator

            private Pointer(uint val)
            {
                _real = val;
            }

            public override string ToString()
            {
                return _real.ToString();
            }

            public string ToString(string format)
            {
                return _real.ToString(format);
            }

            public static bool operator <(Pointer a, Pointer b)
            {
                return a._real < b._real;
            }

            public static bool operator >(Pointer a, Pointer b)
            {
                return a._real > b._real;
            }

            public static bool operator <=(Pointer a, Pointer b)
            {
                return a._real <= b._real;
            }

            public static bool operator >=(Pointer a, Pointer b)
            {
                return a._real >= b._real;
            }

            public static bool operator ==(Pointer a, Pointer b)
            {
                return a._real == b._real;
            }

            public static bool operator !=(Pointer a, Pointer b)
            {
                return a._real != b._real;
            }

            public static Pointer operator +(Pointer a, Pointer b)
            {
                return (dynamic) a._real + b._real;
            }

            public static Pointer operator -(Pointer a, Pointer b)
            {
                return (dynamic) a._real - b._real;
            }

            public override bool Equals(object obj)
            {
                return _real.Equals(obj);
            }

            public override int GetHashCode()
            {
                return _real.GetHashCode();
            }

            public static implicit operator Pointer(int value) => new Pointer((uint) value);
            public static implicit operator Pointer(uint value) => new Pointer(value);
            public static implicit operator uint(Pointer me) => me._real;

            #endregion

            #region Functions

            /// <summary>
            /// Sets to the address listed at the current pointer location.
            /// </summary>
            public void Dereference()
            {
                _real = U32;
            }

            /// <summary>
            /// Sets a new pointer to the address listed at the current pointer location.
            /// </summary>
            public Pointer Deref => U32;

            public string ReadS(int length, int offset = 0) => RdRam.ReadS(_real + (uint) offset, length);

            public void WriteS(string value, int offset = 0, int valOffset = 0, int length = 0, bool forceLog = false)
            {
                RdRam.WriteS(_real + (uint) offset, value, valOffset, length, forceLog);
            }

            public byte[] ReadB(int length, int offset = 0) => RdRam.ReadB(_real + (uint) offset, length);

            public void WriteB(byte[] value, int offset = 0, int valOffset = 0, int length = 0, bool forceLog = false)
            {
                RdRam.WriteB(_real + (uint) offset, value, valOffset, length, forceLog);
            }

            public double ReadD(int offset = 0) => RdRam.ReadD(_real + (uint) offset);

            public void WriteD(double value, int offset = 0, bool forceLog = false)
            {
                RdRam.WriteD(_real + (uint) offset, value, forceLog);
            }

            public float ReadF(int offset = 0) => RdRam.ReadF(_real + (uint) offset);

            public void WriteF(float value, int offset = 0, bool forceLog = false)
            {
                RdRam.WriteF(_real + (uint) offset, value, forceLog);
            }

            public ulong Read64(int offset = 0) => RdRam.Read64(_real + (uint) offset);

            public void Write64(ulong value, int offset = 0, bool forceLog = false)
            {
                RdRam.Write64(_real + (uint) offset, value, forceLog);
            }

            public uint Read32(int offset = 0) => RdRam.Read32(_real + (uint) offset);

            public void Write32(uint value, int offset = 0, bool forceLog = false)
            {
                RdRam.Write32(_real + (uint) offset, value, forceLog);
            }

            public ushort Read16(int offset = 0) => RdRam.Read16(_real + (uint) offset);

            public void Write16(ushort value, int offset = 0, bool forceLog = false)
            {
                RdRam.Write16(_real + (uint) offset, value, forceLog);
            }

            public byte Read8(int offset = 0) => RdRam.Read8(_real + (uint) offset);

            public void Write8(byte value, int offset = 0, bool forceLog = false)
            {
                RdRam.Write8(_real + (uint) offset, value, forceLog);
            }

            public string ReadPS(int offset, int length) => RdRam.ReadPS(_real, offset, length);

            public void WritePS(int offset, string value, int sOffset = 0, int length = 0, bool forceLog = false)
            {
                RdRam.WritePS(_real, offset, value, sOffset, length, forceLog);
            }

            public byte[] ReadPB(int offset, int length) => RdRam.ReadPB(_real, offset, length);

            public void WritePB(int offset, byte[] value, int bOffset = 0, int length = 0, bool forceLog = false)
            {
                RdRam.WritePB(_real, offset, value, bOffset, length, forceLog);
            }

            public double ReadPD(int offset) => RdRam.ReadPD(_real, offset);

            public void WritePD(int offset, double value, bool forceLog = false)
            {
                RdRam.WritePD(_real, offset, value, forceLog);
            }

            public float ReadPF(int offset) => RdRam.ReadPF(_real, offset);

            public void WritePF(int offset, float value, bool forceLog = false)
            {
                RdRam.WritePF(_real, offset, value, forceLog);
            }

            public ulong ReadP64(int offset) => RdRam.ReadP64(_real, offset);

            public void WriteP64(int offset, ulong value, bool forceLog = false)
            {
                RdRam.WriteP64(_real, offset, value, forceLog);
            }

            public uint ReadP32(int offset) => RdRam.ReadP32(_real, offset);

            public void WriteP32(int offset, uint value, bool forceLog = false)
            {
                RdRam.WriteP32(_real, offset, value, forceLog);
            }

            public ushort ReadP16(int offset) => RdRam.ReadP16(_real, offset);

            public void WriteP16(int offset, ushort value, bool forceLog = false)
            {
                RdRam.WriteP16(_real, offset, value, forceLog);
            }

            public byte ReadP8(int offset) => RdRam.ReadP8(_real, offset);

            public void WriteP8(int offset, byte value, bool forceLog = false)
            {
                RdRam.WriteP8(_real, offset, value, forceLog);
            }

            #endregion

            #region Properties

            public int Length => (int) (RdRam.Length - _real);

            public string S
            {
                get => RdRam.ReadS(_real, 0);
                set => RdRam.WriteS(_real, value);
            }

            public double D
            {
                get => RdRam.ReadD(_real);
                set => RdRam.WriteD(_real, value);
            }

            public float F
            {
                get => RdRam.ReadF(_real);
                set => RdRam.WriteF(_real, value);
            }

            public ulong U64
            {
                get => RdRam.Read64(_real);
                set => RdRam.Write64(_real, value);
            }

            public uint U32
            {
                get => RdRam.Read32(_real);
                set => RdRam.Write32(_real, value);
            }

            public ushort U16
            {
                get => RdRam.Read16(_real);
                set => RdRam.Write16(_real, value);
            }

            public byte U8
            {
                get => RdRam.Read8(_real);
                set => RdRam.Write8(_real, value);
            }

            #endregion
        }

        public static void DynarecRefresh()
        {
            M64P.RefreshDynarec();
        }
    }
}