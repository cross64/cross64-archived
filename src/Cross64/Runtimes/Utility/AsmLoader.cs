using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Cross64.Runtimes
{
    public static partial class Utility
    {
        internal static void AutoLoadAssembly<T>(T host, string dir, string preDir = "") where T : class
        {
            AutoLoadAsm(host, dir, preDir);
            AutoLoadGameshark(host, dir, preDir);
        }

        private static void AutoLoadAsm<T>(T host, string dir, string preDir = "") where T : class
        {
            var assembly = host.GetType().GetTypeInfo().Assembly;
            var list = assembly.GetManifestResourceNames().Where(
                s => s.EndsWith(".bin") &&
                     s.StartsWith($@"{assembly.GetName().Name}.{preDir}Resources.Auto.{dir}.")
            );

            foreach (var f in list)
            {
                var parts = f.Split('.');
                var fName = parts[parts.Length - 2];
                var addr = uint.Parse(fName.Substring(fName.Length - 6));
                using var stream = assembly.GetManifestResourceStream(f);
                using var reader = new MemoryStream();
                stream!.CopyTo(reader);
                LoadAsm(addr, reader.ToArray());
            }
        }

        private static void AutoLoadGameshark<T>(T host, string dir, string preDir = "") where T : class
        {
            var assembly = host.GetType().GetTypeInfo().Assembly;
            var list = assembly.GetManifestResourceNames().Where(
                s => s.EndsWith(".gs") &&
                     s.StartsWith($@"{assembly.GetName().Name}.{preDir}Resources.Auto.{dir}.")
            );

            foreach (var f in list)
            {
                using var stream = assembly.GetManifestResourceStream(f);
                using var reader = new StreamReader(stream!);
                LoadGameshark(reader.ReadToEnd());
            }
        }

        /// <summary>
        /// Load an Assembly file from embedded resource. Assembly file must be in the following directory:
        /// CustomPlugin/Resources/Asm/File
        ///
        /// Param:host requires passing 'this'
        /// Param:path should be any sub folders of the 'Asm' and the file ex: "E0/Puppet"
        /// Note: Separation character can be '\', '/', or '.'
        ///       Assembly file must end in '.bin' extension
        /// </summary>
        public static byte[] GetAsm<T>(T host, string path) where T : class
        {
            var assembly = host.GetType().GetTypeInfo().Assembly;
            var name = $@"{assembly.GetName().Name}.Resources.Asm.{path}.gs"
                .Replace('/', '.').Replace('\\', '.');

            using var stream = assembly.GetManifestResourceStream(name);
            if (stream == null) throw new Exception($@"Requested file [{name}] not found.");
            using var reader = new MemoryStream();
            stream.CopyTo(reader);
            return reader.ToArray();
        }

        public static void LoadAsm(uint addr, byte[] binary, bool suppressRefresh = true)
        {
            Emulator.RdRam.WriteB(addr, binary);
            if (!suppressRefresh) Emulator.DynarecRefresh();
        }

        /// <summary>
        /// Load a gameshark file from embedded resource. Gameshark file must be in the following directory:
        /// CustomPlugin/Resources/Asm/File
        ///
        /// Param:host requires passing 'this'
        /// Param:path should be any sub folders of the 'Asm' and the file ex: "E0/Puppet"
        /// Note: Separation character can be '\', '/', or '.'
        ///       Gameshark file must end in '.gs' extension
        /// </summary>
        public static string GetGameshark<T>(T host, string path) where T : class
        {
            var assembly = host.GetType().GetTypeInfo().Assembly;
            var name = $@"{assembly.GetName().Name}.Resources.Asm.{path}.gs"
                .Replace('/', '.').Replace('\\', '.');

            using var stream = assembly.GetManifestResourceStream(name);
            if (stream == null) throw new Exception($@"Requested file [{name}] not found.");
            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public static void LoadGameshark(string gameshark, bool suppressRefresh = true)
        {
            var lines = gameshark.Split(Environment.NewLine);
            foreach (var line in lines)
            {
                var ln = line.Trim();
                if (ln.Length < 13) continue;

                var cmd = ln.Substring(1, 1) == "1";
                var addr = Convert.ToUInt32($"0x{ln.Substring(2, 6)}", 16);
                var val = ln.Split(" ");

                if (!cmd) Emulator.RdRam.Write8(addr, Convert.ToByte($"0x{val[1]}", 16));
                else Emulator.RdRam.Write16(addr, (ushort) Convert.ToInt16($"0x{val[1]}", 16));
            }

            if (!suppressRefresh) Emulator.DynarecRefresh();
        }
    }
}