﻿namespace Cross64.Runtimes
{
    public static partial class Utility
    {
        public static int BitCount(byte[] arr, int offset = 0, int length = 0)
        {
            if (length <= 0 || offset + length > arr.Length)
                length = arr.Length - offset;

            var count = 0;
            for (var i = offset; i < offset + length; i++)
                count += BitCount(arr[i]);

            return count;
        }

        public static int BitCount(int value)
        {
            var count = 0;
            for (var i = 0; i < 32; i++)
                count += (value >> i) & 1;
            return count;
        }

        public static int BitCount(short value)
        {
            var count = 0;
            for (var i = 0; i < 16; i++)
                count += (value >> i) & 1;
            return count;
        }

        public static int BitCount(byte value)
        {
            var count = 0;
            for (var i = 0; i < 8; i++)
                count += (value >> i) & 1;
            return count;
        }
    }
}