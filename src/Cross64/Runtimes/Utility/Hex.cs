using System.Linq;
using System.Text;

namespace Cross64.Runtimes
{
    public static partial class Utility
    {
        public static string Hex(string value)
        {
            return Hex(Encoding.Default.GetBytes(value));
        }

        public static string Hex(byte[] value)
        {
            return value.Aggregate("", (current, t) => current + t.ToString("X2"));
        }

        public static string Hex(double value)
        {
            return value.ToString("X16");
        }

        public static string Hex(ulong value)
        {
            return value.ToString("X16");
        }

        public static string Hex(long value)
        {
            return value.ToString("X16");
        }

        public static string Hex(float value)
        {
            return value.ToString("X8");
        }

        public static string Hex(uint value)
        {
            return value.ToString("X8");
        }

        public static string Hex(int value)
        {
            return value.ToString("X8");
        }

        public static string Hex(ushort value)
        {
            return value.ToString("X4");
        }

        public static string Hex(short value)
        {
            return value.ToString("X4");
        }

        public static string Hex(byte value)
        {
            return value.ToString("X2");
        }
    }
}