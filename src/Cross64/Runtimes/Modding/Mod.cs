﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Asfw;
using Asfw.IO;

namespace Cross64.Runtimes.Modding
{
    public abstract class PluginBase : IDisposable
    {
        #region Network Info

        /// <summary>
        /// Modes to determine networked lobby structure.
        /// </summary>
        public enum NetworkType
        {
            /// <summary> Not using a network </summary>
            None,

            /// <summary> Stores players by the order they connect. Lobby host is Player 1 </summary>
            Indexed,

            /// <summary> Stores players as they come or leave. </summary>
            Decentralized
        }

        /// <summary>
        /// The network mode the server creates a lobby as.
        /// Only necessary to set if using a networked mod
        /// Override this property with get only
        /// [Eg (c#) public override int NetworkMode => NetworkType.Decentralized;]
        /// </summary>
        public virtual NetworkType NetworkMode => NetworkType.None;

        /// <summary>
        /// The 'main' server this plugin will be hosted from.
        /// Does not restrict players to only using the master server.
        /// Only necessary to set if using a networked mod
        /// Override this property with get only
        /// [Eg (c#) public override int MasterServerIp => "127.0.0.1";]
        /// </summary>
        public virtual string MasterServerIp => "127.0.0.1";

        /// <summary>
        /// The 'main' server this plugin will be hosted from.
        /// Does not restrict players to only using the master server.
        /// Only necessary to set if using a networked mod.
        /// Override this property with get only
        /// [Eg (c#) public override int MasterServerPort => 8079;]
        /// </summary>
        public virtual int MasterServerPort => 8079;

        #endregion

        /// <summary>
        /// The game(s?) (by id) your mod will target. Selecting GameID.Mupen64Plus
        /// will run no matter what game is playing, and selecting a specific
        /// game will only run on that game. Override this property with get only
        /// (May contain multiple values if the game is like a cross-over multiworld)
        /// [Eg (c#) public override GameID Game => new [] {GameID.Mupen64Plus};]
        /// </summary>
        public virtual GameID[] Game => new[] {GameID.Mupen64Plus};

        /// <summary>
        /// The name your mod will have publicly in the catalog and
        /// optional config file. Override this property with get
        /// only [Eg (c#) public override string Name => "My Mod";]
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// The version your mod will have publicly in the catalog.
        /// </summary>
        public string Version { get; internal set; } = "0.0.0.0";

        /// <summary>
        /// The description your mod will have publicly in the catalog.
        /// Override this property with get only
        /// [Eg (c#) public override string Name => "My Mod";]
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// Optional tag for the icon to be displayed on Discord "Playing".
        /// </summary>
        public virtual string DiscordImage { get; } = "";

        /// <summary>
        /// Return an instance of your mod class here.
        /// Do not auto-initialize
        /// in the constructor! Mod 'Initialize()' is called by cross
        /// and the active Api is passed to it
        /// </summary>
        public abstract ModBase InitMod { get; }

        /// <summary>
        /// Required function by IDisposable.
        /// Do not invoke!
        /// </summary>
        public void Dispose()
        {
            if (Instance.Count < 1) return;

            Instance.FirstOrDefault().Value.SaveConfig();

            foreach (var inst in Instance.Values)
                inst.Dispose();

            Instance.Clear();
        }

        /// <summary>
        /// Used internally only to manage the plugins!
        /// </summary>
        internal Dictionary<string, ModBase> Instance = new Dictionary<string, ModBase>();
    }

    public abstract class ModBase : IDisposable
    {
        #region Parent

        /// <summary>
        /// This value is set to the parent plugin just after the
        /// parent plugin "Initialize()" function is called
        /// </summary>
        public PluginBase PluginInfo { get; private set; }

        internal string Lobby { get; private set; }
        internal string Password { get; private set; }

        internal void SetIdentity(PluginBase parent, string lobby, string password)
        {
            PluginInfo = parent;
            Lobby = lobby;
            Password = password;
            LoadConfig();
        }

        #endregion

        #region Config

        /// <summary>
        /// To use: Create a class that uses 'properties' for any variables
        /// you want savable to a file [Eg (c#) int a { get; set; } = 24;]
        /// This is loaded just before Initialize() is called.
        /// Then override this property with a pointer to your config
        /// object instance [Eg (c#) protected override Config => _config;]
        /// </summary>
        protected virtual object Config { get; private set; }

        internal void LoadConfig()
        {
            if (Config == null) return;

            var path = $"{Environment.CurrentDirectory}/plugin_settings/{PluginInfo?.Name}.json";

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
                Serialization.SaveJson(path, Config);
            }

            Config = Serialization.LoadJson<object>(path);
            Serialization.SaveJson(path, Config);
        }

        internal void SaveConfig()
        {
            if (Config == null) return;

            var path = $"{Environment.CurrentDirectory}/plugin_settings/{PluginInfo?.Name}.json";
            Serialization.SaveJson(path, Config);
        }

        #endregion

        /// <summary>
        /// It is recommended to save a reference to the
        /// api as the type you need here
        /// [Eg (c#) Api = (GameName) api;]
        /// </summary>
        /// <param name="api"></param>
        public abstract void Initialize(ApiBase api);

        /// <summary>
        /// This code is only activated on the first frame (0)
        /// </summary>
        public virtual void OnFirstFrame()
        {
        }

        /// <summary>
        /// This function is called every frame. The frame variable
        /// will return the current frame (also known as the total
        /// count of frames that have occured since the game has
        /// started running
        /// </summary>
        public abstract void OnTick(uint frame);

        /// <summary>
        /// Optionally dispose any objects that need manual cleanup
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Required function by IDisposable [invokes Dispose(true)]
        /// Do not invoke!
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }
    }

    public abstract class NetworkedModBase : ModBase
    {
        public int MyIndex { get; internal set; } = -1;
        public bool IsClient { get; internal set; } = false;
        public bool IsServer { get; internal set; } = false;

        public readonly Dictionary<int, int> PlayerId = new Dictionary<int, int>();
        public readonly Dictionary<int, string> Players = new Dictionary<int, string>();
        private readonly List<int> UnusedPlayer = new List<int>();

        internal int AddPlayer(string displayName, int id)
        {
            // Get a new index
            var index = 0;
            if (UnusedPlayer.Count > 0)
            {
                index = UnusedPlayer[0];
                UnusedPlayer.RemoveAt(0);
            }
            else index = Players.Count + 1;

            // Update Trackers
            PlayerId.Add(index, id);
            Players.Add(index, displayName);

            if (IsServer)
            {
                // If we host and play
                if (IsClient && index != MyIndex)
                    Client_PlayerJoined(index);

                Server_PlayerJoined(index);
            }

            return index;
        }

        internal void InsertPlayer(string displayName, int playerIndex, bool notif = true)
        {
            if (IsServer) return;

            if (Players.ContainsKey(playerIndex))
            {
                PlayerId.Remove(playerIndex);
                Players.Remove(playerIndex);
            }

            // Update Trackers
            PlayerId.Add(playerIndex, -1);
            Players.Add(playerIndex, displayName);

            // Tell the plugin someone joined
            if (notif && playerIndex != MyIndex)
                Client_PlayerJoined(playerIndex);
        }

        internal void RemovePlayer(int index)
        {
            // We don't have this player/index
            if (!Players.ContainsKey(index)) return;

            if (IsClient && index != MyIndex) Client_PlayerLeft(index);
            if (IsServer) Server_PlayerLeft(index);

            // Update trackers
            PlayerId.Remove(index);
            Players.Remove(index);
            UnusedPlayer.Add(index);
        }

        /// <summary>
        /// This function is invoked every time a packet is received by the lobby.
        /// The sending players index is 'senderIndex'. If senderIndex is -1 then it was sent by the server!
        /// </summary>
        public abstract void OnClientReceivePacket(CrossPacket packet, string packetName, int senderIndex);

        /// <summary>
        /// This function is invoked every time a packet is received by the lobby.
        /// The sending players index is 'senderIndex'
        /// </summary>
        public abstract void OnServerReceivePacket(CrossPacket packet, string packetName, int senderIndex);

        /// <summary>
        /// Send packet to the lobby (persist force sends to all clients)
        /// </summary>
        public void ClientSendPacket(CrossPacket packet, bool persist = true)
        {
            if (packet.readOnly) throw new Exception("This packet can't be sent over the network... Make a new one!");
            Network.CrossClient.SendCrossPacket(packet.ToPacket(persist ? -2 : -1));
            packet.Dispose();
        }

        /// <summary>
        /// Send packet to a specific playerIndex
        /// </summary>
        public void ClientSendPacket(CrossPacket packet, int playerIndex)
        {
            if (packet.readOnly) throw new Exception("This packet can't be sent over the network... Make a new one!");
            Network.CrossClient.SendCrossPacket(packet.ToPacket(playerIndex));
            packet.Dispose();
        }

        /// <summary>
        /// Send packet to a specified player index (leave playerIndex param empty to send to whole lobby)
        /// </summary>
        public void ServerSendPacket(CrossPacket packet, int playerIndex = -1)
        {
            if (packet.readOnly) throw new Exception("This packet can't be sent over the network... Make a new one!");

            var pData = packet.ToPacket();
            packet.Dispose();

            if (playerIndex < 0)
                foreach (var p in Players.Keys)
                    Network.CrossServer.SendCrossPacket(PlayerId[p], pData);
            else Network.CrossServer.SendCrossPacket(PlayerId[playerIndex], pData);
        }

        /// <summary>
        /// Invoked (Client Side) when you join the lobby
        /// </summary>
        public abstract void Client_JoinLobby();

        /// <summary>
        /// Invoked (Client Side) when a player has joined the lobby
        /// </summary>
        public abstract void Client_PlayerJoined(int index);

        /// <summary>
        /// Invoked (Client Side) when a player has left the lobby
        /// </summary>
        public abstract void Client_PlayerLeft(int index);

        /// <summary>
        /// Invoked (Server Side) when the lobby is first created
        /// </summary>
        public abstract void Server_LobbyCreated();

        /// <summary>
        /// Invoked (Server Side) when a player has joined the lobby
        /// </summary>
        public abstract void Server_PlayerJoined(int index);

        /// <summary>
        /// Invoked (Server Side) when a player has left the lobby
        /// </summary>
        public abstract void Server_PlayerLeft(int index);
    }
}