﻿using System.Collections.Generic;

namespace Cross64.Runtimes.Modding
{
    public abstract class ApiBase
    {
        public static readonly Dictionary<int, uint> Addresses = new ();

        public abstract string GameName { get; }
        public abstract GameID GameID { get; }

        internal abstract void Initialize();
        internal abstract void Destroy();

        public virtual void OnFirstFrame()
        {
        }

        public abstract void OnTick(uint frame);
    }

    public class BufferObject
    {
        private Emulator.Pointer Inst;
        private int Length;

        internal BufferObject()
        {
        }

        internal BufferObject(uint addr, int length)
        {
            Inst = (Emulator.Pointer) addr;
            Length = length;
        }

        public byte[] Bytes
        {
            get => Inst.ReadB(Length);
            set => Inst.WriteB(value);
        }

        public byte Byte(int offset = 0)
        {
            return (Inst + offset).U8;
        }

        public void Byte(byte value, int offset = 0)
        {
            var addr = Inst + offset;
            addr.U8 = value;
        }

        public bool Bit(int offset = 0)
        {
            var vbyte = Inst + (offset >> 3);
            var vbit = 1 << (offset & 7);
            return (vbyte.U8 & vbit) != 0;
        }

        public void Bit(bool value, int offset = 0)
        {
            var vbyte = Inst + (offset >> 3);
            var vbit = 1 << (offset & 7);

            if (value) vbyte.U8 |= (byte) vbit;
            else vbyte.U8 &= (byte) ~vbit;
        }
    }
}