﻿using System;
using System.Linq;

namespace Cross64.Runtimes
{
    public class Yaz0
    {
        uint toDWORD(uint d)
        {
            var w1 = d & 0xFF;
            var w2 = (d >> 8) & 0xFF;
            var w3 = (d >> 16) & 0xFF;
            var w4 = d >> 24;
            return (w1 << 24) | (w2 << 16) | (w3 << 8) | w4;
        }

        struct Ret
        {
            public int srcPos, dstPos;
        };

        void decode(ref byte[] src, ref byte[] dst, int uncompressedSize)
        {
            int srcPlace = 0, dstPlace = 0; //current read/write positions

            uint validBitCount = 0; //number of valid bits left in "code" byte
            byte currCodeByte = 0;
            while (dstPlace < uncompressedSize)
            {
                //read new "code" byte if the current one is used up
                if (validBitCount == 0)
                {
                    currCodeByte = src[srcPlace];
                    ++srcPlace;
                    validBitCount = 8;
                }

                if ((currCodeByte & 0x80) != 0)
                {
                    //straight copy
                    dst[dstPlace] = src[srcPlace];
                    dstPlace++;
                    srcPlace++;
                }
                else
                {
                    // RLE part
                    var byte1 = src[srcPlace];
                    var byte2 = src[srcPlace + 1];
                    srcPlace += 2;

                    var dist = (uint) (((byte1 & 0xF) << 8) | byte2);
                    var copySource = (uint) (dstPlace - (dist + 1));

                    var numBytes = (uint) (byte1 >> 4);
                    if (numBytes == 0)
                    {
                        numBytes = (uint) (src[srcPlace] + 0x12);
                        srcPlace++;
                    }
                    else numBytes += 2;

                    // copy run
                    for (var i = 0; i < numBytes; ++i)
                    {
                        dst[dstPlace] = dst[copySource];
                        copySource++;
                        dstPlace++;
                    }
                }

                //use next bit from "code" byte
                currCodeByte <<= 1;
                validBitCount -= 1;
            }
        }

        // simple and straight encoding scheme for Yaz0
        uint simpleEnc(ref byte[] src, int size, int pos, out uint pMatchPos)
        {
            int startPos = pos - 0x1000;
            uint numBytes = 1;
            uint matchPos = 0;

            if (startPos < 0) startPos = 0;
            for (var i = startPos; i < pos; i++)
            {
                var j = 0;
                for (j = 0; j < size - pos; j++)
                {
                    if (src[i + j] != src[j + pos]) break;
                }

                if (j > numBytes)
                {
                    numBytes = (uint) j;
                    matchPos = (uint) i;
                }
            }

            pMatchPos = matchPos;
            if (numBytes == 2) numBytes = 1;
            return numBytes;
        }

        // a lookahead encoding scheme for ngc Yaz0
        uint nintendoEnc(ref byte[] src, int size, int pos, out uint pMatchPos)
        {
            int startPos = pos - 0x1000;
            uint numBytes = 1;
            uint numBytes1 = 0;
            uint matchPos = 0;
            int prevFlag = 0;

            // if prevFlag is set, it means that the previous position was determined by look-ahead try.
            // so just use it. this is not the best optimization, but nintendo's choice for speed.
            if (prevFlag == 1)
            {
                pMatchPos = matchPos;
                prevFlag = 0;
                return numBytes1;
            }

            prevFlag = 0;
            numBytes = simpleEnc(ref src, size, pos, out matchPos);
            pMatchPos = matchPos;

            // if this position is RLE encoded, then compare to copying 1 byte and next position(pos+1) encoding
            if (numBytes >= 3)
            {
                numBytes1 = simpleEnc(ref src, size, pos + 1, out matchPos);
                // if the next position encoding is +2 longer than current position, choose it.
                // this does not guarantee the best optimization, but fairly good optimization with speed.
                if (numBytes1 >= numBytes + 2)
                {
                    numBytes = 1;
                    prevFlag = 1;
                }
            }

            return numBytes;
        }

        int encodeYaz0(ref byte[] src, int srcSize, ref byte[] tBuf)
        {
            Ret r = new Ret();
            var dst = new byte[24]; // 8 codes * 3 bytes maximum
            int bOffset = 0;
            int dstSize = 0;
            int percent = -1;

            uint validBitCount = 0; //number of valid bits left in "code" byte
            byte currCodeByte = 0;
            while (r.srcPos < srcSize)
            {
                uint numBytes;
                uint matchPos;
                uint srcPosBak;

                numBytes = nintendoEnc(ref src, srcSize, r.srcPos, out matchPos);
                if (numBytes < 3)
                {
                    //straight copy
                    dst[r.dstPos] = src[r.srcPos];
                    r.dstPos++;
                    r.srcPos++;
                    //set flag for straight copy
                    currCodeByte |= (byte) (0x80 >> (int) validBitCount);
                }
                else
                {
                    //RLE part
                    uint dist = (uint) r.srcPos - matchPos - 1;
                    byte byte1, byte2, byte3;

                    if (numBytes >= 0x12) // 3 byte encoding
                    {
                        byte1 = (byte) (0 | ((int) dist >> 8));
                        byte2 = (byte) (dist & 0xff);
                        dst[r.dstPos++] = byte1;
                        dst[r.dstPos++] = byte2;
                        // maximum runlength for 3 byte encoding
                        if (numBytes > 0xff + 0x12)
                            numBytes = 0xff + 0x12;
                        byte3 = (byte) (numBytes - 0x12);
                        dst[r.dstPos++] = byte3;
                    }
                    else // 2 byte encoding
                    {
                        byte1 = (byte) (((numBytes - 2) << 4) | (dist >> 8));
                        byte2 = (byte) (dist & 0xff);
                        dst[r.dstPos++] = byte1;
                        dst[r.dstPos++] = byte2;
                    }

                    r.srcPos += (int) numBytes;
                }

                validBitCount++;
                //write eight codes
                if (validBitCount == 8)
                {
                    tBuf[bOffset] = currCodeByte;
                    bOffset += 1;

                    Buffer.BlockCopy(dst, 0, tBuf, bOffset, r.dstPos);
                    bOffset += r.dstPos;

                    dstSize += r.dstPos + 1;

                    srcPosBak = (uint) r.srcPos;
                    currCodeByte = 0;
                    validBitCount = 0;
                    r.dstPos = 0;
                }

                if ((r.srcPos + 1) * 100 / srcSize != percent)
                    percent = (r.srcPos + 1) * 100 / srcSize;
            }

            if (validBitCount > 0)
            {
                tBuf[bOffset] = currCodeByte;
                bOffset += 1;

                Buffer.BlockCopy(dst, 0, tBuf, bOffset, r.dstPos);
                bOffset += r.dstPos;

                dstSize += r.dstPos + 1;

                currCodeByte = 0;
                validBitCount = 0;
                r.dstPos = 0;
            }

            return dstSize;
        }

        // #########################################################
        // ## Yaz0 Node Functions
        // #########################################################

        byte[] Yaz0_Encode(byte[] buf)
        {
            var len = buf.Length;

            var tBuf = new byte[len * 2];
            var size = encodeYaz0(ref buf, len, ref tBuf);
            var alignSize = (int) Math.Ceiling((float) (size + 16) / 16) * 16;
            var yBuf = new byte[alignSize];

            var header = "Yaz0";
            var origSize = toDWORD((uint) len);
            Buffer.BlockCopy(header.ToArray(), 0, yBuf, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(origSize), 0, yBuf, 4, 4);
            Buffer.BlockCopy(tBuf, 0, yBuf, 16, size);

            return yBuf;
        }

        byte[] Yaz0_Decode(byte[] buf)
        {
            uint size = BitConverter.ToUInt32(buf, 4);
            size = toDWORD(size);

            var tBuf = new byte[size];
            var yBuf = new byte[buf.Length - 16];
            Buffer.BlockCopy(buf, 16, yBuf, 0, (int) size);
            decode(ref yBuf, ref tBuf, (int) size);

            return tBuf;
        }
    }
}