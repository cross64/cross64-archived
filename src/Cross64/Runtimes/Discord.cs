namespace Cross64.Runtimes
{
    public static class Discord
    {
        public static string GetDescription()
        {
            return Cross64.Discord.RichPresence.GetDescription();
        }
        
        public static void SetDescription(string value)
        {
            Cross64.Discord.RichPresence.SetDescription(value);
        }
    }
}