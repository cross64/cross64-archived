﻿using System;
using System.Linq;
using Asfw;
using Cross64.Api;
using Cross64.Runtimes.Modding;

namespace Cross64
{
    internal static partial class Network
    {
        internal static partial class CrossServer
        {
            internal static void PacketRouter()
            {
                if (Socket == null) return;

                Socket.PacketId[(int) ClientPackets.CrossPacket] = Packet.CrossPacket;

                Socket.PacketId[(int) ClientPackets.Connect] = Packet.Connect;
                Socket.PacketId[(int) ClientPackets.Disconnect] = Packet.Disconnect;

                Socket.PacketId[(int) ClientPackets.FetchListing] = Packet.FetchListing;
            }

            private static class Packet
            {
                internal static void CrossPacket(int index, ref byte[] data)
                {
                    if (Players.ContainsKey(index)) return;
                    
                    using var bs = new ByteStream(data);

                    var lobby = Players[index].Lobby;
                    var cPacket = new CrossPacket(bs.ReadBytes());
                    var packetName = cPacket.ReadString();
                    var playerIndex = bs.ReadInt32();

                    if (playerIndex == -1) // Persist == false
                        lobby.OnServerReceivePacket(cPacket, packetName, playerIndex);
                    else if (playerIndex == -2) // Persist == true
                    {
                        var relayPacket = new byte[data.Length + 4];
                        Buffer.BlockCopy(data, 0, relayPacket, 4, data.Length);

                        foreach (var pIndex in lobby.PlayerId.Values.Where(pIndex => pIndex != index))
                            SendCrossPacket(pIndex, relayPacket);

                        lobby.OnServerReceivePacket(cPacket, packetName, playerIndex);
                    }
                    else if (playerIndex < 0 || playerIndex >= lobby.Players.Count)
                    {
                        bs.Dispose();
                        return;
                    }
                    else
                    {
                        var relayPacket = new byte[data.Length + 4];
                        Buffer.BlockCopy(data, 0, relayPacket, 4, data.Length);

                        SendCrossPacket(playerIndex, relayPacket);
                    }

                    cPacket.Dispose();
                }

                internal static void Connect(int index, ref byte[] data)
                {
                    // Multi-connect check (can be glitch or hacker)
                    if (Players.ContainsKey(index)) SendConnectFail(index, "Already connected to a lobby!");

                    using var bs = new ByteStream(data);

                    var modName = bs.ReadString();
                    var modVersion = bs.ReadString();

                    PluginBase plugin = null;
                    foreach (var p in ModLoader.Plugins.Where(p => p.Name == modName))
                    {
                        if (p.Version != modVersion)
                        {
                            SendConnectFail(index,
                                $"Mod version did not match! " +
                                "Expected Version[{p.Version}], Attempted Version[{modVersion}]");
                            return;
                        }

                        plugin = p;
                        break;
                    }

                    if (plugin == null)
                    {
                        SendConnectFail(index,
                            $"Tried to connect with mod[{modName}], but that mod isn't hosted on this server!");
                        return;
                    }

                    var dispName = bs.ReadString().Trim();
                    var lobName = bs.ReadString().Trim().ToLower();
                    var lobPass = bs.ReadString();

                    if (plugin.Instance.ContainsKey(lobName))
                    {
                        var lobby = plugin.Instance[lobName] as NetworkedModBase;
                        var pass = lobby.Password;

                        // Connect successful
                        if (pass == "" || pass == lobPass)
                        {
                            // Update our trackers
                            var pIndex = lobby.AddPlayer(dispName, index);
                            Players.Add(index, new PlayerDef(lobby, dispName, pIndex));

                            // Send server event
                            lobby.Server_PlayerJoined(pIndex);

                            // Send them the notification that they joined
                            SendConnectSuccess(index, pIndex);

                            // Send others the notification that they joined
                            foreach (var id in (plugin.Instance[lobName] as NetworkedModBase)!.PlayerId.Values)
                                if (id != index)
                                    SendPlayerConnected(id, pIndex, dispName);
                        }
                        else // Still failed
                            SendConnectFail(index,
                                $"Lobby is password protected and incorrect password was submitted!");
                    }
                    else
                    {
                        // Create a new lobby
                        var lobby = plugin.InitMod as NetworkedModBase;
                        plugin.Instance.Add(lobName, lobby);
                        lobby?.SetIdentity(plugin, lobName, lobPass);
                        lobby!.IsServer = true;

                        // If we are also client
                        if (Program.Settings.Network.IsClient)
                        {
                            lobby!.IsClient = true;
                            lobby!.MyIndex = 0;
                        }

                        // Update our trackers
                        var myIndex = lobby!.AddPlayer(dispName, index);
                        Lobbies.Add(lobName, lobby);
                        Players.Add(index, new PlayerDef(lobby, dispName, myIndex));

                        // Invoke Initialize on the lobby
                        lobby.Initialize(ApiLoader.Api);

                        // Send server events
                        lobby!.Server_LobbyCreated();
                        lobby!.Server_PlayerJoined(myIndex);

                        // Send them the notification that they joined
                        SendConnectSuccess(index, 0);
                    }
                }

                internal static void Disconnect(int index, ref byte[] data)
                {
                    Socket.Disconnect(index);
                }

                internal static void FetchListing(int index, ref byte[] data)
                {
                    using var bs = new ByteStream(data);
                }
            }
        }
    }
}