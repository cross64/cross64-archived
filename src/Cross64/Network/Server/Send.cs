﻿using Asfw;

namespace Cross64
{
    internal static partial class Network
    {
        internal static partial class CrossServer
        {
            internal static void SendCrossPacket(int index, byte[] packet)
            {
                Socket?.SendDataTo(index, packet);
            }

            internal static void SendConnectSuccess(int index, int playerIndex)
            {
                // Get info on lobby
                var lobby = Players[index].Lobby;
                var playerCount = lobby.Players.Count - 1;
                var hasPlayers = playerCount > 0;
                var addCount = 0;

                // Cache player info size
                if (hasPlayers)
                    foreach (var pIndex in lobby.Players.Keys)
                    {
                        if (pIndex == playerIndex) continue;
                        addCount += 8 + lobby.Players[pIndex].Length;
                    }

                using var bs = new ByteStream(12 + addCount);
                bs.WriteInt32((int) ServerPackets.ConnectSuccess);
                bs.WriteInt32(playerIndex);
                bs.WriteInt32(playerCount);

                // Send other player info
                if (hasPlayers)
                    foreach (var pIndex in lobby.Players.Keys)
                    {
                        bs.WriteInt32(pIndex);
                        bs.WriteString(lobby.Players[pIndex]);
                    }

                Socket?.SendDataTo(index, bs.Data, bs.Head);
            }

            internal static void SendConnectFail(int index, string reason)
            {
                using var bs = new ByteStream(8 + reason.Length);
                bs.WriteInt32((int) ServerPackets.ConnectFail);
                bs.WriteString(reason);
                Socket?.SendDataTo(index, bs.Data, bs.Head);
            }

            internal static void SendPlayerConnected(int index, int playerIndex, string dispName)
            {
                using var bs = new ByteStream(12 + dispName.Length);
                bs.WriteInt32((int) ServerPackets.PlayerConnected);
                bs.WriteInt32(playerIndex);
                bs.WriteString(dispName);
                SendToLobby(index, bs.ToPacket());
            }

            internal static void SendPlayerLeft(int index, int playerIndex)
            {
                using var bs = new ByteStream(8);
                bs.WriteInt32((int) ServerPackets.PlayerLeft);
                bs.WriteInt32(playerIndex);
                SendToLobby(index, bs.ToPacket());
            }

            internal static void SendLobbyList(int index, byte[] rawList)
            {
                using var bs = new ByteStream(8 + rawList.Length);
                bs.WriteInt32((int) ClientPackets.FetchListing);
                bs.WriteBytes(rawList);
                Socket?.SendDataTo(index, bs.Data, bs.Head);
            }
        }
    }
}