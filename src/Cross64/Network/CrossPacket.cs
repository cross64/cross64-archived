using System;
using Asfw;
using Cross64.Runtimes.Modding;

namespace Cross64
{
    public class CrossPacket : IDisposable
    {
        internal ByteStream Data;
        internal readonly bool readOnly;

        public CrossPacket(ModBase me, string packetName)
        {
            Data = new ByteStream(264 +
                                  packetName.Length
            );
            
            Data.Head = 4; // Cross Packet == (int)0
            Data.WriteString(packetName);
        }

        internal CrossPacket(byte[] data)
        {
            Data = new ByteStream(data);
            readOnly = true;
        }

        internal byte[] ToPacket(int playerIndex = -1)
        {
            Data.WriteInt32(playerIndex);
            return Data.ToArray();
        }

        /// <summary>
        /// Do not invoke this in plugins! This is used by Cross64 Network
        /// </summary>
        public void Dispose()
        {
            Data.Dispose();
        }

        #region Read

        public byte[] ReadBytes()
        {
            return Data.ReadBytes();
        }

        public string ReadString()
        {
            return Data.ReadString();
        }

        public double ReadDouble()
        {
            return Data.ReadDouble();
        }

        public ulong ReadULong()
        {
            return Data.ReadUInt64();
        }

        public long ReadLong()
        {
            return Data.ReadInt64();
        }

        public float ReadFloat()
        {
            return Data.ReadSingle();
        }

        public float ReadSingle()
        {
            return Data.ReadSingle();
        }

        public uint ReadUInt()
        {
            return Data.ReadUInt32();
        }

        public int ReadInt()
        {
            return Data.ReadInt32();
        }

        public ushort ReadUShort()
        {
            return Data.ReadUInt16();
        }

        public short ReadShort()
        {
            return Data.ReadInt16();
        }

        public bool ReadBoolean()
        {
            return Data.ReadBoolean();
        }

        public char ReadChar()
        {
            return Data.ReadChar();
        }

        public byte ReadByte()
        {
            return Data.ReadByte();
        }

        #endregion

        #region Write

        public void WriteBytes(byte[] value, int offset = 0, int length = 0)
        {
            Data.WriteBytes(value, offset, length);
        }

        public void WriteString(string value)
        {
            Data.WriteString(value);
        }

        public void WriteDouble(double value)
        {
            Data.WriteDouble(value);
        }

        public void WriteULong(ulong value)
        {
            Data.WriteUInt64(value);
        }

        public void WriteLong(long value)
        {
            Data.WriteInt64(value);
        }

        public void WriteFloat(float value)
        {
            Data.WriteSingle(value);
        }

        public void WriteSingle(float value)
        {
            Data.WriteSingle(value);
        }

        public void WriteUInt(uint value)
        {
            Data.WriteUInt32(value);
        }

        public void WriteInt(int value)
        {
            Data.WriteInt32(value);
        }

        public void WriteUShort(ushort value)
        {
            Data.WriteUInt16(value);
        }

        public void WriteShort(short value)
        {
            Data.WriteInt16(value);
        }

        public void WriteBoolean(bool value)
        {
            Data.WriteBoolean(value);
        }

        public void WriteChar(char value)
        {
            Data.WriteChar(value);
        }

        public void WriteByte(byte value)
        {
            Data.WriteByte(value);
        }

        #endregion
    }
}