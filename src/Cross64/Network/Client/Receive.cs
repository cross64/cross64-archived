﻿using System;
using Asfw;
using Cross64.Runtimes.Modding;

namespace Cross64
{
    internal static partial class Network
    {
        internal static partial class CrossClient
        {
            internal static void PacketRouter()
            {
                if (Socket == null) return;

                Socket.PacketId[(int) ServerPackets.CrossPacket] = Packet.CrossPacket;

                Socket.PacketId[(int) ServerPackets.ConnectSuccess] = Packet.ConnectSuccess;
                Socket.PacketId[(int) ServerPackets.ConnectFail] = Packet.ConnectFail;

                Socket.PacketId[(int) ServerPackets.PlayerConnected] = Packet.PlayerConnected;
                Socket.PacketId[(int) ServerPackets.PlayerLeft] = Packet.PlayerLeft;

                Socket.PacketId[(int) ServerPackets.LobbyList] = Packet.LobbyList;
            }

            private static class Packet
            {
                internal static void CrossPacket(ref byte[] data)
                {
                    using var bs = new ByteStream(data);

                    var lobName = Program.Settings.Network.LobbyName;
                    var lobby = ModLoader.OnlineMod.Instance[lobName] as NetworkedModBase;
                    var cPacket = new CrossPacket(bs.ReadBytes());
                    var packetName = cPacket.ReadString();
                    var playerIndex = bs.ReadInt32();

                    lobby?.OnClientReceivePacket(cPacket, packetName, playerIndex);

                    cPacket.Dispose();
                }

                internal static void ConnectSuccess(ref byte[] data)
                {
                    WaitingOnLobby = false;

                    var lobName = Program.Settings.Network.LobbyName;
                    var lobPass = Program.Settings.Network.LobbyPassword;
                    if (!CrossServer.Hosting)
                    {
                        using var bs = new ByteStream(data);
                        var index = bs.ReadInt32();
                        var dispName = Program.Settings.Network.DisplayName;

                        var lobby = ModLoader.OnlineMod.InitMod as NetworkedModBase;
                        ModLoader.OnlineMod.Instance.Add(lobName, lobby);
                        lobby?.SetIdentity(ModLoader.OnlineMod, lobName, lobPass);
                        lobby?.Initialize(ApiLoader.Api);
                        lobby!.IsClient = true;
                        lobby!.MyIndex = index;
                        lobby!.InsertPlayer(dispName, index);

                        // Add any other players
                        var playerCount = bs.ReadInt32();
                        if (playerCount > 0)
                            for (var i = 0; i < playerCount; i++)
                            {
                                var pIndex = bs.ReadInt32();
                                var pDispName = bs.ReadString();
                                lobby.InsertPlayer(pDispName, pIndex, false);
                            }
                    }

                    (ModLoader.OnlineMod.Instance[lobName] as NetworkedModBase)!.Client_JoinLobby();

                    // Invoke Initialize on the lobby
                    if (!CrossServer.Hosting)
                        (ModLoader.OnlineMod.Instance[lobName] as NetworkedModBase)!.Initialize(ApiLoader.Api);
                }

                internal static void ConnectFail(ref byte[] data)
                {
                    using var bs = new ByteStream(data);
                    var reason = bs.ReadString();
                    Console.WriteLine(reason);
                    WaitingOnLobby = false;
                }

                internal static void PlayerConnected(ref byte[] data)
                {
                    if (ModLoader.OnlineMod == null) return;

                    using var bs = new ByteStream(data);
                    var playerIndex = bs.ReadInt32();
                    var dispName = bs.ReadString();
                    var lobby = Program.Settings.Network.LobbyName;
                    (ModLoader.OnlineMod.Instance[lobby] as NetworkedModBase)!.InsertPlayer(dispName, playerIndex);
                }

                internal static void PlayerLeft(ref byte[] data)
                {
                    if (ModLoader.OnlineMod == null) return;

                    using var bs = new ByteStream(data);
                    var playerIndex = bs.ReadInt32();
                    var lobby = Program.Settings.Network.LobbyName;
                    (ModLoader.OnlineMod.Instance[lobby] as NetworkedModBase)!.RemovePlayer(playerIndex);
                }

                internal static void LobbyList(ref byte[] data)
                {
                    using var bs = new ByteStream(data);
                }
            }
        }
    }
}