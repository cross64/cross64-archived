﻿using Asfw;

namespace Cross64
{
    internal static partial class Network
    {
        internal static partial class CrossClient
        {
            internal static void SendCrossPacket(byte[] packet)
            {
                Socket?.SendData(packet);
            }

            internal static void SendConnect(string dispName, string lobName, string lobPass)
            {
                // Send status waiting on lobby
                WaitingOnLobby = true;
                
                using var bs = new ByteStream(16 + dispName.Length + lobName.Length + lobPass.Length);
                bs.WriteInt32((int) ClientPackets.Connect);
                bs.WriteString(ModLoader.OnlineMod?.Name);
                bs.WriteString(ModLoader.OnlineMod?.Version);
                bs.WriteString(dispName);
                bs.WriteString(lobName);
                bs.WriteString(lobPass);
                Socket?.SendData(bs.Data, bs.Head);
            }

            internal static void SendDisconnect()
            {
                using var bs = new ByteStream(4);
                bs.WriteInt32((int) ClientPackets.Disconnect);
                Socket?.SendData(bs.Data, bs.Head);
            }

            internal static void SendFetchListing(string[] plugName)
            {
                if (plugName.Length < 1) return;
                
                using var bs = new ByteStream(8 + plugName.Length);
                bs.WriteInt32((int) ClientPackets.FetchListing);
                
                bs.WriteInt32(plugName.Length);
                
                foreach (var name in plugName)
                    bs.WriteString(name);

                Socket?.SendData(bs.Data, bs.Head);
            }
        }
    }
}