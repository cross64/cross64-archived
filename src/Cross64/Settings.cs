using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Asfw.IO;

namespace Cross64
{
    internal class SettingsDef
    {
        private static readonly Regex regex = new Regex("^[0-9a-zA-Z\\-_ ]+$");

        public class GeneralDef
        {
            public string Game { get; set; } = "Mupen64Plus";
        }

        public class NetworkDef
        {
            public bool IsClient { get; set; } = true;
            public bool IsServer { get; set; } = false;
            public bool IsOnline { get; set; } = true;

            public bool ConnectPrivateServer { get; set; } = false;
            public string Ip { get; set; } = "127.0.0.1";
            public int Port { get; set; } = 8079;

            private string _displayName = "Player";

            public string DisplayName
            {
                get => _displayName;
                set
                {
                    value = value.Trim();
                    if (!regex.IsMatch(value))
                        value = "Player";
                    _displayName = value;
                }
            }

            private string _lobbyName = "cross64";

            public string LobbyName
            {
                get => _lobbyName;
                set
                {
                    value = value.Trim();
                    if (!regex.IsMatch(value))
                        value = "cross64";
                    _lobbyName = value.ToLower();
                }
            }

            private string _lobbyPassword = "";

            public string LobbyPassword
            {
                get => _lobbyPassword;
                set
                {
                    value = value.Trim();
                    if (!regex.IsMatch(value))
                        value = "";
                    _lobbyPassword = value;
                }
            }
        }

        public class CrashSafetyDef
        {
            public bool Enabled { get; set; } = true;
            public int Time_InSeconds { get; set; } = 30;
        }

        public GeneralDef General { get; set; } = new GeneralDef();
        public NetworkDef Network { get; set; } = new NetworkDef();

        public CrashSafetyDef CrashSafety { get; set; } = new CrashSafetyDef();
        public Dictionary<string, bool> LoadedPlugins { get; set; } = new Dictionary<string, bool>();
    }

    internal static partial class Program
    {
        internal static SettingsDef Settings = new SettingsDef();

        internal static void LoadSettings()
        {
            var path = $"{Environment.CurrentDirectory}/Settings.json";

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
                Serialization.SaveJson(path, new SettingsDef());
            }

            Settings = Serialization.LoadJson<SettingsDef>(path);
            Serialization.SaveJson(path, Settings);
        }

        internal static void SaveSettings()
        {
            var path = $"{Environment.CurrentDirectory}/Settings.json";
            Serialization.SaveJson(path, Settings);
        }
    }
}