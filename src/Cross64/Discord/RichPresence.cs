using System;
using DiscordRPC;

namespace Cross64
{
    internal static partial class Discord
    {
        internal static class RichPresence
        {
            private static DiscordRpcClient DiscordClient = new DiscordRpcClient("782914399526584330");
            private static Assets DiscordAssets = new Assets();
            private static Party DiscordParty = new Party();

            internal static void Initialize()
            {
                DiscordClient.Initialize();
                DiscordClient.SkipIdenticalPresence = true;

                DiscordClient.UpdateStartTime();

                var s = new Secrets();
                DiscordClient.UpdateSecrets(s);

                DiscordParty.ID = "bla";
                //DiscordParty.Privacy = Party.PrivacySetting.Public;
                //DiscordParty.Size = 2;
                DiscordParty.Max = 16;
                DiscordClient.UpdateParty(DiscordParty);
            }

            internal static void Test()
            {
            }

            internal static void ResetRPC()
            {
                DiscordClient.UpdateLargeAsset("large", "Cross64 Emulator");
                DiscordClient.UpdateSmallAsset("none", "No Game Selected");
                DiscordClient.UpdateDetails("Selecting a game...");
            }

            internal static void SetGame()
            {
                var gameID = ((int) ApiLoader.Api.GameID).ToString("X4").ToLower();
                DiscordClient.UpdateLargeAsset(gameID, ApiLoader.Api.GameName);

                if (ModLoader.OnlineMod != null)
                {
                    DiscordClient.UpdateSmallAsset(ModLoader.OnlineMod.DiscordImage, ModLoader.OnlineMod.Name + " mod");
                    DiscordClient.UpdateDetails("Playing " + ModLoader.OnlineMod.Name);
                }
                else
                {
                    DiscordClient.UpdateSmallAsset("small", "Cross64 Emulator");
                    DiscordClient.UpdateDetails("Playing " + ApiLoader.Api.GameName);
                }
            }

            internal static string GetDescription()
            {
                return DiscordClient.CurrentPresence.Details;
            }

            internal static void SetDescription(string desc)
            {
                DiscordClient.UpdateDetails(desc);
            }
        }
    }
}