﻿using System;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using TGUI;

namespace Cross64
{
    internal static class Window
    {
        internal static RenderWindow Handle;
        internal static Gui Interface;

        internal static ScreenBase Screen;
        internal static Type ScreenType;
        internal static void SetScreen<T>() where T : ScreenBase
        {
            if (ScreenType == typeof(T)) return;
            if (!(Screen is null)) Screen.Dispose();

            Screen = (T)Activator.CreateInstance(typeof(T));
            Screen.Initialize();

            ScreenType = typeof(T);
        }

        internal static void Initialize()
        {
            Handle = new RenderWindow(VideoMode.DesktopMode, 
                "Cross64: A modding N64 emulator based on Mupen64Plus");
            Handle.Position = new Vector2i(100, 100);
            Handle.Size = new Vector2u(600, 400);

            Interface = new Gui(Handle);
            Interface.View = new View(new FloatRect(0, 0, 600, 400));
            SetScreen<MainMenu>();
        }

        internal static void DoEvents()
        {
            if (Handle is null || 
                Interface is null || 
                Screen is null ||
                !Handle.IsOpen) return;

            // Handle window input events
            Handle.DispatchEvents();

            // Process any screen 'loop' logic
            Screen.Update();

            // Process render
            Handle.Clear(Color.Black);
            Screen.Draw();
            Interface.Draw();
            Handle.Display();
        }

        internal static void Destroy()
        {
            Screen?.Dispose();

            if (!(Interface is null))
            {
                foreach (var widget in Interface.Widgets)
                    widget?.Dispose();

                Interface.RemoveAllWidgets();
                Interface.Dispose();
            }

            if (!(Handle is null))
            {
                if (Handle.IsOpen) Handle.Close();
                Handle.Dispose();
            }
        }
    }
}