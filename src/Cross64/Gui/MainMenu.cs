﻿using System;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using TGUI;

namespace Cross64
{
    internal class MainMenu : ScreenBase
    {
        private Tabs tabPrimary;

        public override void Initialize()
        {
            var tabPrimary = new Tabs();
            tabPrimary.Size = new Vector2f(
                Window.Handle.Size.X, 20);
            tabPrimary.Insert(0, "Play", true);
            tabPrimary.Insert(1, "Controls", false);
            tabPrimary.Insert(2, "Emulator", false);
            Window.Interface.Add(tabPrimary);
        }

        public override void Update()
        {

        }

        public override void Draw()
        {

        }

        protected override void Destroy()
        {
        }
    }
}