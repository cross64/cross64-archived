﻿using System;
using System.IO;
using Cross64.Runtimes;

namespace Cross64
{
    internal static partial class Program
    {
        private static string SaveStatePath => $"{Environment.CurrentDirectory}/saves/{Settings.Network.LobbyName}/" +
                                               $"CrashSave_{Emulator.Cartridge.Name}_Rev{Emulator.Cartridge.Revision}.ss";

        private static long SaveStateLength => Environment.TickCount64 + (Settings.CrashSafety.Time_InSeconds * 1000);
        private static long _ssTime;

        private static void CheckIntegrity()
        {
            var cd = $"{Environment.CurrentDirectory}/";

            if (!Directory.Exists($"{cd}emulator"))
                throw new Exception(
                    "Emulator not found! Install has been modified or corrupted, please reinstall Cross64.");

            if (!Directory.Exists($"{cd}roms")) Directory.CreateDirectory(($"{cd}roms"));
            if (!Directory.Exists($"{cd}plugins")) Directory.CreateDirectory(($"{cd}plugins"));
            if (!Directory.Exists($"{cd}plugin_settings")) Directory.CreateDirectory(($"{cd}plugin_settings"));

            Setup.Verify();
        }

        private static void Main()
        {
            // Load our settings
            LoadSettings();

            // Detect usable plugins
            ModLoader.Cache();

            // Make sure app install is okay and runtimes are installed
            if (Settings.Network.IsClient)
            {
                // Initialize window stuff
                SFML.Portable.Activate();
                Window.Initialize();

                // Initialize Discord Status
                Discord.RichPresence.Initialize();

                CheckIntegrity();

                M64P.PluginLoader.Initialize();
                Emulator.InitMupen();
            }

            // Temporary loop
            if (Settings.Network.IsClient)
            {
                var exts = new[] {".z64", ".n64", ".v64"};

                while (true)
                {
                    Window.DoEvents();

                    // Reset Discord status to 'not playing a game'
                    Discord.RichPresence.ResetRPC();

                    Console.WriteLine(
                        @"Enter a game name to begin (empty to load last entry) or 'exit' to close the app.");
                    var input = Console.ReadLine()?.Trim();
                    var startGame = false;

                    // Reload settings between game instances in
                    // case changes are occuring between play session
                    {
                        LoadSettings();

                        // Enable network
                        if (Settings.Network.IsOnline)
                        {
                            if (Settings.Network.IsServer) Network.CrossServer.InitNetwork();
                            else Network.CrossServer.DestroyNetwork();
                            Network.CrossClient.InitNetwork();
                        }
                        // Disable network
                        else
                        {
                            Network.CrossClient.DestroyNetwork();
                            Network.CrossServer.DestroyNetwork();
                        }

                        // Already playing, has to be a client
                        Settings.Network.IsClient = true;
                    }

                    if (input == "exit") break;
                    if (input == "") input = Settings.General.Game;

                    foreach (var ext in exts)
                        if (File.Exists($"{Environment.CurrentDirectory}/roms/{input?.Split('.')[0]}{ext}"))
                        {
                            if (input != Settings.General.Game)
                            {
                                Settings.General.Game = input ?? "";
                                SaveSettings();
                            }

                            startGame = true;
                        }

                    if (startGame) StartGame();
                    else Console.WriteLine($@"Input of {input} was not valid. Please try again...");

                    // Destroy Networks
                    Network.CrossClient.DestroyNetwork();
                    Network.CrossServer.DestroyNetwork();

                    // Cleanup between sessions
                    CollectGarbage();
                }
                
                // Destroy App Window
                Window.Destroy();
            }
            else if (Settings.Network.IsOnline && Settings.Network.IsServer)
            {
                // Enable the network
                Console.WriteLine("Initializing server-only mode...");
                Network.CrossServer.InitNetwork();
                if (Network.CrossServer.Socket == null)
                    throw new Exception("An error occured starting the Server!");
                Network.CrossServer.Hosting = true;

                // Load the plugins
                Console.WriteLine("Initializing plugins...");
                ModLoader.Load();

                // Start listening for connections then begin server runtime
                try
                {
                    Network.CrossServer.Socket.StartListening(Settings.Network.Port, 5);
                    Console.WriteLine("Server ready - Waiting for players!");
                }
                catch
                {
                    throw new Exception($@"Server could not be ran on port[{Settings.Network.Port}]...");
                }

                var running = true;
                while (running)
                {
                    var command = Console.ReadLine()?.Trim() ?? "";
                    switch (command.ToLower())
                    {
                        case "plugins":
                            Console.Write(@"Current loaded plugins:");
                            foreach (var p in ModLoader.Plugins)
                                Console.Write($@" [{p.Name} | {p.Version}]");
                            Console.WriteLine(@".");
                            break;

                        case "lobbies":
                            Console.WriteLine($@"Current count of lobbies [{Network.CrossServer.Lobbies.Count}]");
                            break;

                        case "exit":
                            running = false;
                            break;

                        default: break;
                    }
                }

                Network.CrossServer.Socket.StopListening();
            }
            else Console.WriteLine(@"No play mode was specified. Exiting program!");

            // Destroy Networks
            Network.CrossClient.DestroyNetwork();
            Network.CrossServer.DestroyNetwork();
        }

        internal static void StartGame()
        {
            // Mupen init process -> Might need rearranged/moved
            Emulator.LoadGame(Settings.General.Game);
            Emulator.SaveDirectory(Settings.Network.LobbyName);

            // Load mod stuff
            ApiLoader.Load();
            ModLoader.Load();

            // Set some discord information based on the active game or networked mod
            Discord.RichPresence.SetGame();

            if (ModLoader.OnlineMod != null)
            {
                // Run networking stuff
                if (Settings.Network.IsServer)
                {
                    Console.WriteLine(@"Server: Preparing for connections...");

                    try
                    {
                        Network.CrossServer.Socket?.StartListening(Settings.Network.Port, 5);

                        Console.WriteLine(@"Server: Waiting for player connections!");

                        Network.CrossServer.Hosting = true;
                    }
                    catch
                    {
                        Console.WriteLine($@"Server could not be ran on port[{Settings.Network.Port}]...");
                        Console.WriteLine(@"Continuing as client only!");
                        Network.CrossServer.DestroyNetwork();
                    }
                }

                if (Settings.Network.IsClient)
                {
                    // Get the proper connection
                    if (Network.CrossServer.Hosting)
                        Network.CrossClient.Connect("127.0.0.1", Settings.Network.Port);
                    else if (Settings.Network.ConnectPrivateServer)
                        Network.CrossClient.Connect(Settings.Network.Ip, Settings.Network.Port);
                    else
                        Network.CrossClient.Connect(ModLoader.OnlineMod.MasterServerIp,
                            ModLoader.OnlineMod.MasterServerPort);

                    Console.WriteLine(@"Client: Preparing to connect...");

                    var timer = Environment.TickCount + 5000;
                    while (Network.CrossClient.Connecting)
                    {
                        // Didn't connect after 5 seconds
                        if (timer < Environment.TickCount) break;

                        // Waiting for the client to connect
                        System.Threading.Thread.Sleep(1);
                    }

                    if (!Network.CrossClient.Socket?.IsConnected ?? true)
                    {
                        Console.WriteLine(@"Client: Failed to connect to server...");
                        Console.WriteLine(@"Unloading the online mod!");

                        Network.CrossClient.DestroyNetwork();
                        if (Network.CrossServer.Hosting)
                            Network.CrossServer.DestroyNetwork();

                        // Dispose of the net plugin, we cant play it!
                        ModLoader.OnlineMod.Dispose();
                        ModLoader.OnlineMod = null;
                        CollectGarbage();

                        goto RunGame;
                    }

                    Console.WriteLine(@"Client: Connected to server successfully!");

                    Network.CrossClient.SendConnect(
                        Settings.Network.DisplayName,
                        Settings.Network.LobbyName,
                        Settings.Network.LobbyPassword
                    );

                    while (Network.CrossClient.WaitingOnLobby)
                    {
                        Network.CrossClient.Socket?.ReceiveData();
                        System.Threading.Thread.Sleep(1);
                    }

                    if (!ModLoader.OnlineMod.Instance.ContainsKey(Settings.Network.LobbyName))
                    {
                        Console.WriteLine(@"Failed to connect to lobby... Unloading the mod!");

                        // Exit the boot cycle -> We couldn't begin game!
                        //     - Unload mod stuff
                        ModLoader.Unload();
                        ApiLoader.Unload();

                        CollectGarbage();
                        return;
                    }
                }
            }

            RunGame:

            // Boot cycle
            Emulator.RunGame();
            Emulator.ResetMupen();

            // Kill crash detection save state
            var ssPath = SaveStatePath;
            if (File.Exists(ssPath)) File.Delete(ssPath);

            // Unload mod stuff
            ModLoader.Unload();
            ApiLoader.Unload();

            CollectGarbage();

            // Save before we close
            SaveSettings();
        }

        internal static void FrameCallback(uint curFrame)
        {
            if (ModLoader.OnlineMod != null)
                Network.CrossClient.Socket?.ReceiveData();

            if (curFrame == 0)
            {
                // Check for existing save state
                var ssPath = SaveStatePath;
                if (File.Exists(ssPath))
                {
                    Emulator.LoadState(ssPath);
                    File.Delete(ssPath);
                }

                // Make sure any new plugins settings get saved first
                M64P.ConfigSaveFile();

                // Pre-load any 'auto' asm
                ApiLoader.LoadAsm();
                ModLoader.LoadAsm();
                Emulator.DynarecRefresh();

                ApiLoader.OnFirstFrame();
                ModLoader.OnFirstFrame();

                // Reset save state timer
                _ssTime = SaveStateLength;
            }
            else
            {
                ApiLoader.OnTick(curFrame);
                ModLoader.OnTick(curFrame);

                // Update save state + timer
                if (_ssTime < Environment.TickCount64)
                {
                    if (Settings.CrashSafety.Enabled)
                        Emulator.SaveState(SaveStatePath);

                    _ssTime = SaveStateLength;
                }
            }
        }

        /// <summary>
        /// Force GC cleanup
        /// </summary>
        internal static void CollectGarbage()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}