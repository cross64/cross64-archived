using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using Cross64.Runtimes;
using Cross64.Runtimes.Modding;

namespace Cross64
{
    internal static class ApiLoader
    {
        public static ApiBase Api;

        public static void LoadAsm()
        {
            Utility.AutoLoadAssembly(
                Program.Settings,
                Emulator.Cartridge.Country + Emulator.Cartridge.Revision,
                $@"Api.{Api!.GameID.ToString()}."
            );
        }

        public static void OnFirstFrame()
        {
            try
            {
                Api?.OnFirstFrame();
            }
            catch (Exception e)
            {
                Console.WriteLine(@"Api[OnFirstFrame] encountered an error: " + e);
            }
        }

        public static void OnTick(uint curFrame)
        {
            try
            {
                Api?.OnTick(curFrame);
            }
            catch (Exception e)
            {
                Console.WriteLine(@"Api[OnTick] encountered an error: " + e);
            }
        }

        public static void Load()
        {
            if (Api != null) Unload();

            try
            {
                var cross64 = Assembly.GetExecutingAssembly();
                var game = Emulator.Cartridge.Serial;

                var types = cross64.GetTypes()
                    .Where(p =>
                        typeof(ApiBase).IsAssignableFrom(p)
                        && p.IsClass && !p.IsAbstract)
                    .ToArray();
                foreach (var type in types)
                {
                    var api = (ApiBase) Activator.CreateInstance(type)!;

                    if (api?.GameID != (GameID) game) continue;
                    api.Initialize();
                    Api = api;

                    Console.WriteLine($@"Detected Api [{(GameID) game}]");
                }

                if (Api != null) return;

                // Api not found, fallback to empty mupen64plus core
                Api = new Api.Mupen64Plus();
                Api.Initialize();

                Console.WriteLine($@"Api could not be found for [{(GameID) game}]! " +
                                  @"Please contact the developers on the official Cross64 Discord!");
            }
            catch (Exception e)
            {
                throw new Exception($"Api couldn't be loaded: {e.Message}");
            }
        }

        public static void Unload()
        {
            Api?.Destroy();
            Api = null;
        }
    }

    internal static class ModLoader
    {
        public static readonly List<PluginBase> Plugins = new List<PluginBase>();
        public static PluginBase OnlineMod;

        public static void LoadAsm()
        {
            if (Plugins.Count < 1) return;

            var curMod = 0;

            try
            {
                for (var i = 0; i < Plugins.Count; i++)
                {
                    curMod = i;

                    foreach (var inst in Plugins[i].Instance.Values)
                        Utility.AutoLoadAssembly(inst, Emulator.Cartridge.Country + Emulator.Cartridge.Revision);
                }
            }
            catch (Exception e)
            {
                var name = Plugins[curMod].Name;
                Console.WriteLine($@"Plugin [{name}][AutoAsmLoader] encountered an error: {e}");
                Plugins[curMod].Dispose();
                Plugins.RemoveAt(curMod);
                Console.WriteLine($@"Plugin [{name}] has been unloaded!");
            }
        }

        public static void OnFirstFrame()
        {
            if (Plugins.Count < 1) return;

            var curMod = 0;

            try
            {
                for (var i = 0; i < Plugins.Count; i++)
                {
                    curMod = i;

                    foreach (var inst in Plugins[i].Instance.Values)
                        inst.OnFirstFrame();
                }
            }
            catch (Exception e)
            {
                var name = Plugins[curMod].Name;
                Console.WriteLine($@"Plugin [{name}][OnFirstFrame] encountered an error: {e}");
                Plugins[curMod].Dispose();
                Plugins.RemoveAt(curMod);
                Console.WriteLine($@"Plugin [{name}] has been unloaded!");
            }
        }

        public static void OnTick(uint curFrame)
        {
            if (Plugins.Count < 1) return;

            var curMod = 0;

            try
            {
                for (var i = 0; i < Plugins.Count; i++)
                {
                    curMod = i;

                    foreach (var inst in Plugins[i].Instance.Values)
                        inst.OnTick(curFrame);
                }
            }
            catch (Exception e)
            {
                var name = Plugins[curMod].Name;
                Console.WriteLine($@"Plugin [{name}][OnTick] encountered an error: {e}");
                Plugins[curMod].Dispose();
                Plugins.RemoveAt(curMod);
                Console.WriteLine($@"Plugin [{name}] has been unloaded!");
            }
        }

        #region Loader

        public static void Cache()
        {
            var path = $"{Environment.CurrentDirectory}/plugins/";
            if (!Directory.Exists(path)) return;

            var dupCheck = new Dictionary<string, string>();
            var realList = new Dictionary<string, bool>();
            var modifiedList = false;

            var files = new DirectoryInfo(path).GetFiles();
            foreach (var file in files)
            {
                if (file.Extension != ".dll") continue;

                var plugin = LoadMod(file.FullName);
                var types = plugin.GetTypes()
                    .Where(p =>
                        typeof(PluginBase).IsAssignableFrom(p)
                        && p.IsClass)
                    .ToArray();
                foreach (var type in types)
                {
                    var inst = (PluginBase) Activator.CreateInstance(type)!;

                    // Make sure no duplicate or incompatible plugins exist (We don't want to load plugins with the same name!)
                    if (dupCheck.ContainsKey(inst.Name))
                    {
                        throw new Exception($"Incompatible or duplicate plugins! [{inst.Name}] exists in: " +
                                            $"{file.Name} and {dupCheck[inst.Name]}! Please remove one of them and re-open Cross64 to continue");
                    }

                    // Add an entry to mark if we've already found this name before
                    dupCheck.Add(inst.Name, file.Name);

                    // Add if missing entry otherwise steal existing entry!
                    if (!Program.Settings.LoadedPlugins.ContainsKey(file.Name))
                    {
                        realList.Add(file.Name, true);
                        modifiedList = true;
                    }
                    else realList.Add(file.Name, Program.Settings.LoadedPlugins[file.Name]);

                    inst.Dispose();
                }
            }

            // Try to detect any changes (remove non-existent entries and update new ones)
            if (!modifiedList)
                if (Program.Settings.LoadedPlugins.Keys.Any(p => !realList.ContainsKey(p)))
                    modifiedList = true;

            // Save any changes
            if (!modifiedList) return;
            Program.Settings.LoadedPlugins = realList;
            Program.SaveSettings();
        }

        public static void Load()
        {
            try
            {
                Unload();
                LoadMods();
            }
            catch (Exception e)
            {
                throw new Exception($"Plugins couldn't be loaded: {e.Message}");
            }
        }

        public static void Unload()
        {
            OnlineMod = null;

            if (Plugins.Count < 1) return;

            foreach (var p in Plugins)
                p.Dispose();

            Plugins.Clear();
        }

        private static void LoadMods()
        {
            var path = $"{Environment.CurrentDirectory}/plugins/";
            if (!Directory.Exists(path)) return;

            OnlineMod = null;
            var game = Program.Settings.Network.IsClient ? Emulator.Cartridge.Serial : (ushort) 0;
            var files = Program.Settings.LoadedPlugins.Where(p => p.Value);
            foreach (var file in files)
            {
                var plugin = LoadMod(path + file.Key);
                var types = plugin.GetTypes().Where(p =>
                    typeof(PluginBase).IsAssignableFrom(p)
                    && p.IsClass
                ).ToArray();
                foreach (var type in types)
                {
                    var inst = (PluginBase) Activator.CreateInstance(type)!;

                    try
                    {
                        if (Program.Settings.Network.IsClient)
                        {
                            if (inst.Game.Contains((GameID) game) ||
                                inst.Game.Contains(GameID.Mupen64Plus))
                            {
                                // Add mod entry
                                if (inst.NetworkMode != PluginBase.NetworkType.None) // Online
                                {
                                    // Check if offline only is desired
                                    if (!Program.Settings.Network.IsOnline)
                                    {
                                        inst.Dispose();
                                        continue;
                                    }

                                    // Prevent multiple online mods for a single game
                                    if (OnlineMod != null)
                                        throw new Exception(
                                            "Only one online plugin targeting this game can be loaded at a time!");

                                    // Mark that an online mod has been loaded!
                                    OnlineMod = inst;
                                }
                                else // Offline
                                {
                                    inst.Instance.Add("!", inst.InitMod);
                                    inst.Instance["!"].SetIdentity(inst, "!", "");
                                    inst.Instance["!"].Initialize(ApiLoader.Api);
                                }

                                inst.Version = plugin.GetName().Version?.ToString() ?? "0.0.0.0";
                                Console.WriteLine($@"Loaded Plugin [{inst.Name} | {inst.Version}]");

                                Plugins.Add(inst);
                                continue;
                            }
                        }
                        else if (Network.CrossServer.Hosting)
                        {
                            if (inst.NetworkMode != PluginBase.NetworkType.None)
                            {
                                // Add plugin entry
                                inst.Version = plugin.GetName().Version?.ToString() ?? "0.0.0.0";
                                Console.WriteLine($@"Loaded Plugin [{inst.Name} | {inst.Version}]");
                                
                                Plugins.Add(inst);
                                continue;
                            }
                        }
                        else throw new Exception("Cannot load mods, no game/client or server to run them!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($@"Loading plugin[{inst.Name} failed: {e}]");
                    }

                    inst.Dispose();
                }
            }
        }

        private static Assembly LoadMod(string path)
        {
            var loadContext = new ModLoadContext(path);
            path = Path.GetFileNameWithoutExtension(path);
            return loadContext.LoadFromAssemblyName(new AssemblyName(path));
        }

        private class ModLoadContext : AssemblyLoadContext
        {
            private readonly AssemblyDependencyResolver _resolver;

            public ModLoadContext(string path)
            {
                _resolver = new AssemblyDependencyResolver(path);
            }

            protected override Assembly Load(AssemblyName assemblyName)
            {
                var assemblyPath = _resolver.ResolveAssemblyToPath(assemblyName);
                return (assemblyPath != null ? LoadFromAssemblyPath(assemblyPath) : null)!;
            }

            protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
            {
                var libraryPath = _resolver.ResolveUnmanagedDllToPath(unmanagedDllName);
                return libraryPath != null ? LoadUnmanagedDllFromPath(libraryPath) : IntPtr.Zero;
            }
        }

        #endregion
    }
}